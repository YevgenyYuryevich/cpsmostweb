import { Component, OnInit, HostListener } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {AppCommonService} from "../../services/app-common.service";
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit {
  color = 'defaultdark';
  showSettings = false;
  showMinisidebar = false;
  showDarktheme = false;

  public innerWidth: any;

  public config: PerfectScrollbarConfigInterface = {};

  constructor(public router: Router, private route: ActivatedRoute, private appCommonService: AppCommonService, private authService: AuthService) {
      appCommonService.userLevels = this.route.snapshot.data['appCommon']['user_levels'].map(level => {
          level.can_form_library = parseInt(level.can_form_library);
          return level;
      });
      this.authService.auth.role = this.appCommonService.userLevels.find(level => {
          return level.id == this.authService.auth.level;
      }).name.toLowerCase();
      this.authService.auth.role_data = this.appCommonService.userLevels.find(level => {
          return level.id == this.authService.auth.level;
      });
  }

  ngOnInit() {
    if (this.router.url === '/') {
      this.router.navigate(['/admin/dashboard']);
    }
    this.handleLayout();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.handleLayout();
  }

  toggleSidebar() {
    this.showMinisidebar = !this.showMinisidebar;
  }

  handleLayout() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 1170) {
      this.showMinisidebar = true;
    } else {
      this.showMinisidebar = false;
    }
  }
}

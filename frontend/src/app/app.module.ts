import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';

import { NavigationComponent } from './shared/header-navigation/navigation.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { BreadcrumbComponent } from './shared/breadcrumb/breadcrumb.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { AppRoutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpinnerComponent } from './shared/spinner.component';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import {ServicesModule} from './services/services.module';
import {AuthGuard} from "./guards/auth.guard";
import {NgxLoadingModule} from "ngx-loading";
import {Ng2SmartTableModule} from "ng2-smart-table";
import { ProfileComponent } from './pages/admin/profile/profile.component';
import { UserLevelsComponent } from './pages/admin/user-levels/user-levels.component';
import {AppCommonResolveService} from "./services/app-common-resolve.service";
import {HelperService} from './services/helper.service';
import { ManageContentComponent } from './pages/admin/manage-content/manage-content.component';
import {SortablejsModule} from 'angular-sortablejs';
import {AppComponentsModule} from './app-components/app-components.module';
import {PipesModule} from './pipes/pipes.module';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {ResolvesModule} from './resolves/resolves.module';
import {UiSwitchModule} from 'ngx-ui-switch';
import {ToastrModule} from 'ngx-toastr';
import { HomeComponent } from './pages/public/home/home.component';
import { FindCourseComponent } from './pages/public/find-course/find-course.component';
import { AgmCoreModule } from '@agm/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { CoursesComponent } from './pages/public/courses/courses.component';
import { CourseDetailsComponent } from './pages/public/course-details/course-details.component';
import { BecomeInstructorComponent } from './pages/public/become-instructor/become-instructor.component';
import { ListPostComponent } from './pages/public/list-post/list-post.component';
import { ViewPostComponent } from './pages/public/view-post/view-post.component';
import { ContactComponent } from './pages/public/contact/contact.component';
import { SelectLocationComponent } from './pages/public/select-location/select-location.component';
import { SelectCourseTypeComponent } from './pages/public/select-course-type/select-course-type.component';
import { FaqComponent } from './pages/public/faq/faq.component';
import { NewsletterSubscribingComponent } from './pages/admin/newsletter-subscribing/newsletter-subscribing.component';
import { AdminGuard } from './guards/admin.guard';
import { ListVideoComponent } from './pages/public/list-video/list-video.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true,
    wheelSpeed: 2,
    wheelPropagation: true
};

@NgModule({
    declarations: [
        AppComponent,
        SpinnerComponent,
        FullComponent,
        NavigationComponent,
        BreadcrumbComponent,
        SidebarComponent,
        DashboardComponent,
        ProfileComponent,
        UserLevelsComponent,
        ManageContentComponent,
        HomeComponent,
        FindCourseComponent,
        CoursesComponent,
        CourseDetailsComponent,
        BecomeInstructorComponent,
        ListPostComponent,
        ViewPostComponent,
        ContactComponent,
        SelectLocationComponent,
        SelectCourseTypeComponent,
        FaqComponent,
        NewsletterSubscribingComponent,
        ListVideoComponent,
    ],
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            positionClass: 'toast-bottom-right'
        }),
        FormsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC34jh9oAM8ivtIz2eTm-CCHfK-HsALgt0',
            libraries: ['geometry', 'visualization']
        }),
        HttpClientModule,
        NgbModule,
        RouterModule.forRoot(AppRoutes),
        PerfectScrollbarModule,
        ServicesModule,
        NgxLoadingModule,
        Ng2SmartTableModule,
        SortablejsModule.forRoot({ animation: 150 }),
        AppComponentsModule,
        PipesModule,
        SlickCarouselModule,
        ResolvesModule,
        UiSwitchModule,
        AngularFontAwesomeModule,
        NgxDatatableModule,
        ScrollToModule.forRoot(),
    ],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        },
        AuthGuard,
        AdminGuard,
        AppCommonResolveService,
        HelperService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}

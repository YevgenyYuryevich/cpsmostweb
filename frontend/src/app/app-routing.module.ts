import { Routes} from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import {DashboardComponent} from './pages/admin/dashboard/dashboard.component';
import {AuthGuard} from './guards/auth.guard';
import {ProfileComponent} from './pages/admin/profile/profile.component';
import {UserLevelsComponent} from './pages/admin/user-levels/user-levels.component';
import {AppCommonResolveService} from './services/app-common-resolve.service';
import {ManageContentComponent} from './pages/admin/manage-content/manage-content.component';
import {PostsResolveService} from './resolves/posts-resolve.service';
import {SocialResolveService} from './resolves/social-resolve.service';
import {CourseTypeResolveService} from './resolves/course-type-resolve.service';
import {CourseProviderResolveService} from './resolves/course-provider-resolve.service';
import {PublicFullLayoutComponent} from './app-components/public-full-layout/public-full-layout.component';
import {HomeComponent} from './pages/public/home/home.component';
import {FindCourseComponent} from './pages/public/find-course/find-course.component';
import {CoursesResolveService} from './resolves/courses-resolve.service';
import {GetCourseTypeResolveService} from './resolves/get-course-type-resolve.service';
import {SocialFeedResolveService} from './resolves/social-feed-resolve.service';
import {CoursesComponent} from './pages/public/courses/courses.component';
import {CourseDetailsComponent} from './pages/public/course-details/course-details.component';
import {BecomeInstructorComponent} from './pages/public/become-instructor/become-instructor.component';
import {ListPostComponent} from './pages/public/list-post/list-post.component';
import {ViewPostComponent} from './pages/public/view-post/view-post.component';
import {PostResolveService} from './resolves/post-resolve.service';
import {SelectLocationComponent} from './pages/public/select-location/select-location.component';
import {SelectCourseTypeComponent} from './pages/public/select-course-type/select-course-type.component';
import {FaqComponent} from './pages/public/faq/faq.component';
import {FaqsResolveService} from './resolves/faqs-resolve.service';
import {PermanentResourcesResolveService} from './resolves/permanent-resources-resolve.service';
import {ContactComponent} from './pages/public/contact/contact.component';
import {InstructorsResolveService} from './resolves/instructors-resolve.service';
import {InsurancesResolveService} from './resolves/insurances-resolve.service';
import {RenewalsResolveService} from './resolves/renewals-resolve.service';
import {InsuranceOptionsResolveService} from './resolves/insurance-options-resolve.service';
import {RenewalOptionsResolveService} from './resolves/renewal-options-resolve.service';
import {IncidentsResolveService} from './resolves/incidents-resolve.service';
import {CourseTracksResolveService} from './resolves/course-tracks-resolve.service';
import {NewsletterSubscribingComponent} from './pages/admin/newsletter-subscribing/newsletter-subscribing.component';
import {NewsletterEmailsResolveService} from './resolves/newsletter-emails-resolve.service';
import {AdminGuard} from './guards/admin.guard';
import {ListVideoComponent} from './pages/public/list-video/list-video.component';
import {VendorsResolveService} from './resolves/vendors-resolve.service';
import {UsersResolveService} from './pages/admin/users/services/users-resolve.service';

export const AppRoutes: Routes = [
    {
        path: 'admin',
        component: FullComponent,
        canActivate: [AuthGuard],
        resolve: {
            appCommon: AppCommonResolveService
        },
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent,
                canActivate: [AdminGuard],
                resolve: {
                    users: UsersResolveService,
                    instructors: InstructorsResolveService,
                    vendors: VendorsResolveService,
                    insurances: InsurancesResolveService,
                    renewals: RenewalsResolveService,
                    insurance_options: InsuranceOptionsResolveService,
                    renewal_options: RenewalOptionsResolveService,
                    incidents: IncidentsResolveService,
                    course_tracks: CourseTracksResolveService
                }
            },
            {
                path: 'profile',
                component: ProfileComponent,
            },
            {
                path: 'users',
                loadChildren: './pages/admin/users/users.module#UsersModule'
            },
            {
                path: 'courses',
                loadChildren: './pages/admin/courses/courses.module#CoursesModule',
            },
            {
                path: 'manage-content',
                component: ManageContentComponent,
                resolve: {
                    posts: PostsResolveService
                }
            },
            {
                path: 'user-levels',
                component: UserLevelsComponent,
            },
            {
                path: 'add-content',
                loadChildren: './pages/admin/add-content/add-content.module#AddContentModule'
            },
            {
                path: 'settings',
                loadChildren: './pages/admin/settings/settings.module#SettingsModule'
            },
            {
                path: 'course-types',
                loadChildren: './pages/admin/course-types/course-types.module#CourseTypesModule'
            },
            {
                path: 'course-providers',
                loadChildren: './pages/admin/course-providers/course-providers.module#CourseProvidersModule'
            },
            {
                path: 'reminders',
                loadChildren: './pages/admin/reminders/reminders.module#RemindersModule'
            },
            {
                path: 'faq',
                loadChildren: './pages/admin/faq/faq.module#FaqModule'
            },
            {
                path: 'incidents',
                loadChildren: './pages/admin/incidents/incidents.module#IncidentsModule'
            },
            {
                path: 'insurance',
                loadChildren: './pages/admin/insurance-verification-system/insurance-verification-system.module#InsuranceVerificationSystemModule'
            },
            {
                path: 'rider-coach-certification',
                loadChildren: './pages/admin/rider-coach-certification/rider-coach-certification.module#RiderCoachCertificationModule'
            },
            {
                path: 're-app',
                loadChildren: './pages/admin/renewal-application/renewal-application.module#RenewalApplicationModule'
            },
            {
                path: 'newsletter-subscribing',
                component: NewsletterSubscribingComponent,
                resolve: {
                    subscribe_emails: NewsletterEmailsResolveService
                }
            },
            {
                path: 'quality-assurance',
                loadChildren: './pages/admin/quality-assurance/quality-assurance.module#QualityAssuranceModule'
            },
            {
                path: 'form-library',
                loadChildren: './pages/admin/form-library/form-library.module#FormLibraryModule'
            },
        ]
    },
    {
        path: 'auth',
        loadChildren: './pages/auth/auth.module#AuthModule'
    },
    {
        path: '',
        component: PublicFullLayoutComponent,
        resolve: {
            social: SocialResolveService,
            permanent_resources: PermanentResourcesResolveService,
            app_common: AppCommonResolveService
        },
        children: [
            {
                path: '',
                component: HomeComponent,
                resolve: {
                    posts: PostsResolveService,
                    course_types: CourseTypeResolveService,
                    course_providers: CourseProviderResolveService,
                    social_feed: SocialFeedResolveService,
                }
            },
            {
                path: 'home',
                component: HomeComponent,
                resolve: {
                    posts: PostsResolveService,
                    course_types: CourseTypeResolveService,
                    course_providers: CourseProviderResolveService,
                    social_feed: SocialFeedResolveService,
                }
            },
            {
                path: 'find-course/:type_id/:tab',
                component: FindCourseComponent,
                resolve: {
                    courses: CoursesResolveService,
                    course_providers: CourseProviderResolveService,
                    course_type: GetCourseTypeResolveService,
                }
            },
            {
                path: 'find-course/:type_id/:tab/:center_location',
                component: FindCourseComponent,
                resolve: {
                    courses: CoursesResolveService,
                    course_providers: CourseProviderResolveService,
                    course_type: GetCourseTypeResolveService,
                }
            },
            {
                path: 'contact',
                component: ContactComponent,
            },
            {
                path: 'courses',
                component: CoursesComponent,
                resolve: {
                    course_providers: CourseProviderResolveService,
                    course_types: CourseTypeResolveService,
                    social_feed: SocialFeedResolveService,
                }
            },
            {
                path: 'course-detail/:id',
                component: CourseDetailsComponent,
                resolve: {
                    course_types: CourseTypeResolveService,
                    social_feed: SocialFeedResolveService,
                }
            },
            {
                path: 'faq',
                component: FaqComponent,
                resolve: {
                    faqs: FaqsResolveService,
                    course_providers: CourseProviderResolveService,
                    course_types: CourseTypeResolveService,
                    social_feed: SocialFeedResolveService,
                }
            },
            {
                path: 'become-instructor',
                component: BecomeInstructorComponent,
                resolve: {
                }
            },
            {
                path: 'list-video',
                component: ListVideoComponent,
                resolve: {
                    posts: PostsResolveService,
                    course_types: CourseTypeResolveService,
                    social_feed: SocialFeedResolveService,
                },
                data: {
                    post_where: { type: 'video' }
                }
            },
            {
                path: 'list-post',
                component: ListPostComponent,
                resolve: {
                    posts: PostsResolveService,
                    course_types: CourseTypeResolveService,
                    social_feed: SocialFeedResolveService,
                },
                data: {
                    post_where: {}
                }
            },
            {
                path: 'view-post/:post',
                component: ViewPostComponent,
                resolve: {
                    post: PostResolveService,
                    course_types: CourseTypeResolveService,
                    social_feed: SocialFeedResolveService,
                },
                pathMatch: 'full'
            },
            {
                path: 'select-location',
                component: SelectLocationComponent,
            },
            {
                path: 'select-location/:course_type_id',
                component: SelectLocationComponent,
            },
            {
                path: 'select-course-type',
                component: SelectCourseTypeComponent,
                resolve: {
                    course_types: CourseTypeResolveService
                }
            },
            {
                path: 'select-course-type/:location',
                component: SelectCourseTypeComponent,
                resolve: {
                    course_types: CourseTypeResolveService
                }
            },
        ]
    },
];

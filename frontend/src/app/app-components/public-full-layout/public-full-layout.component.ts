import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {AppCommonService} from '../../services/app-common.service';
import {AjaxApiService} from '../../services/ajax-api.service';
import {ToastrService} from 'ngx-toastr';

declare var jQuery: any;

@Component({
    selector: 'app-public-full-layout',
    templateUrl: './public-full-layout.component.html',
    styleUrls: ['./public-full-layout.component.scss']
})
export class PublicFullLayoutComponent implements OnInit {

    mobileSidebarOpened = false;
    social: any = {};
    permanentResources: any[];
    joinNewsletterActive = false;
    newsletter_email = '';
    @ViewChild('joinFormWrap') joinFormWrap: ElementRef;
    private twitter: any;

    constructor(private route: ActivatedRoute, private router: Router, private appCommonService: AppCommonService, private ajaxApi: AjaxApiService, private toastr: ToastrService) {
        this.social = route.snapshot.data['social'];
        this.permanentResources = route.snapshot.data['permanent_resources'].sort((a, b) => {
            if (a.id < b.id) {
                return 1;
            } else if (a.id === b.id) {
                return 0;
            } else {
                return -1;
            }
        });
        this.appCommonService.permanentResources = this.permanentResources;
        this.appCommonService.socialLinks = this.social;
        this.appCommonService.userLevels = this.route.snapshot.data['app_common']['user_levels'].map(level => {
            level.can_form_library = parseInt(level.can_form_library, 10);
            return level;
        });
        this.initTwitterWidget();
    }

    initTwitterWidget() {
        this.twitter = this.router.events.subscribe(val => {
            if (val instanceof NavigationEnd) {
                (<any>window).twttr = (function (d, s, id) {
                    let js: any, fjs = d.getElementsByTagName(s)[0],
                        t = (<any>window).twttr || {};
                    if (d.getElementById(id)) return t;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = 'https://platform.twitter.com/widgets.js';
                    fjs.parentNode.insertBefore(js, fjs);

                    t._e = [];
                    t.ready = function (f: any) {
                        t._e.push(f);
                    };

                    return t;
                }(document, 'script', 'twitter-wjs'));

                if ((<any>window).twttr.ready())
                    (<any>window).twttr.widgets.load();
            }
        });
    }

    ngOnInit() {
    }

    onSiteBackDropClick() {
        if (this.mobileSidebarOpened) {
            this.mobileSidebarOpened = false;
        }
    }

    scrollToEvents() {
        if (this.router.url === '/' || this.router.url === '/?scroll_to=upcoming-events') {
            document.getElementById('upcoming-events').scrollIntoView({behavior: 'smooth'});
        } else {
            this.router.navigateByUrl('/?scroll_to=upcoming-events');
        }
    }

    sendNewsletterEmail() {
        this.ajaxApi.apiPost('newsletter/subscribe', {email: this.newsletter_email}).then((res) => {
            this.toastr.success('Thanks for subscribing for our Newsletter!', 'Great');
            this.joinNewsletterActive = false;
        });
    }

    activeJoinNewsletter() {
        this.joinNewsletterActive = true;
        jQuery(this.joinFormWrap.nativeElement).focus();
    }
}

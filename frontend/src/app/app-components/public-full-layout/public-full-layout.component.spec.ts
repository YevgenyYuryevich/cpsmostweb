import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicFullLayoutComponent } from './public-full-layout.component';

describe('PublicFullLayoutComponent', () => {
  let component: PublicFullLayoutComponent;
  let fixture: ComponentFixture<PublicFullLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicFullLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicFullLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

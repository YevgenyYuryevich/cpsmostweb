import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';

@Component({
    selector: 'app-post-list-row',
    templateUrl: './post-list-row.component.html',
    styleUrls: ['./post-list-row.component.scss']
})
export class PostListRowComponent implements OnInit {

    @Input() post: any;
    @Output() updateClick = new EventEmitter<number>();
    @Output() delete = new EventEmitter<number>();
    constructor(public ajaxApi: AjaxApiService) { }

    ngOnInit() {
    }
    clickUpdate() {
        this.updateClick.emit(this.post.id);
    }
    updatePost(sets: any = false) {
        if (!sets) {
            sets = Object.assign({}, this.post);
            delete sets.id;
        } else {
            sets = Object.assign({}, sets);
        }
        if (typeof(sets.featured) !== 'undefined') {
            sets.featured = sets.featured ? 1 : 0;
        }
        if (typeof sets.permanent_resource !== 'undefined') {
            sets.permanent_resource = sets.permanent_resource ? 1 : 0;
        }
        const data = new FormData();
        data.append('where', JSON.stringify({id: this.post.id}));
        data.append('sets', JSON.stringify(sets));
        this.ajaxApi.apiMultiPartPost('posts/update', data).then((res) => {
        });
    }
    deletePost() {
        if (confirm('Are you sure to delete?')) {
            this.ajaxApi.apiPost('posts/delete/' + this.post.id).then(() => {
                this.delete.emit(this.post.id);
            });
        }
    }
}

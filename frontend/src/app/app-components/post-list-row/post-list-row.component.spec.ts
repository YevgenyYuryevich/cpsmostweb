import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostListRowComponent } from './post-list-row.component';

describe('PostListRowComponent', () => {
  let component: PostListRowComponent;
  let fixture: ComponentFixture<PostListRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostListRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostListRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

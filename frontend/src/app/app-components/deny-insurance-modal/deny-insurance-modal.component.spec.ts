import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenyInsuranceModalComponent } from './deny-insurance-modal.component';

describe('DenyInsuranceModalComponent', () => {
  let component: DenyInsuranceModalComponent;
  let fixture: ComponentFixture<DenyInsuranceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenyInsuranceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenyInsuranceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';

declare var jQuery: any;

@Component({
    selector: 'app-deny-insurance-modal',
    templateUrl: './deny-insurance-modal.component.html',
    styleUrls: ['./deny-insurance-modal.component.scss']
})
export class DenyInsuranceModalComponent implements OnInit {

    @ViewChild('denyInsuranceModal') domModal;
    @Output() onDeny = new EventEmitter<any>();
    @Input('options') options;

    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    message: string;
    insurance: any;
    constructor(private ajaxApi: AjaxApiService) {
    }

    ngOnInit() {
        this.message = this.options.insurance_deny_msg;
    }
    cancel() {}
    deny() {
        const data = {
            message: this.message,
            id: this.insurance.id,
        };
        this.ajaxApi.apiPost('insurances/deny', data).then(res => {
            jQuery(this.domModal.nativeElement).modal('hide');
            this.insurance.state = 'denied';
            this.onDeny.emit(this.insurance);
        });
    }
    open(insurance) {
        this.insurance = insurance;
        jQuery(this.domModal.nativeElement).modal('show');
    }
}

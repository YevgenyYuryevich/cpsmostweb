import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { WebSearchSelectableComponent } from './web-search-selectable/web-search-selectable.component';
import { WebSearchSelectableRowComponent } from './web-search-selectable-row/web-search-selectable-row.component';
import { WebSearchSelectableItemComponent } from './web-search-selectable-item/web-search-selectable-item.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import { FileUploadRowComponent } from './file-upload-row/file-upload-row.component';
import {FormsModule} from '@angular/forms';
import { PostListRowComponent } from './post-list-row/post-list-row.component';
import { PostEditorComponent } from './post-editor/post-editor.component';
import { NgxEditorModule } from './ngx-editor/src/app/ngx-editor/ngx-editor.module';
import { PostGridItemComponent } from './post-grid-item/post-grid-item.component';
import { CourseTypeEditorComponent } from './course-type-editor/course-type-editor.component';
import { CourseProviderEditorComponent } from './course-provider-editor/course-provider-editor.component';
import { CourseEditorComponent } from './course-editor/course-editor.component';
import {UiSwitchModule} from 'ngx-ui-switch';
import { PostViewComponent } from './post-view/post-view.component';
import { PublicFullLayoutComponent } from './public-full-layout/public-full-layout.component';
import { PublicHeaderComponent } from './public-header/public-header.component';
import { PublicFooterComponent } from './public-footer/public-footer.component';
import {RouterModule} from '@angular/router';
import { PublicSidebarComponent } from './public-sidebar/public-sidebar.component';
import { SignupForNewsletterComponent } from './signup-for-newsletter/signup-for-newsletter.component';
import { PreloaderComponent } from './preloader/preloader.component';
import { ApproveInsuranceModalComponent } from './approve-insurance-modal/approve-insurance-modal.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {PipesModule} from '../pipes/pipes.module';
import { DenyInsuranceModalComponent } from './deny-insurance-modal/deny-insurance-modal.component';
import { ApproveRenewalModalComponent } from './approve-renewal-modal/approve-renewal-modal.component';
import { DenyRenewalModalComponent } from './deny-renewal-modal/deny-renewal-modal.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import {ConfirmaDialogService} from './confirm-dialog/confirm-dialog.service';
import {ApproveRiderCoachCertificationModalComponent} from './approve-rider-coach-certification-modal/approve-rider-coach-certification-modal.component';
import {DenyRiderCoachCertificationModalComponent} from './deny-rider-coach-certification-modal/deny-rider-coach-certification-modal.component';

@NgModule({
    imports: [
        CommonModule,
        SlickCarouselModule,
        FormsModule,
        NgxEditorModule,
        UiSwitchModule,
        RouterModule,
        NgbModule,
        AngularFontAwesomeModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        PipesModule,
    ],
    providers: [
        DatePipe,
        ConfirmaDialogService
    ],
    declarations: [
        WebSearchSelectableComponent,
        WebSearchSelectableRowComponent,
        WebSearchSelectableItemComponent,
        FileUploadRowComponent,
        PostListRowComponent,
        PostEditorComponent,
        PostGridItemComponent,
        CourseTypeEditorComponent,
        CourseProviderEditorComponent,
        CourseEditorComponent,
        PostViewComponent,
        PublicFullLayoutComponent,
        PublicHeaderComponent,
        PublicFooterComponent,
        PublicSidebarComponent,
        SignupForNewsletterComponent,
        PreloaderComponent,
        ApproveInsuranceModalComponent,
        DenyInsuranceModalComponent,
        ApproveRiderCoachCertificationModalComponent,
        DenyRiderCoachCertificationModalComponent,
        ApproveRenewalModalComponent,
        DenyRenewalModalComponent,
        ConfirmDialogComponent],
    exports: [
        WebSearchSelectableComponent,
        FileUploadRowComponent,
        PostListRowComponent,
        PostEditorComponent,
        PostGridItemComponent,
        CourseTypeEditorComponent,
        CourseProviderEditorComponent,
        CourseEditorComponent,
        PostViewComponent,
        PublicFullLayoutComponent,
        PublicHeaderComponent,
        PublicFooterComponent,
        PublicSidebarComponent,
        SignupForNewsletterComponent,
        PreloaderComponent,
        ApproveInsuranceModalComponent,
        DenyInsuranceModalComponent,
        ApproveRiderCoachCertificationModalComponent,
        DenyRiderCoachCertificationModalComponent,
        ApproveRenewalModalComponent,
        DenyRenewalModalComponent,
        ConfirmDialogComponent
    ],
    entryComponents: [
        ConfirmDialogComponent
    ]
})
export class AppComponentsModule { }

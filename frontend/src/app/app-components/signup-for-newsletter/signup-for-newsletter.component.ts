import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
import {ActivatedRoute} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-signup-for-newsletter',
    templateUrl: './signup-for-newsletter.component.html',
    styleUrls: ['./signup-for-newsletter.component.scss']
})
export class SignupForNewsletterComponent implements OnInit {

    newsletter_email = '';
    constructor(private ajaxApi: AjaxApiService, private toastr: ToastrService) { }

    ngOnInit() {
    }
    sendNewsletterEmail() {
        this.ajaxApi.apiPost('newsletter/subscribe', {email: this.newsletter_email}).then((res) => {
            this.toastr.success('Thanks for subscribing for our Newsletter!', 'Great');
        });
    }
}

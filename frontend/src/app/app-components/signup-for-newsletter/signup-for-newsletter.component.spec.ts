import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupForNewsletterComponent } from './signup-for-newsletter.component';

describe('SignupForNewsletterComponent', () => {
  let component: SignupForNewsletterComponent;
  let fixture: ComponentFixture<SignupForNewsletterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupForNewsletterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupForNewsletterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenyRiderCoachCertificationModalComponent } from './deny-rider-coach-certification-modal.component';

describe('DenyInsuranceModalComponent', () => {
  let component: DenyRiderCoachCertificationModalComponent;
  let fixture: ComponentFixture<DenyRiderCoachCertificationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenyRiderCoachCertificationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenyRiderCoachCertificationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

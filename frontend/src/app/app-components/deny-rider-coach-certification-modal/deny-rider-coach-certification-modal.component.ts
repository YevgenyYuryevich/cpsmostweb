import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';

declare var jQuery: any;

@Component({
    selector: 'app-deny-rider-coach-certification-modal',
    templateUrl: './deny-rider-coach-certification-modal.component.html',
    styleUrls: ['./deny-rider-coach-certification-modal.component.scss']
})
export class DenyRiderCoachCertificationModalComponent implements OnInit {

    @ViewChild('denyInsuranceModal') domModal;
    @Output() onDeny = new EventEmitter<any>();
    @Input('options') options;

    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    message: string;
    riderCoachCertification: any;
    constructor(private ajaxApi: AjaxApiService) {
    }

    ngOnInit() {
        this.message = this.options.insurance_deny_msg;
    }
    cancel() {}
    deny() {
        const data = {
            message: this.message,
            id: this.riderCoachCertification.id,
        };
        this.ajaxApi.apiPost('rider-coach-certification/deny', data).then(res => {
            jQuery(this.domModal.nativeElement).modal('hide');
            this.riderCoachCertification.state = 'denied';
            this.onDeny.emit(this.riderCoachCertification);
        });
    }
    open(riderCoachCertification) {
        this.riderCoachCertification = riderCoachCertification;
        jQuery(this.domModal.nativeElement).modal('show');
    }
}

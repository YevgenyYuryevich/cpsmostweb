import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';

declare var jQuery: any;

@Component({
    selector: 'app-course-provider-editor',
    templateUrl: './course-provider-editor.component.html',
    styleUrls: ['./course-provider-editor.component.scss']
})
export class CourseProviderEditorComponent implements OnInit {
    @Output() create = new EventEmitter<any>();
    @Output() update = new EventEmitter<any>();
    @ViewChild('courseProviderEditorModal') domModal;
    courseProvider: any = {};
    orgCourseProvider: any = {};
    mode = 'create';
    coverImageSrc: any;
    imageFile: any;
    constructor(private ajaxApi: AjaxApiService) { }

    ngOnInit() {
    }
    open() {
        jQuery(this.domModal.nativeElement).modal('show');
    }
    createProvider() {
        const ajaxData = new FormData();
        ajaxData.append('sets', JSON.stringify(this.courseProvider));
        if (this.imageFile) {
            ajaxData.append('image', this.imageFile);
        }
        return this.ajaxApi.apiMultiPartPost('course-providers/insert', ajaxData).then( res => {
            jQuery(this.domModal.nativeElement).modal('hide');
            this.create.emit(res);
        });
    }
    saveCourseProvider() {
        const ajaxData = new FormData();
        const sets = Object.assign({}, this.courseProvider);
        delete sets.id;
        ajaxData.append('where', JSON.stringify({id: this.courseProvider.id}));
        ajaxData.append('sets', JSON.stringify(sets));
        if (this.imageFile) {
            ajaxData.append('image', this.imageFile);
        }
        return this.ajaxApi.apiMultiPartPost('course-providers/update', ajaxData).then( res => {
            Object.assign(this.courseProvider, res);
            jQuery(this.domModal.nativeElement).modal('hide');
            this.update.emit(this.courseProvider);
        });
    }
    setMode(m) {
        this.mode = m;
        if (this.mode === 'create') {
            this.courseProvider = {};
        }
    }
    setCourseProvider(p) {
        this.orgCourseProvider = Object.assign({}, p);
        this.courseProvider = p;
    }
    cancel() {
        Object.assign(this.courseProvider, this.orgCourseProvider);
    }
    updateCoverImage(event) {
        if (event.target.files && event.target.files[0]) {
            this.imageFile = event.target.files[0];
            const reader: any = new FileReader();
            reader.onload = e => this.coverImageSrc = reader.result;
            reader.readAsDataURL(this.imageFile);
        } else {
            this.imageFile = false;
        }
    }
}

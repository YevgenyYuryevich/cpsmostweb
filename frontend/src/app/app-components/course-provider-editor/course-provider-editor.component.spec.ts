import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseProviderEditorComponent } from './course-provider-editor.component';

describe('CourseProviderEditorComponent', () => {
  let component: CourseProviderEditorComponent;
  let fixture: ComponentFixture<CourseProviderEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseProviderEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseProviderEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

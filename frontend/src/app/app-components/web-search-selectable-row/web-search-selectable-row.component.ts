import {Component, Input, OnInit} from '@angular/core';
import {HelperService} from '../../services/helper.service';

@Component({
    selector: 'app-web-search-selectable-row',
    templateUrl: './web-search-selectable-row.component.html',
    styleUrls: ['./web-search-selectable-row.component.scss']
})
export class WebSearchSelectableRowComponent implements OnInit {
    _items: any[] = [];
    @Input() from: string;
    @Input()
    set items(items: any[]) {
        this._items = items.map((item) => {
            return this.appHelper.formatWebItem(item, this.from);
        });
    }
    get items() {
        return this._items;
    }
    slideConfig = {
        slidesToShow: 5,
        slidesToScroll: 4,
        centerMode: false,
        infinite: false,
        lazyLoad: 'progressive'
    };
    constructor(public appHelper: HelperService) {
    }
    selectedItems() {
        return this._items.filter((item) => {
            return item.selected;
        });
    }
    newItems() {
        return this._items.filter((item) => {
            return item.selected && !item.saved;
        });
    }
    fillAsSaved() {
        const nItems = this.newItems();
        nItems.forEach((item) => {
           item.saved = true;
        });
    }
    ngOnInit() {
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebSearchSelectableRowComponent } from './web-search-selectable-row.component';

describe('WebSearchSelectableRowComponent', () => {
  let component: WebSearchSelectableRowComponent;
  let fixture: ComponentFixture<WebSearchSelectableRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebSearchSelectableRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebSearchSelectableRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

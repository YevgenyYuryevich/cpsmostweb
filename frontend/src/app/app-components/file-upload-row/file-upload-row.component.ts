import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HelperService} from '../../services/helper.service';
import {AjaxApiService} from '../../services/ajax-api.service';

@Component({
    selector: 'app-file-upload-row',
    templateUrl: './file-upload-row.component.html',
    styleUrls: ['./file-upload-row.component.scss']
})
export class FileUploadRowComponent implements OnInit {

    @Input() file: any;
    @Output() itemDeleted = new EventEmitter<void>();
    item: any = {
        canConvert: false,
        converted: false,
        uploadedUrl: false,
        fileType: false,
        convertedHtml: false
    };
    imageSrc: any = '';
    imageFile: any = '';
    constructor(public appHelper: HelperService, private ajaxApi: AjaxApiService) {
    }

    ngOnInit() {
        this.makePost();
    }
    makePost() {
        this.item = {};
        this.item.title = this.file.name;
        this.item.fileType = this.appHelper.extractTypeFromFileType(this.file.type);
        switch (this.item.fileType) {
            case 'video':
                this.item.type = 'video';
                break;
            case 'audio':
                this.item.type = 'audio';
                break;
            case 'image':
                this.updateCoverImage(this.file);
                this.item.type = 'text';
                this.item.content = 'created by image';
                break;
            case 'application':
            case 'text':
                this.item.canConvert = true;
                this.item.type = 'text';
                this.uploadFile().then((url) => {
                    this.item.uploadedUrl = url;
                    this.item.content = url;
                });
                break;
        }
    }
    uploadFile(f = this.file, where = 'media/' + this.item.fileType) {
        const ajaxData = new FormData();
        ajaxData.append('file', f);
        ajaxData.append('where', where);
        return this.ajaxApi.apiMultiPartPost('upload', ajaxData).then((res) => {
            return res.data;
        });
    }
    updateCoverImage(f) {
        this.imageFile = f;
        if (f) {
            const reader: any = new FileReader();
            reader.onload = e => this.imageSrc = reader.result;
            reader.readAsDataURL(f);
        }
    }
    updateImageFile(event) {
        if (event.target.files && event.target.files[0]) {
            this.imageFile = event.target.files[0];
            this.updateCoverImage(this.imageFile);
        } else {
            this.imageFile = false;
        }
    }
    deleteItem() {
        this.itemDeleted.emit();
    }
    upload() {
        let resPromise = Promise.resolve(true);
        if (this.item.type !== 'text') {
            resPromise = this.uploadFile(this.file).then((res) => {
                this.item.uploadedUrl = res;
                this.item.content = res;
                return true;
            }, () => {
                alert('Sorry, This file is too large size to upload');
                return false;
            });
        }
        return resPromise.then((rlt) => {
            if (rlt) {
                if (this.imageFile) {
                    return this.uploadFile(this.imageFile, 'posts').then((res) => {
                        this.item.image = res;
                        return this.item;
                    });
                }
                return this.item;
            }
        });
    }
    asConvert() {
        if (this.item.convertedHtml) {
            this.item.content = this.item.convertedHtml;
            this.item.converted = true;
        } else {
            this.htmlConvert().then(() => {
                this.item.converted = true;
            });
        }
    }
    asKeep() {
        this.item.content = '<a href="' + this.item.uploadedUrl + '" target="_self">view detail</a>';
        this.item.converted = false;
    }
    htmlConvert() {
        const ajaxData = new FormData();
        ajaxData.append('file', this.file);
        return this.ajaxApi.apiMultiPartPost('file_to_html', ajaxData).then((res) => {
            this.item.content = res.data;
            this.item.convertedHtml = res.data;
            console.log(res);
        });
    }
}

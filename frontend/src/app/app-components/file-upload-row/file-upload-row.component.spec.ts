import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadRowComponent } from './file-upload-row.component';

describe('FileUploadRowComponent', () => {
  let component: FileUploadRowComponent;
  let fixture: ComponentFixture<FileUploadRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileUploadRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

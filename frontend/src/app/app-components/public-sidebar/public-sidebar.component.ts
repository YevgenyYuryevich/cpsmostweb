import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {AppCommonService} from '../../services/app-common.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-public-sidebar',
  templateUrl: './public-sidebar.component.html',
  styleUrls: ['./public-sidebar.component.scss']
})
export class PublicSidebarComponent implements OnInit {
  @ViewChild('socialInstagramGrid') socialInstagramGrid: ElementRef;
  @ViewChild('socialFacebook') socialFacebook: ElementRef;
  @Input('courseTypes') courseTypes: any[];
  @Input('socialFeed') socialFeed: any;

  eventbriteEvents: any[] = [];
  instagramPhotos: any[] = [];
  twitterTweets: any[] = [];
  facebookFeeds: any[];
  permanentResources: any[];

  socialInstagramHeight = '300px';
  facebookReady = false;
  facebookEmbedUrl;
  socialFacebookWidth = 300;
  constructor(private appCommonService: AppCommonService, private sanitizer: DomSanitizer) {
    this.permanentResources = this.appCommonService.permanentResources;
  }

  ngOnInit() {
    if (this.socialFeed.eventbrite_events) {
      this.eventbriteEvents = JSON.parse(this.socialFeed.eventbrite_events).map((item: any) => {
        item.startTime = new Date(item.start_time);
        item.endTime = new Date(item.end.utc);
        item.eventDur = Math.round((item.endTime - item.startTime) / (1000 * 60 * 60 * 24)) + 1;
        return item;
      });
      this.eventbriteEvents = this.eventbriteEvents.filter(e => {
        const now = new Date();
        now.setDate(now.getDate() - 1);
        return e.startTime.getTime() > now.getTime();
      });
    } else {
      this.eventbriteEvents = [];
    }
    this.instagramPhotos = this.socialFeed.instagram_photos ? JSON.parse(this.socialFeed.instagram_photos) : [];
    this.twitterTweets = this.socialFeed.twitter_tweets ? JSON.parse(this.socialFeed.twitter_tweets) : {};
    this.facebookFeeds = this.socialFeed.facebook_feeds ? JSON.parse(this.socialFeed.facebook_feeds) : [];
    this.facebookFeeds = this.facebookFeeds.map(feed => {
      feed.attachment = feed.attachments.data[0];
      return feed;
    });
    this.socialInstagramHeight = this.socialInstagramGrid.nativeElement.offsetWidth + 'px';
    this.socialFacebookWidth = this.socialFacebook.nativeElement.offsetWidth;
    this.facebookEmbedUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FMotorcylesafety&tabs=timeline&width=' + this.socialFacebookWidth + '&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=785428811857929');
    this.facebookReady = true;
  }
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';

@Component({
    selector: 'app-post-grid-item',
    templateUrl: './post-grid-item.component.html',
    styleUrls: ['./post-grid-item.component.scss']
})
export class PostGridItemComponent implements OnInit {

    @Input() post: any;
    @Output() delete = new EventEmitter<number>();
    constructor(private ajaxApi: AjaxApiService) { }

    ngOnInit() {
    }
    deletePost() {
        if (confirm('Are you sure to delete?')) {
            this.ajaxApi.apiPost('posts/delete/' + this.post.id).then(() => {
                this.delete.emit(this.post.id);
            });
        }
    }
}

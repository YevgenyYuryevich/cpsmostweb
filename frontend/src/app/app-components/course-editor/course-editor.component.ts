import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
declare var jQuery: any;

@Component({
    selector: 'app-course-editor',
    templateUrl: './course-editor.component.html',
    styleUrls: ['./course-editor.component.scss']
})
export class CourseEditorComponent implements OnInit {

    @Input() types;
    @Input() providers;
    @Output() update = new EventEmitter<any>();
    @ViewChild('courseEditorModal') modalRoot;
    course: any = {};
    orgCourse: any = {};
    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    mode = 'edit';
    imageFile: any;
    coverImageSrc: any;
    constructor(private ajaxApi: AjaxApiService) { }

    ngOnInit() {
    }
    createCourse() {
    }
    saveCourse() {
        const ajaxData = new FormData();
        const sets = Object.assign({}, this.course);
        delete sets.id;
        delete sets.typeName;
        delete sets.providerCompanyName;
        ajaxData.append('sets', JSON.stringify(sets));
        ajaxData.append('where', JSON.stringify({id: this.course.id}));
        if (this.imageFile) {
            ajaxData.append('image', this.imageFile);
        }
        this.ajaxApi.apiMultiPartPost('courses/update', ajaxData).then(res => {
            Object.assign(this.course, res);
            jQuery(this.modalRoot.nativeElement).modal('hide');
            this.update.emit(this.course);
        });
    }
    cancel() {
        Object.assign(this.course, this.orgCourse);
    }
    setCourse(c) {
        this.orgCourse = Object.assign({}, c);
        this.course = c;
    }
    open() {
        jQuery(this.modalRoot.nativeElement).modal('show');
    }
    updateCoverImage(event) {
        if (event.target.files && event.target.files[0]) {
            this.imageFile = event.target.files[0];
            const reader: any = new FileReader();
            reader.onload = e => this.coverImageSrc = reader.result;
            reader.readAsDataURL(this.imageFile);
        } else {
            this.imageFile = false;
        }
    }
}

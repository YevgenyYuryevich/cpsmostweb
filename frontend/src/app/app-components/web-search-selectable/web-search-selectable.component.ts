import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';

@Component({
    selector: 'app-web-search-selectable',
    templateUrl: './web-search-selectable.component.html',
    styleUrls: ['./web-search-selectable.component.scss']
})
export class WebSearchSelectableComponent implements OnInit {
    @ViewChild('youtubeRow') youtubeRowComponent;
    @ViewChild('podcastsRow') podcastsRowComponent;
    @ViewChild('blogsRow') blogsRowComponent;
    @Output() onSave = new EventEmitter<any>();
    q = '';
    searchData: any = {
        youtube: {
            items: [],
        },
        spreaker: {
            items: []
        },
        blogs: {
            items: []
        }
    };
    selectedItems: any[] = [];
    newItems: any[] = [];
    constructor(private ajaxApi: AjaxApiService) {
    }
    ngOnInit() {
    }
    searchWeb(q = this.q) {
        const ajaxData = {
            q: q,
            size: 50,
            items: [
                {name: 'youtube', token: false},
                {name: 'spreaker', token: false},
                {name: 'blogs', token: false},
            ]
        };
        this.ajaxApi.apiPost('search', ajaxData).then((res) => {
            Object.assign(this.searchData, res.data);
        });
    }
    recordSelected() {
        this.selectedItems = this.selectedItems.concat(this.youtubeRowComponent.newItems());
        this.selectedItems = this.selectedItems.concat(this.podcastsRowComponent.newItems());
        this.selectedItems = this.selectedItems.concat(this.blogsRowComponent.newItems());
        this.newItems = this.newItems.concat(this.youtubeRowComponent.newItems());
        this.newItems = this.newItems.concat(this.podcastsRowComponent.newItems());
        this.newItems = this.newItems.concat(this.blogsRowComponent.newItems());
        this.youtubeRowComponent.fillAsSaved();
        this.podcastsRowComponent.fillAsSaved();
        this.blogsRowComponent.fillAsSaved();
    }
    addSelected() {
        this.recordSelected();
        this.onSave.emit(this.newItems);
        this.newItems = [];
    }
}

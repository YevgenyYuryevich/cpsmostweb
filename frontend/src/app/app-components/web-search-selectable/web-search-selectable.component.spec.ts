import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebSearchSelectableComponent } from './web-search-selectable.component';

describe('WebSearchSelectableComponent', () => {
  let component: WebSearchSelectableComponent;
  let fixture: ComponentFixture<WebSearchSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebSearchSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebSearchSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-public-footer',
    templateUrl: './public-footer.component.html',
    styleUrls: ['./public-footer.component.scss']
})
export class PublicFooterComponent implements OnInit {

    @Input('social') social;
    constructor() { }

    ngOnInit() {
    }

}

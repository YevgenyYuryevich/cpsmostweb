import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
declare var jQuery: any;
declare var videojs: any;

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {

  @ViewChild('postView') domModal: ElementRef;
  @ViewChild('postViewBody') domBody: ElementRef;
  post: any = {};
  jqVideoDom: any;
  videoDomClone: any;
  videoPlayer: any;
  constructor() { }

  ngOnInit() {
    this.jqVideoDom = jQuery(this.domBody.nativeElement).find('video');
    jQuery(this.domModal.nativeElement).on('hidden.bs.modal', (e) => {
      this.empty();
    });
  }
  setPost(p) {
    this.post = p;
    if (this.post.type === 'video') {
      this.setupBodyVideo();
    }
  }
  setupBodyVideo() {
    this.videoDomClone = this.jqVideoDom.clone();
    if (this.post.content.match(/^(https:\/\/www.youtube.com)/)) {
      const setup = {
        techOrder: ['youtube'],
        sources: [
          { type: 'video/youtube',
            src: this.post.content
          }]
      };
      this.jqVideoDom.attr('data-setup', JSON.stringify(setup));
    } else {
      this.jqVideoDom.append('<source src="' + this.post.content + '"/>');
    }
    const pWidth = parseInt(jQuery(this.domModal.nativeElement).find('.modal-dialog').css('max-width'));
    const padding = parseInt(jQuery(this.domBody.nativeElement).css('padding-left'));
    const height = (pWidth - padding * 2) * 540 / 960;
    this.videoPlayer = videojs(this.jqVideoDom.get(0), {
      height: height
    });
  }
  open() {
    jQuery(this.domModal.nativeElement).modal('show');
  }
  empty() {
    if (this.post.type === 'video') {
      this.videoPlayer.dispose();
      this.jqVideoDom = this.videoDomClone.appendTo(jQuery(this.domBody.nativeElement).find('.video-wrapper'));
    }
  }
}

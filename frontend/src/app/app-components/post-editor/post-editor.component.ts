import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {AjaxApiService} from '../../services/ajax-api.service';
import {HelperService} from '../../services/helper.service';
declare var jQuery: any;

@Component({
    selector: 'app-post-editor',
    templateUrl: './post-editor.component.html',
    styleUrls: ['./post-editor.component.scss']
})
export class PostEditorComponent implements OnInit {

    @ViewChild('content') content: ElementRef;
    closeResult: string;
    post: any = {};
    orgPost: any;
    imageFile: any;
    coverImageSrc: any;
    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    private modalRef: NgbModalRef;
    constructor(private ajaxApi: AjaxApiService, private appHelper: HelperService, private modalService: NgbModal) { }

    ngOnInit() {
    }
    open() {
        this.modalRef = this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
        this.modalRef.result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    setPost(p) {
        this.orgPost = Object.assign({}, p);
        this.post = p;
    }
    savePost() {
        let sets = Object.assign({}, this.post);
        delete sets.id;
        const ajaxData = new FormData();
        const where = JSON.stringify({id: this.post.id});
        sets = JSON.stringify(sets);
        ajaxData.append('encoded', '1');
        ajaxData.append('where', this.appHelper.utf8ToBase64(where));
        ajaxData.append('sets', this.appHelper.utf8ToBase64(sets));
        if (this.imageFile) {
            ajaxData.append('image', this.imageFile);
        }
        this.ajaxApi.apiMultiPartPost('posts/update', ajaxData).then((res) => {
            Object.assign(this.post, res);
            this.modalRef.close();
        });
    }
    cancel() {
        Object.assign(this.post, this.orgPost);
        this.modalRef.close();
    }
    updateCoverImage(event) {
        if (event.target.files && event.target.files[0]) {
            this.imageFile = event.target.files[0];
            const reader: any = new FileReader();
            reader.onload = e => this.coverImageSrc = reader.result;
            reader.readAsDataURL(this.imageFile);
        } else {
            this.imageFile = false;
        }
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }
    onMediaUploaded(media) {
        if (media.url !== this.post.content) {
            if (media.url + '<br>' === this.post.content) {
                this.post.content = media.url;
            } else {
                return false;
            }
        }
        switch (media.type) {
            case 'audio':
                this.post.type = 'audio';
                break;
            case 'video':
                this.post.type = 'video';
                break;
            default:
                break;
        }
    }
}

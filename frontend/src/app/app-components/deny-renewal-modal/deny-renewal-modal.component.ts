import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';

declare var jQuery: any;

@Component({
    selector: 'app-deny-renewal-modal',
    templateUrl: './deny-renewal-modal.component.html',
    styleUrls: ['./deny-renewal-modal.component.scss']
})
export class DenyRenewalModalComponent implements OnInit {

    @ViewChild('denyRenewalModal') domModal;
    @Output() onDeny = new EventEmitter<any>();
    @Input('options') options;

    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    message: string;
    renewal: any;
    constructor(private ajaxApi: AjaxApiService) {
    }

    ngOnInit() {
        this.message = this.options.renewal_deny_msg;
    }
    cancel() {}
    deny() {
        const data = {
            message: this.message,
            id: this.renewal.id,
        };
        this.ajaxApi.apiPost('renewal-application/deny', data).then(res => {
            jQuery(this.domModal.nativeElement).modal('hide');
            this.renewal.state = 'denied';
            this.onDeny.emit(this.renewal);
        });
    }
    open(insurance) {
        this.renewal = insurance;
        jQuery(this.domModal.nativeElement).modal('show');
    }
}

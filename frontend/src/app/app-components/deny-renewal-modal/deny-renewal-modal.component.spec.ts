import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenyRenewalModalComponent } from './deny-renewal-modal.component';

describe('DenyRenewalModalComponent', () => {
  let component: DenyRenewalModalComponent;
  let fixture: ComponentFixture<DenyRenewalModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenyRenewalModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenyRenewalModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

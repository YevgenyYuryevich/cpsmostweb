import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-web-search-selectable-item',
    templateUrl: './web-search-selectable-item.component.html',
    styleUrls: ['./web-search-selectable-item.component.scss']
})
export class WebSearchSelectableItemComponent implements OnInit {
    @Input() item: any;
    constructor() { }

    ngOnInit() {
    }
    toggleSelected() {
        this.item.selected = !this.item.selected;
    }
}

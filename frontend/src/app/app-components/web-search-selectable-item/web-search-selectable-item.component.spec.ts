import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebSearchSelectableItemComponent } from './web-search-selectable-item.component';

describe('WebSearchSelectableItemComponent', () => {
  let component: WebSearchSelectableItemComponent;
  let fixture: ComponentFixture<WebSearchSelectableItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebSearchSelectableItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebSearchSelectableItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

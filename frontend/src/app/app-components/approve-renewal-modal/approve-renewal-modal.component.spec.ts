import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveRenewalModalComponent } from './approve-renewal-modal.component';

describe('ApproveRenewalModalComponent', () => {
  let component: ApproveRenewalModalComponent;
  let fixture: ComponentFixture<ApproveRenewalModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveRenewalModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveRenewalModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

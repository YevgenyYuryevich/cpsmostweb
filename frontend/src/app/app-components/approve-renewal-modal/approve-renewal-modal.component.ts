import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
import {DatePipe} from '@angular/common';

declare var jQuery: any;

@Component({
  selector: 'app-approve-renewal-modal',
  templateUrl: './approve-renewal-modal.component.html',
  styleUrls: ['./approve-renewal-modal.component.scss']
})
export class ApproveRenewalModalComponent implements OnInit {

    @ViewChild('approveRenewalModal') domModal;
    @Input('options') options;
    @Output() onApprove = new EventEmitter<any>();

    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    message: string;
    startDateObj: any = new Date();
    endDateObj: any;
    renewal: any;
    constructor(private ajaxApi: AjaxApiService, private datePipe: DatePipe) {
    }

    ngOnInit(): void {
        this.message = this.options.renewal_approve_msg;
    }

    cancel() {
    }
    approve() {
        const startDate = this.datePipe.transform(this.startDateObj, 'yyyy-MM-dd');
        const endDate = this.datePipe.transform(this.endDateObj, 'yyyy-MM-dd');
        const data = {
            id: this.renewal.id,
            start_date: startDate,
            end_date: endDate,
            message: this.message,
        };
        this.ajaxApi.apiPost('renewal-application/approve', data).then(res => {
            jQuery(this.domModal.nativeElement).modal('hide');
            this.renewal.state = 'approved';
            this.renewal.valid_from = data.start_date;
            this.renewal.valid_to = data.end_date;
            this.onApprove.emit(data);
        });
    }
    open(renewal) {
        this.renewal = renewal;
        jQuery(this.domModal.nativeElement).modal('show');
    }

}

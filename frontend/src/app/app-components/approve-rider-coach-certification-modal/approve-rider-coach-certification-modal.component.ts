import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
import {DatePipe} from '@angular/common';
declare var jQuery: any;

@Component({
    selector: 'app-approve-rider-coach-certification-modal',
    templateUrl: './approve-rider-coach-certification-modal.component.html',
    styleUrls: ['./approve-rider-coach-certification-modal.component.scss']
})
export class ApproveRiderCoachCertificationModalComponent implements OnInit {

    @ViewChild('approveInsuranceModal') domModal;
    @Input('options') options;
    @Output() onApprove = new EventEmitter<any>();

    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    message: string;
    startDateObj: any = new Date();
    endDateObj: any;
    riderCoachCertification: any;
    constructor(private ajaxApi: AjaxApiService, private datePipe: DatePipe) {
    }

    ngOnInit(): void {
        this.message = this.options.insurance_approve_msg;
    }

    cancel() {
    }
    approve() {
        const startDate = this.datePipe.transform(this.startDateObj, 'yyyy-MM-dd');
        const endDate = this.datePipe.transform(this.endDateObj, 'yyyy-MM-dd');
        const data = {
            id: this.riderCoachCertification.id,
            start_date: startDate,
            end_date: endDate,
            message: this.message,
        };
        this.ajaxApi.apiPost('rider-coach-certification/approve', data).then(res => {
            jQuery(this.domModal.nativeElement).modal('hide');
            this.riderCoachCertification.state = 'approved';
            this.riderCoachCertification.valid_from = data.start_date;
            this.riderCoachCertification.valid_to = data.end_date;
            this.onApprove.emit(data);
        });
    }
    open(riderCoachCertification) {
        this.riderCoachCertification = riderCoachCertification;
        jQuery(this.domModal.nativeElement).modal('show');
    }
}

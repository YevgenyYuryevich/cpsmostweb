import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveRiderCoachCertificationModalComponent } from './approve-insurance-modal.component';

describe('ApproveInsuranceModalComponent', () => {
  let component: ApproveRiderCoachCertificationModalComponent;
  let fixture: ComponentFixture<ApproveRiderCoachCertificationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveRiderCoachCertificationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveRiderCoachCertificationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveInsuranceModalComponent } from './approve-insurance-modal.component';

describe('ApproveInsuranceModalComponent', () => {
  let component: ApproveInsuranceModalComponent;
  let fixture: ComponentFixture<ApproveInsuranceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveInsuranceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveInsuranceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

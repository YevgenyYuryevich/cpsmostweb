import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
import {DatePipe} from '@angular/common';
declare var jQuery: any;

@Component({
    selector: 'app-approve-insurance-modal',
    templateUrl: './approve-insurance-modal.component.html',
    styleUrls: ['./approve-insurance-modal.component.scss']
})
export class ApproveInsuranceModalComponent implements OnInit {

    @ViewChild('approveInsuranceModal') domModal;
    @Input('options') options;
    @Output() onApprove = new EventEmitter<any>();

    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    message: string;
    startDateObj: any = new Date();
    endDateObj: any;
    insurance: any;
    constructor(private ajaxApi: AjaxApiService, private datePipe: DatePipe) {
    }

    ngOnInit(): void {
        this.message = this.options.insurance_approve_msg;
    }

    cancel() {
    }
    approve() {
        const startDate = this.datePipe.transform(this.startDateObj, 'yyyy-MM-dd');
        const endDate = this.datePipe.transform(this.endDateObj, 'yyyy-MM-dd');
        const data = {
            id: this.insurance.id,
            start_date: startDate,
            end_date: endDate,
            message: this.message,
        };
        this.ajaxApi.apiPost('insurances/approve', data).then(res => {
            jQuery(this.domModal.nativeElement).modal('hide');
            this.insurance.state = 'approved';
            this.insurance.valid_from = data.start_date;
            this.insurance.valid_to = data.end_date;
            this.onApprove.emit(data);
        });
    }
    open(insurance) {
        this.insurance = insurance;
        jQuery(this.domModal.nativeElement).modal('show');
    }
}

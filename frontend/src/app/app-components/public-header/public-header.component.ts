import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

declare var jQuery: any;

@Component({
    selector: 'app-public-header',
    templateUrl: './public-header.component.html',
    styleUrls: ['./public-header.component.scss']
})
export class PublicHeaderComponent implements OnInit {

    @Input('social') social;
    @Input('permanent_resources') permanent_resources;
    @Input() mobileSidebarOpened: boolean;
    @ViewChild('joinFormWrap') joinFormWrap: ElementRef;
    @Output() mobileSidebarOpenedChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    joinNewsletterActive = false;
    newsletter_email = '';
    constructor(private ajaxApi: AjaxApiService, private toastr: ToastrService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
    }
    sendNewsletterEmail() {
        this.ajaxApi.apiPost('newsletter/subscribe', {email: this.newsletter_email}).then((res) => {
            this.toastr.success('Thanks for subscribing for our Newsletter!', 'Great');
            this.joinNewsletterActive = false;
        });
    }
    activeJoinNewsletter() {
        this.joinNewsletterActive = true;
        jQuery(this.joinFormWrap.nativeElement).focus();
    }
    scrollToEvents() {
        if (this.router.url === '/' || this.router.url === '/?scroll_to=upcoming-events') {
            document.getElementById('upcoming-events').scrollIntoView({behavior: 'smooth'});
        } else {
            this.router.navigateByUrl('/?scroll_to=upcoming-events');
        }
    }
}

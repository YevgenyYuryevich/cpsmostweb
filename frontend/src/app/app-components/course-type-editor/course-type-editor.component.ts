import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../services/ajax-api.service';
import {HelperService} from '../../services/helper.service';
declare var jQuery: any;

@Component({
    selector: 'app-course-type-editor',
    templateUrl: './course-type-editor.component.html',
    styleUrls: ['./course-type-editor.component.scss']
})
export class CourseTypeEditorComponent implements OnInit {

    @ViewChild('courseTypeEditorModal') modalRoot;
    @Output() create = new EventEmitter<any>();
    @Output() update = new EventEmitter<any>();

    mode = 'create';
    courseType: any = {};
    orgCourseType: any = {};
    coverImageSrc: any;
    imageFile: any;
    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    constructor(private ajaxApi: AjaxApiService, private appHelper: HelperService) { }

    ngOnInit() {
    }
    saveType() {
        const ajaxData = new FormData();
        if (this.imageFile) {
            ajaxData.append('image', this.imageFile);
        }
        const sets = Object.assign({}, this.courseType);
        delete sets.id;
        if (sets.description) {
            sets.description = this.appHelper.utf8ToBase64(sets.description);
        }
        ajaxData.append('where', JSON.stringify({id: this.courseType.id}));
        ajaxData.append('sets', JSON.stringify( sets));
        this.ajaxApi.apiMultiPartPost('course-types/update', ajaxData).then((res) => {
            Object.assign(this.courseType, res);
            jQuery(this.modalRoot.nativeElement).modal('hide');
            this.update.emit(this.courseType);
        });
    }
    createType() {
        const ajaxData = new FormData();
        if (this.imageFile) {
            ajaxData.append('image', this.imageFile);
        }
        const sets = Object.assign({}, this.courseType);
        if (sets.description) {
            sets.description = this.appHelper.utf8ToBase64(sets.description);
        }
        ajaxData.append('sets', JSON.stringify(sets));
        this.ajaxApi.apiMultiPartPost('course-types/insert', ajaxData).then((res) => {
            jQuery(this.modalRoot.nativeElement).modal('hide');
            this.create.emit(res);
        });
    }
    cancel() {
        Object.assign(this.courseType, this.orgCourseType);
    }
    setMode(m) {
        this.mode = m;
        if (this.mode === 'create') {
            this.courseType = {};
        }
    }
    setCourseType(t) {
        this.orgCourseType = Object.assign({}, t);
        this.courseType = t;
    }
    updateCoverImage(event) {
        if (event.target.files && event.target.files[0]) {
            this.imageFile = event.target.files[0];
            const reader: any = new FileReader();
            reader.onload = e => this.coverImageSrc = reader.result;
            reader.readAsDataURL(this.imageFile);
        } else {
            this.imageFile = false;
        }
    }
    open() {
        jQuery(this.modalRoot.nativeElement).modal('show');
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseTypeEditorComponent } from './course-type-editor.component';

describe('CourseTypeEditorComponent', () => {
  let component: CourseTypeEditorComponent;
  let fixture: ComponentFixture<CourseTypeEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseTypeEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseTypeEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

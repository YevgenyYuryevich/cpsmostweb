import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TableSearchPipe} from './table-search.pipe';
import { ArraySortPipe } from './array-sort.pipe';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        TableSearchPipe,
        ArraySortPipe
    ],
    exports: [
        TableSearchPipe,
        ArraySortPipe,
    ],
})
export class PipesModule { }

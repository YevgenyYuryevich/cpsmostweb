import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'arraySort'
})
export class ArraySortPipe implements PipeTransform {

    transform(value: any, args?: any, dir = true): any {
        return value.sort((a, b) => {
            if (a[args] > b[args]) {
                return dir ? 1 : -1;
            } else if (a[args] < b[args]) {
                return dir ? -1 : 1;
            }
            return 0;
        });
    }
}

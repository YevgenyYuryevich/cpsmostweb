import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'tableSearch',
})
export class TableSearchPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        return value.filter((item) => {
            return this.isFiltered(item, args);
        });
    }
    isFiltered(item, q) {
        if (!q) {
            return true;
        }
        for ( const k in item ) {
            let v = '' + item[k];
            v = v.toLowerCase();
            if (v.indexOf(q.toLowerCase()) > -1) {
                return true;
            }
        }
        return false;
    }
}

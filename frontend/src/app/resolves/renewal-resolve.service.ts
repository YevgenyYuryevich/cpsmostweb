import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AjaxApiService} from '../services/ajax-api.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RenewalResolveService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        const id = route.paramMap.get('id');
        return this.ajaxApi.apiPost('renewal-application/get', {where: {id: id}});
    }
}

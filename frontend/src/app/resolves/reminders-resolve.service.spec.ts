import { TestBed } from '@angular/core/testing';

import { RemindersResolveService } from './reminders-resolve.service';

describe('RemindersResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemindersResolveService = TestBed.get(RemindersResolveService);
    expect(service).toBeTruthy();
  });
});

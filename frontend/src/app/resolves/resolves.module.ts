import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsResolveService } from './posts-resolve.service';
import { CourseProviderResolveService } from './course-provider-resolve.service';
import { CourseTypeResolveService } from './course-type-resolve.service';
import { CoursesResolveService } from './courses-resolve.service';
import { SocialResolveService } from './social-resolve.service';
import { GetCourseTypeResolveService } from './get-course-type-resolve.service';
import { EventbriteOptionsResolveService } from './eventbrite-options-resolve.service';
import { SocialFeedResolveService } from './social-feed-resolve.service';
import { TwitterOptionsResolveService } from './twitter-options-resolve.service';
import { PostResolveService } from './post-resolve.service';
import { RemindersResolveService } from './reminders-resolve.service';
import { IncidentsResolveService } from './incidents-resolve.service';
import { IncidentResolveService } from './incident-resolve.service';
import { InstructorsResolveService } from './instructors-resolve.service';
import { InsurancesResolveService } from './insurances-resolve.service';
import { InsuranceResolveService } from './insurance-resolve.service';
import { InsuranceOptionsResolveService } from './insurance-options-resolve.service';
import { RenewalsResolveService } from './renewals-resolve.service';
import { RenewalResolveService } from './renewal-resolve.service';
import { RenewalOptionsResolveService } from './renewal-options-resolve.service';
import { AnalyticsCodeResolveService } from './analytics-code-resolve.service';
import { CourseTracksResolveService } from './course-tracks-resolve.service';
import { NewsletterEmailsResolveService } from './newsletter-emails-resolve.service';
import {FacebookOptionsResolveService} from './facebook-options-resolve.service';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    providers: [
        PostsResolveService,
        CourseProviderResolveService,
        CourseTypeResolveService,
        CoursesResolveService,
        SocialResolveService,
        GetCourseTypeResolveService,
        EventbriteOptionsResolveService,
        SocialFeedResolveService,
        TwitterOptionsResolveService,
        PostResolveService,
        RemindersResolveService,
        IncidentsResolveService,
        IncidentResolveService,
        InstructorsResolveService,
        InsurancesResolveService,
        InsuranceResolveService,
        InsuranceOptionsResolveService,
        RenewalsResolveService,
        RenewalResolveService,
        RenewalOptionsResolveService,
        AnalyticsCodeResolveService,
        CourseTracksResolveService,
        NewsletterEmailsResolveService,
        FacebookOptionsResolveService
    ]
})
export class ResolvesModule { }

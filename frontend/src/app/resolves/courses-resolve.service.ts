import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable({
  providedIn: 'root'
})
export class CoursesResolveService implements Resolve<any> {

  constructor(private ajaxApi: AjaxApiService) { }
  resolve(route: ActivatedRouteSnapshot) {
      const type_id = route.paramMap.get('type_id');
      if (type_id) {
          return this.ajaxApi.apiPost('courses/get-many', {where: {type: type_id}});
      } else {
          return this.ajaxApi.apiPost('courses/get-many');
      }
  }
}

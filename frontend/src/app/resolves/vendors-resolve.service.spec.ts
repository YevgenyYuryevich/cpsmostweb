import { TestBed } from '@angular/core/testing';

import { VendorsResolveService } from './vendors-resolve.service';

describe('VendorsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VendorsResolveService = TestBed.get(VendorsResolveService);
    expect(service).toBeTruthy();
  });
});

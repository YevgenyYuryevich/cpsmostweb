import { TestBed } from '@angular/core/testing';

import { IncidentResolveService } from './incident-resolve.service';

describe('IncidentResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncidentResolveService = TestBed.get(IncidentResolveService);
    expect(service).toBeTruthy();
  });
});

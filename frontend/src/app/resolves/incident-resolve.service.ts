import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable({
    providedIn: 'root'
})
export class IncidentResolveService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        const id = route.paramMap.get('id');
        return this.ajaxApi.apiPost('incidents/get', {where: {id: id}});
    }
}

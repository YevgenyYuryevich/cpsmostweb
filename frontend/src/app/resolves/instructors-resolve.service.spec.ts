import { TestBed } from '@angular/core/testing';

import { InstructorsResolveService } from './instructors-resolve.service';

describe('InstructorsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstructorsResolveService = TestBed.get(InstructorsResolveService);
    expect(service).toBeTruthy();
  });
});

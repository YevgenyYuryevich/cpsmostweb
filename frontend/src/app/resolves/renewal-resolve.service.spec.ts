import { TestBed } from '@angular/core/testing';

import { RenewalResolveService } from './renewal-resolve.service';

describe('RenewalResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RenewalResolveService = TestBed.get(RenewalResolveService);
    expect(service).toBeTruthy();
  });
});

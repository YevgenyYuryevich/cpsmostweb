import { TestBed } from '@angular/core/testing';

import { CourseProviderResolveService } from './course-provider-resolve.service';

describe('CourseProviderResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseProviderResolveService = TestBed.get(CourseProviderResolveService);
    expect(service).toBeTruthy();
  });
});

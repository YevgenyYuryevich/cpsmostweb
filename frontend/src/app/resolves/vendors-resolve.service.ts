import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable({
    providedIn: 'root'
})
export class VendorsResolveService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) {
    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return this.ajaxApi.apiPost('users/get-vendors');
    }
}

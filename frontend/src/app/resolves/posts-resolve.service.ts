import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable({
  providedIn: 'root'
})
export class PostsResolveService implements Resolve<any> {

  constructor(private ajaxApi: AjaxApiService) { }
  resolve(route: ActivatedRouteSnapshot) {
    const where = route.data['post_where'];
    return this.ajaxApi.apiPost('posts/get_many', {where: where}).then((res) => {
      res.forEach((item) => {
        item.featured = parseInt(item.featured);
      });
      return res;
    });
  }
}

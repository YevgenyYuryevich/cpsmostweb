import { Injectable } from '@angular/core';
import {AjaxApiService} from '../services/ajax-api.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class EventbriteOptionsResolveService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        const options = ['eventbrite_personal_token', 'eventbrite_organizations', 'eventbrite_organization', 'eventbrite_events'];
        return this.ajaxApi.apiPost('options/get-options', {options: options});
    }
}

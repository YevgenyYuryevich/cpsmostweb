import { TestBed } from '@angular/core/testing';

import { SocialFeedResolveService } from './social-feed-resolve.service';

describe('SocialFeedResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocialFeedResolveService = TestBed.get(SocialFeedResolveService);
    expect(service).toBeTruthy();
  });
});

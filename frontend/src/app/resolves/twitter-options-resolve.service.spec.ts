import { TestBed } from '@angular/core/testing';

import { TwitterOptionsResolveService } from './twitter-options-resolve.service';

describe('TwitterOptionsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TwitterOptionsResolveService = TestBed.get(TwitterOptionsResolveService);
    expect(service).toBeTruthy();
  });
});

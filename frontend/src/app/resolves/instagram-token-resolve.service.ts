import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AjaxApiService} from '../services/ajax-api.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class InstagramTokenResolveService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        if (route.queryParams.code) {
            return this.ajaxApi.apiPost('instagram/token/' + route.queryParams.code, {redirect_uri: location.origin + '/admin/settings/instagram'});
        } else {
            return Promise.resolve(false);
        }
    }
}

import { Injectable } from '@angular/core';
import {AjaxApiService} from '../services/ajax-api.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AnalyticsCodeResolveService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return this.ajaxApi.apiPost('options/get-options', {options: ['analytics_code']});
    }
}

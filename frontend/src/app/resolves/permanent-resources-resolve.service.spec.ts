import { TestBed } from '@angular/core/testing';

import { PermanentResourcesResolveService } from './permanent-resources-resolve.service';

describe('PermanentResourcesResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PermanentResourcesResolveService = TestBed.get(PermanentResourcesResolveService);
    expect(service).toBeTruthy();
  });
});

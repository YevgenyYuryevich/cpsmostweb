import { TestBed } from '@angular/core/testing';

import { GetProviderResolveService } from './get-provider-resolve.service';

describe('GetProviderResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetProviderResolveService = TestBed.get(GetProviderResolveService);
    expect(service).toBeTruthy();
  });
});

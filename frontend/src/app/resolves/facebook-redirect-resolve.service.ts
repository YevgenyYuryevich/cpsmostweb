import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable({
    providedIn: 'root'
})
export class FacebookRedirectResolveService implements Resolve <any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        if (route.queryParams.code) {
            return Promise.resolve(false);
        } else {
            const rUrl = location.origin + '/admin/settings/facebook';
            return this.ajaxApi.apiPost('facebook/login_url', {redirect_url: rUrl});
        }
    }
}

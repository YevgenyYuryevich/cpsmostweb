import { TestBed } from '@angular/core/testing';

import { QualityAssuranceResolveService } from './quality-assurance-resolve.service';

describe('QualityAssuranceResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QualityAssuranceResolveService = TestBed.get(QualityAssuranceResolveService);
    expect(service).toBeTruthy();
  });
});

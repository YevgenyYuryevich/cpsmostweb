import { TestBed } from '@angular/core/testing';

import { InsuranceOptionsResolveService } from './insurance-options-resolve.service';

describe('InsuranceOptionsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InsuranceOptionsResolveService = TestBed.get(InsuranceOptionsResolveService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { IncidentsResolveService } from './incidents-resolve.service';

describe('IncidentsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IncidentsResolveService = TestBed.get(IncidentsResolveService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable({
    providedIn: 'root'
})
export class FacebookTokenResolveService implements Resolve <any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        if (route.queryParams.code) {
            const rUrl = location.origin + '/admin/settings/facebook';
            return this.ajaxApi.apiGet('facebook/token?code=' + route.queryParams.code + '&state=' + route.queryParams.state + '&FBRLH_state=' + localStorage.getItem('FBRLH_sate') + '&redirect_url=' + rUrl);
        } else {
            return this.ajaxApi.apiPost('options/get-options', {options: ['facebook_feeds']}).then(res => JSON.parse(res.facebook_feeds));
        }
    }
}

import { TestBed } from '@angular/core/testing';

import { RenewalOptionsResolveService } from './renewal-options-resolve.service';

describe('RenewalOptionsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RenewalOptionsResolveService = TestBed.get(RenewalOptionsResolveService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { RenewalsResolveService } from './renewals-resolve.service';

describe('RenewalsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RenewalsResolveService = TestBed.get(RenewalsResolveService);
    expect(service).toBeTruthy();
  });
});

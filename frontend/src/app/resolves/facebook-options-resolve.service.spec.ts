import { TestBed } from '@angular/core/testing';

import { FacebookOptionsResolveService } from './facebook-options-resolve.service';

describe('FacebookOptionsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacebookOptionsResolveService = TestBed.get(FacebookOptionsResolveService);
    expect(service).toBeTruthy();
  });
});

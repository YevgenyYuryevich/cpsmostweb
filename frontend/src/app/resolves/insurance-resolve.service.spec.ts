import { TestBed } from '@angular/core/testing';

import { InsuranceResolveService } from './insurance-resolve.service';

describe('InsuranceResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InsuranceResolveService = TestBed.get(InsuranceResolveService);
    expect(service).toBeTruthy();
  });
});

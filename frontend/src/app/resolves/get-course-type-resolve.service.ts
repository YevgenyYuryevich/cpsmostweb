import { Injectable } from '@angular/core';
import {AjaxApiService} from '../services/ajax-api.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetCourseTypeResolveService implements Resolve<any> {

  constructor(private ajaxApi: AjaxApiService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
      const id = route.paramMap.get('type_id');
      return this.ajaxApi.apiPost('course-types/get', {where: {id: id}});
  }
}

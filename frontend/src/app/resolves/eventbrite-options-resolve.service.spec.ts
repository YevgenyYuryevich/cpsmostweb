import { TestBed } from '@angular/core/testing';

import { EventbriteOptionsResolveService } from './eventbrite-options-resolve.service';

describe('EventbriteOptionsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventbriteOptionsResolveService = TestBed.get(EventbriteOptionsResolveService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable({
  providedIn: 'root'
})
export class InstagramOptionsResolveService implements Resolve <any> {

  constructor(private ajaxApi: AjaxApiService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
      const options = ['instagram_client_id', 'instagram_client_secrete'];
      return this.ajaxApi.apiPost('options/get-options', {options: options});
  }
}

import { TestBed } from '@angular/core/testing';

import { FacebookRedirectResolveService } from './facebook-redirect-resolve.service';

describe('FacebookRedirectResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacebookRedirectResolveService = TestBed.get(FacebookRedirectResolveService);
    expect(service).toBeTruthy();
  });
});

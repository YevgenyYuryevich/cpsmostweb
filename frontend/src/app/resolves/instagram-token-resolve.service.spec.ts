import { TestBed } from '@angular/core/testing';

import { InstagramTokenResolveService } from './instagram-token-resolve.service';

describe('InstagramTokenResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstagramTokenResolveService = TestBed.get(InstagramTokenResolveService);
    expect(service).toBeTruthy();
  });
});

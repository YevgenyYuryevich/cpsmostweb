import { Injectable } from '@angular/core';
import {Resolve} from '@angular/router';
import {AjaxApiService} from '../services/ajax-api.service';

@Injectable({
  providedIn: 'root'
})
export class CourseProviderResolveService implements Resolve<any> {

  constructor(private ajaxApi: AjaxApiService) { }
  resolve() {
      return this.ajaxApi.apiPost('course-providers/get-many');
  }
}

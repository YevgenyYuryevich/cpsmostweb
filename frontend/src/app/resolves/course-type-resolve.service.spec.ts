import { TestBed } from '@angular/core/testing';

import { CourseTypeResolveService } from './course-type-resolve.service';

describe('CourseTypeResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseTypeResolveService = TestBed.get(CourseTypeResolveService);
    expect(service).toBeTruthy();
  });
});

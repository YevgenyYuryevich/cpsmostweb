import { TestBed } from '@angular/core/testing';

import { CoursesResolveService } from './courses-resolve.service';

describe('CoursesResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoursesResolveService = TestBed.get(CoursesResolveService);
    expect(service).toBeTruthy();
  });
});

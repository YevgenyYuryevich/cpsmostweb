import { TestBed } from '@angular/core/testing';

import { SocialResolveService } from './social-resolve.service';

describe('SocialResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocialResolveService = TestBed.get(SocialResolveService);
    expect(service).toBeTruthy();
  });
});

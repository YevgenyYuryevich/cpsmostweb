import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AjaxApiService} from '../services/ajax-api.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class GetProviderResolveService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        if (route.queryParams.provider) {
            return this.ajaxApi.apiPost('course-providers/get', {where: {id: route.queryParams.provider}});
        } else {
            return Promise.resolve(false);
        }
    }
}

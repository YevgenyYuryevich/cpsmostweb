import { TestBed } from '@angular/core/testing';

import { InstagramOptionsResolveService } from './instagram-options-resolve.service';

describe('InstagramOptionsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstagramOptionsResolveService = TestBed.get(InstagramOptionsResolveService);
    expect(service).toBeTruthy();
  });
});

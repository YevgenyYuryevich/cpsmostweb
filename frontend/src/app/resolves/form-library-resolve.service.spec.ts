import { TestBed } from '@angular/core/testing';

import { FormLibraryResolveService } from './form-library-resolve.service';

describe('FormLibraryResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormLibraryResolveService = TestBed.get(FormLibraryResolveService);
    expect(service).toBeTruthy();
  });
});

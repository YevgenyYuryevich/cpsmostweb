import { TestBed } from '@angular/core/testing';

import { GetCourseTypeResolveService } from './get-course-type-resolve.service';

describe('GetCourseTypeResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetCourseTypeResolveService = TestBed.get(GetCourseTypeResolveService);
    expect(service).toBeTruthy();
  });
});

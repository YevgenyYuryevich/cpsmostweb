import { TestBed } from '@angular/core/testing';

import { CourseTracksResolveService } from './course-tracks-resolve.service';

describe('CourseTracksResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseTracksResolveService = TestBed.get(CourseTracksResolveService);
    expect(service).toBeTruthy();
  });
});

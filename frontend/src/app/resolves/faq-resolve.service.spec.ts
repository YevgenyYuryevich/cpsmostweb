import { TestBed } from '@angular/core/testing';

import { FaqResolveService } from './faq-resolve.service';

describe('FaqResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FaqResolveService = TestBed.get(FaqResolveService);
    expect(service).toBeTruthy();
  });
});

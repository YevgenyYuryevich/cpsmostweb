import { TestBed } from '@angular/core/testing';

import { NewsletterEmailsResolveService } from './newsletter-emails-resolve.service';

describe('NewsletterEmailsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewsletterEmailsResolveService = TestBed.get(NewsletterEmailsResolveService);
    expect(service).toBeTruthy();
  });
});

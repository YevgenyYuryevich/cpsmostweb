import { TestBed } from '@angular/core/testing';

import { FacebookTokenResolveService } from './facebook-token-resolve.service';

describe('FacebookTokenResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacebookTokenResolveService = TestBed.get(FacebookTokenResolveService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { AnalyticsCodeResolveService } from './analytics-code-resolve.service';

describe('AnalyticsCodeResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnalyticsCodeResolveService = TestBed.get(AnalyticsCodeResolveService);
    expect(service).toBeTruthy();
  });
});

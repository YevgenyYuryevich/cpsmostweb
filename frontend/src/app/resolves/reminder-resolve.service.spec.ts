import { TestBed } from '@angular/core/testing';

import { ReminderResolveService } from './reminder-resolve.service';

describe('ReminderResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReminderResolveService = TestBed.get(ReminderResolveService);
    expect(service).toBeTruthy();
  });
});

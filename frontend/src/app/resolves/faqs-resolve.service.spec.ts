import { TestBed } from '@angular/core/testing';

import { FaqsResolveService } from './faqs-resolve.service';

describe('FaqsResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FaqsResolveService = TestBed.get(FaqsResolveService);
    expect(service).toBeTruthy();
  });
});

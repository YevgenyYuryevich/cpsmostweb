import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    {
        path: '/admin/dashboard',
        title: 'Dashboard',
        icon: 'mdi mdi-view-dashboard',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '/admin/profile',
        title: 'Profile',
        icon: 'mdi mdi-account',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '',
        title: 'Users',
        icon: 'mdi mdi-account-multiple',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/users',
                title: 'Users',
                icon: 'mdi mdi-view-list',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/users/add',
                title: 'Add User',
                icon: 'mdi mdi-account-plus',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/user-levels',
                title: 'User Levels',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ]
    },
    {
        path: '',
        title: 'Posts',
        icon: 'mdi mdi-blogger',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/manage-content',
                title: 'Manage Content',
                icon: 'mdi mdi-view-list',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/add-content',
                title: 'Add Content',
                icon: 'mdi mdi-plus',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ]
    },
    {
        path: '',
        title: 'Courses',
        icon: 'mdi mdi-motorbike',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/courses',
                title: 'Manage Courses',
                icon: 'mdi mdi-view-list',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/courses/add',
                title: 'Add Course',
                icon: 'mdi mdi-plus',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ]
    },
    {
        path: '',
        title: 'Settings',
        icon: 'mdi mdi-settings',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/settings/social',
                title: 'Social Link',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/settings/eventbrite',
                title: 'Eventbrite',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/settings/instagram',
                title: 'Instagram',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/settings/twitter',
                title: 'Twitter',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/settings/facebook',
                title: 'Facebook',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/settings/insurance',
                title: 'Insurance',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/settings/renewal',
                title: 'Renewal Application',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/settings/analytics',
                title: 'Analytics Code',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '/admin/course-types/manage',
        title: 'Manage Course Type',
        icon: 'mdi mdi-subway',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '/admin/course-providers/manage',
        title: 'Manage Course Provider',
        icon: 'mdi mdi-gauge',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '',
        title: 'Reminders',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/reminders/create',
                title: 'Set Reminder',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/reminders/current',
                title: 'Current Reminders',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/reminders/sent',
                title: 'Reminders Sent',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '',
        title: 'FAQ',
        icon: 'mdi mdi-clipboard-text',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/faq/add',
                title: 'Add FAQ',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/faq/manage',
                title: 'View/Edit FAQs',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '',
        title: 'Incidents',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/incidents/create',
                title: 'Submit Incident',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/incidents/manage',
                title: 'View/Edit Incidents',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '',
        title: 'Rider Coach Certification',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/rider-coach-certification/create',
                title: 'Submit Certification',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/rider-coach-certification/manage',
                title: 'Edit/View Certifications',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '',
        title: 'Renewal Application',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/re-app/create',
                title: 'Submit Renewal Application',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/re-app/manage',
                title: 'View Renewal Applications',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '',
        title: 'Insurance Verification',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/insurance/create',
                title: 'Create Insurance Verification',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/insurance/manage',
                title: 'Edit/View Insurance Verification',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '/admin/newsletter-subscribing',
        title: 'Newsletter Subscribing',
        icon: 'mdi mdi-email',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '/admin/quality-assurance',
        title: 'View Quality Assurance Files',
        icon: 'mdi mdi-email',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '/admin/form-library',
        title: 'Form Library',
        icon: 'mdi mdi-email',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
];

export const INSTRUCTOR_ROUTES: RouteInfo[] = [
    {
        path: '/admin/profile',
        title: 'Profile',
        icon: 'mdi mdi-gauge',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '',
        title: 'Rider Coach Certification',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/rider-coach-certification/create',
                title: 'Submit Certification',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/rider-coach-certification/manage',
                title: 'Edit/View Certifications',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '',
        title: 'Instructor Renewal Application',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/re-app/create',
                title: 'Submit Renewal Application',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/re-app/manage',
                title: 'View Renewal Applications',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '/admin/quality-assurance',
        title: 'View Quality Assurance Files',
        icon: 'mdi mdi-email',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '/admin/form-library',
        title: 'Form Library',
        icon: 'mdi mdi-email',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
];
export const VENDOR_ROUTES: RouteInfo[] = [
    {
        path: '/admin/profile',
        title: 'Profile',
        icon: 'mdi mdi-gauge',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '',
        title: 'Incidents',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/incidents/create',
                title: 'Submit Incident',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/incidents/manage',
                title: 'View/Edit Incidents',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '',
        title: 'Vendor Renewal Application',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/re-app/create',
                title: 'Submit Renewal Application',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/re-app/manage',
                title: 'View Renewal Applications',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '',
        title: 'Insurance Verification',
        icon: 'mdi mdi-gauge',
        class: 'has-arrow',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: [
            {
                path: '/admin/insurance/create',
                title: 'Create Insurance Verification',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
            {
                path: '/admin/insurance/manage',
                title: 'Edit/View Insurance Verification',
                icon: 'mdi mdi-gauge',
                class: '',
                label: '',
                labelClass: '',
                extralink: false,
                submenu: []
            },
        ],
    },
    {
        path: '/admin/quality-assurance',
        title: 'View Quality Assurance Files',
        icon: 'mdi mdi-email',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
    {
        path: '/admin/form-library',
        title: 'Form Library',
        icon: 'mdi mdi-email',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
    },
];

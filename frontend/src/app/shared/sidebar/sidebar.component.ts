import {Component, AfterViewInit, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {INSTRUCTOR_ROUTES, ROUTES, VENDOR_ROUTES} from './menu-items';
import {RouteInfo} from './sidebar.metadata';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthService} from '../../services/auth.service';

declare var $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    showMenu = '';
    showSubMenu = '';
    public sidebarnavItems: any[];

    // this is for the open close
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        private authService: AuthService,
    ) {
    }

    // End open close
    ngOnInit() {
        if (this.authService.auth.role.toLowerCase() === 'admin') {
            this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);
        } else if (this.authService.auth.role.toLowerCase() === 'vendor'){
            this.sidebarnavItems = VENDOR_ROUTES.filter(sidebarnavItem => sidebarnavItem);
        } else {
            this.sidebarnavItems = INSTRUCTOR_ROUTES.filter(sidebarnavItem => sidebarnavItem);
        }
        if (this.authService.auth.role.toLowerCase() === 'instructor') {
            this.sidebarnavItems = this.sidebarnavItems.filter(item => {
                return true;
            });
        }
        if (this.authService.auth.role.toLowerCase() === 'vendor') {
            this.sidebarnavItems = this.sidebarnavItems.filter(item => {
                return true;
            });
        }
        if (!parseInt(this.authService.auth.quality_assurance)) {
            this.sidebarnavItems = this.sidebarnavItems.filter(item => {
                if (item.path === '/admin/quality-assurance') {
                    return false;
                }
                return true;
            });
        }
        if (parseInt(this.authService.auth.role_data.can_form_library) === 0) {
            this.sidebarnavItems = this.sidebarnavItems.filter(item => {
                if (item.path === '/admin/form-library') {
                    return false;
                }
                return true;
            });
        }
    }
}

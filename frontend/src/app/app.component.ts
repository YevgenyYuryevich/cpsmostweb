import { Component } from '@angular/core';
import {AjaxApiService} from "./services/ajax-api.service";
import {NavigationEnd, Router} from '@angular/router';
import {AppCommonService} from './services/app-common.service';

declare var jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(public ajaxApi: AjaxApiService, private router: Router, private appCommonService: AppCommonService) {
        this.router.events.subscribe(
            event => {
                if ( event instanceof NavigationEnd ) {
                    this.loadAnalyticsCode();
                    this.scrollTop();
                }
            }
        );
    }
    loadAnalyticsCode() {
        if (this.appCommonService.analyticsCode) {
            jQuery('#external-script-wrap').empty();
            jQuery('#external-script-wrap').append(this.appCommonService.analyticsCode);
        } else {
            this.ajaxApi.apiPost('options/get-options', {options: ['analytics_code']}, false).then(res => {
                if (res.analytics_code) {
                    this.appCommonService.analyticsCode = res.analytics_code.replace(/safe-script/g, 'script');
                    jQuery('#external-script-wrap').empty();
                    jQuery('#external-script-wrap').append(this.appCommonService.analyticsCode);
                } else {
                    return false;
                }
            });
        }
    }
    scrollTop() {
        window.scrollTo(0, 0);
    }
}

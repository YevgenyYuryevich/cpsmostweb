import {AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
declare var jQuery: any;
declare var videojs: any;

@Component({
    selector: 'app-list-video',
    templateUrl: './list-video.component.html',
    styleUrls: ['./list-video.component.scss']
})
export class ListVideoComponent implements OnInit, AfterViewInit {
    @ViewChild('listWrap') listWrap: ElementRef;
    @ViewChildren('video') videos: QueryList<ElementRef>;
    posts: any[];
    courseTypes: any[];
    socialFeed: any;
    constructor(private route: ActivatedRoute) {
        this.posts = this.route.snapshot.data['posts'].filter((post) => {
            return post.type !== 'audio';
        }).sort(( a, b ) => {
            if (a.id < b.id) {
                return 1;
            } else if (a.id === b.id) {
                return  0;
            } else {
                return  -1;
            }
        });
        this.courseTypes = this.route.snapshot.data['course_types'];
        this.socialFeed = this.route.snapshot.data['social_feed'];
    }
    ngOnInit() {
    }
    ngAfterViewInit(): void {
        this.videos.forEach(video => {
            const src = video.nativeElement.attributes['data-src'].value;
            this.setupVideo(video.nativeElement, src);
        });
    }
    setupVideo(dom, src) {
        if (src.match(/^(https:\/\/www.youtube.com)/)) {
            const setup = {
                techOrder: ['youtube'],
                sources: [
                    { type: 'video/youtube',
                        src: src
                    }]
            };
            jQuery(dom).attr('data-setup', JSON.stringify(setup));
        } else {
            jQuery(dom).append('<source src="' + src + '"/>');
        }
        const pWidth = parseInt(jQuery(this.listWrap.nativeElement).css('width'));
        const padding = parseInt(jQuery(this.listWrap.nativeElement).css('padding-left'));
        const height = (pWidth - padding * 2) * 540 / 960;
        videojs(dom, {
            width: (pWidth - padding * 2),
            height: height
        });
    }
}

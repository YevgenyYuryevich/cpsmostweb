import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PostViewComponent} from '../../../app-components/post-view/post-view.component';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {AjaxApiService} from '../../../services/ajax-api.service';
import {GeocodeService} from '../../../services/geocode.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  @ViewChild(PostViewComponent) postViewComponent: PostViewComponent;
  @ViewChild('socialInstagramGrid') socialInstagramGrid: ElementRef;
  @ViewChild('socialFacebook') socialFacebook: ElementRef;
  private twitter: any;
  heroSlideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: false,
    infinite: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
  };
  posts: any[] = [];
  videoPosts: any[] = [];
  featuredPosts: any[] = [];
  courseTypes: any[] = [];
  courseProviders: any[] = [];
  courseProviderCnt: number;
  newsletter_email: any;
  socialFeed: any;
  eventbriteEvents: any[];
  instagramPhotos: any[];
  twitterTweets: any;
  facebookFeeds: any[];
  circleD: number;
  socialInstagramHeight = '300px';
  facebookReady = false;
  facebookEmbedUrl;
  socialFacebookWidth = 300;
  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private ajaxApi: AjaxApiService,
    private geocodeService: GeocodeService,
    private router: Router,
    private sanitizer: DomSanitizer
  ) {
    this.posts = this.route.snapshot.data['posts'];
    this.posts.forEach((post) => {
      if (post.type === 'video') {
        this.videoPosts.push(post);
      }
      if (post.featured == 1) {
        this.featuredPosts.push(post);
      }
    });
    this.videoPosts.sort((a, b) => {
      return parseInt(a.sort) - parseInt(b.sort);
    });
    this.courseTypes = this.route.snapshot.data['course_types'];
    this.courseProviders = this.route.snapshot.data['course_providers'].map(p => {
      p.featured = parseInt(p.featured);
      return p;
    });
    this.courseProviders = this.courseProviders.filter(p => p.featured);
    this.courseProviders.sort((a, b) => {
      if (a.image > b.image) {
        return -1;
      } else if (a.image < b.image) {
        return 1;
      }
      return 0;
    });
    this.courseProviderCnt = this.courseProviders.length;
    this.circleD = this.courseProviderCnt * 120 / 3.14;
    this.socialFeed = this.route.snapshot.data['social_feed'];
    if (this.socialFeed.eventbrite_events) {
      this.eventbriteEvents = JSON.parse(this.socialFeed.eventbrite_events).map((item: any) => {
        item.startTime = new Date(item.start_time);
        item.endTime = new Date(item.end.utc);
        item.eventDur = Math.round((item.endTime - item.startTime) / (1000 * 60 * 60 * 24)) + 1;
        return item;
      });
      this.eventbriteEvents = this.eventbriteEvents.filter(e => {
        const now = new Date();
        now.setDate(now.getDate() - 1);
        return e.startTime.getTime() > now.getTime();
      });
    } else {
      this.eventbriteEvents = [];
    }
    this.instagramPhotos = this.socialFeed.instagram_photos ? JSON.parse(this.socialFeed.instagram_photos) : [];
    this.twitterTweets = this.socialFeed.twitter_tweets ? JSON.parse(this.socialFeed.twitter_tweets) : {};
    this.facebookFeeds = this.socialFeed.facebook_feeds ? JSON.parse(this.socialFeed.facebook_feeds) : [];
    this.facebookFeeds = this.facebookFeeds.map(feed => {
      feed.attachment = feed.attachments.data[0];
      return feed;
    });
  }
  ngOnInit() {
    this.socialInstagramHeight = this.socialInstagramGrid.nativeElement.offsetWidth + 'px';
    this.socialFacebookWidth = this.socialFacebook.nativeElement.offsetWidth;
    this.facebookEmbedUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FMotorcylesafety&tabs=timeline&width=' + this.socialFacebookWidth + '&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=785428811857929');
    this.facebookReady = true;
  }
  ngAfterViewInit(): void {
    const scrollTo = this.route.snapshot.queryParams['scroll_to'];
    if (scrollTo === 'upcoming-events') {
      document.getElementById('upcoming-events').scrollIntoView({behavior: 'smooth'});
    }
  }
  viewPost(post) {
    this.postViewComponent.setPost(post);
    this.postViewComponent.open();
  }
  sendNewsletterEmail() {
    this.ajaxApi.apiPost('newsletter/subscribe', {email: this.newsletter_email}).then((res) => {
      this.toastr.success('Thanks for subscribing for our Newsletter!', 'Great');
    });
  }
  findCourse(id = false) {
    this.geocodeService.currentPosition.then((pos) => {
      if (pos) {
        if (id) {
          this.router.navigateByUrl('/find-course/' + id + '/map');
        } else {
          this.router.navigateByUrl('/select-course-type');
        }
      } else {
        if (id) {
          this.router.navigateByUrl('/select-location/' + id);
        } else {
          this.router.navigateByUrl('/select-location');
        }
      }
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectCourseTypeComponent } from './select-course-type.component';

describe('SelectCourseTypeComponent', () => {
  let component: SelectCourseTypeComponent;
  let fixture: ComponentFixture<SelectCourseTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectCourseTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCourseTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

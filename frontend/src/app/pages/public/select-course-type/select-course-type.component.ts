import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-select-course-type',
    templateUrl: './select-course-type.component.html',
    styleUrls: ['./select-course-type.component.scss']
})
export class SelectCourseTypeComponent implements OnInit {

    location: any = false;
    courseTypes: any[];
    constructor(private route: ActivatedRoute, private router: Router) {
        this.courseTypes = this.route.snapshot.data['course_types'];
        this.location = this.route.snapshot.paramMap.get('location') || false;
    }

    ngOnInit() {
    }
    findCourse(id) {
        if (this.location) {
            this.router.navigateByUrl('/find-course/' + id + '/map/' + this.location);
        } else {
            this.router.navigateByUrl('/find-course/' + id + '/map');
        }
    }
}

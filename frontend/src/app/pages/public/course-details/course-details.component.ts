import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GeocodeService} from '../../../services/geocode.service';
import {AppCommonService} from '../../../services/app-common.service';

@Component({
    selector: 'app-course-details',
    templateUrl: './course-details.component.html',
    styleUrls: ['./course-details.component.scss']
})
export class CourseDetailsComponent implements OnInit {

    courseType: any;
    courseTypes: any[];
    socialFeed: any;
    clicked = false;
    constructor(private route: ActivatedRoute, private geocodeService: GeocodeService, private router: Router, private appCommonService: AppCommonService) {
        this.courseTypes = this.route.snapshot.data['course_types'];
        this.courseType = this.courseTypes.find((item) => {
            return parseInt(item.id) === parseInt(this.route.snapshot.paramMap.get('id'));
        });
        this.socialFeed = this.route.snapshot.data['social_feed'];
        appCommonService.plusCourseViews();
    }

    ngOnInit() {
    }
    findCourse() {
        const id = this.route.snapshot.paramMap.get('id');
        this.geocodeService.currentPosition.then((pos) => {
            if (pos) {
                if (id) {
                    this.router.navigateByUrl('find-course/' + id + '/map');
                } else {
                    this.router.navigateByUrl('select-course-type');
                }
            } else {
                if (id) {
                    this.router.navigateByUrl('select-location/' + id);
                } else {
                    this.router.navigateByUrl('select-location');
                }
            }
        });
        if (!this.clicked) {
            this.appCommonService.plusCourseClicks();
            this.clicked = true;
        }
    }
}

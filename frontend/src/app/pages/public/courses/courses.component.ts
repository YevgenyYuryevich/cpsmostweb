import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AppCommonService} from '../../../services/app-common.service';

@Component({
    selector: 'app-courses',
    templateUrl: './courses.component.html',
    styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {

    courses: any[];
    courseTypes: any[];
    socialFeed: any;
    keyword = '';
    clicked = false;
    constructor(private route: ActivatedRoute, private appCommonService: AppCommonService) {
        this.courseTypes = this.route.snapshot.data['course_types'];
        this.socialFeed = this.route.snapshot.data['social_feed'];
        appCommonService.plusCourseViews();
    }

    ngOnInit() {
    }
}

import { Component, OnInit } from '@angular/core';
import {AppCommonService} from '../../../services/app-common.service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

    lat = 39.7541032;
    lng = -105.0002242;

    social: any = {};
    constructor(private appCommonService: AppCommonService) {
        this.social = this.appCommonService.socialLinks;
    }
    ngOnInit() {
    }
    dropUs() {
    }
}

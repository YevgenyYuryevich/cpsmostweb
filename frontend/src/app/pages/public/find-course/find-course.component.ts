import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HelperService} from '../../../services/helper.service';
import {GeocodeService} from '../../../services/geocode.service';
import {GeometryService} from '../../../services/geometry.service';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {TableSearchPipe} from '../../../pipes/table-search.pipe';
import {AppScrollToService} from '../../../services/app-scroll-to.service';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import {AjaxApiService} from '../../../services/ajax-api.service';

declare var jQuery;

@Component({
    selector: 'app-find-course',
    templateUrl: './find-course.component.html',
    styleUrls: ['./find-course.component.scss'],
    providers: [TableSearchPipe]
})
export class FindCourseComponent implements OnInit {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('dateCellTemplate') dateCellTemplate: TemplateRef<any>;
    @ViewChild('locationCellTemplate') locationCellTemplate: TemplateRef<any>;
    @ViewChild('listTable') listTable: DatatableComponent;
    @ViewChild('mapTable') mapTable: DatatableComponent;
    courseType: any;
    courses: any[];
    mapCourses: any[] = [];
    listCourses: any[] = [];
    providers: any[];
    mapProviders: any[];
    radius = 50;
    lat = 39.7541032;
    lng = -105.0002242;
    centerLocation = '80202';
    keyword: string;
    viewLen = 15;
    columns: any[];
    table_page_size: any = 10;
    searchSidebarOpened = false;
    tab = 'list';
    mobileSortByDir = true;
    mobileSortItems = [
        {name: 'Date Time', prop: 'start_at'},
        {name: 'Provider', prop: 'provider_name'},
        {name: 'Location', prop: 'locationDisplay'},
        {name: 'Cost', prop: 'cost'},
        {name: 'Distance', prop: 'distance'},
    ];
    mobileSortBy: any = this.mobileSortItems[0];
    days: any[] = [
        {name: 'M', selected: false},
        {name: 'T', selected: false},
        {name: 'W', selected: false},
        {name: 'Th', selected: false},
        {name: 'F', selected: false},
        {name: 'Sat', selected: false},
        {name: 'Sun', selected: false},
    ];
    dayTouched = false;
    selectedLocation: any = false;

    constructor(
        private route: ActivatedRoute,
        public appHelper: HelperService,
        private geocodeService: GeocodeService,
        private geometryService: GeometryService,
        private tableSearch: TableSearchPipe,
        config: NgbDropdownConfig,
        private ajaxApi: AjaxApiService
    ) {
        this.tab = this.route.snapshot.paramMap.get('tab') || 'map';
        if (this.route.snapshot.paramMap.has('center_location')) {
            this.centerLocation = this.route.snapshot.paramMap.get('center_location');
            if (this.centerLocation === '81230') {
                this.radius = 150;
            }
            this.geocodeService.geocodeAddress(this.centerLocation).toPromise().then((res) => {
                if (res.lat || res.lng) {
                    this.lat = res.lat;
                    this.lng = res.lng;
                }
                this.geocodeService.waitForMapsToLoad().toPromise().then(() => {
                    this.applyFilter();
                });
            });
        } else {
            this.geocodeService.currentPosition.then((pos) => {
                this.centerLocation = 'current';
                this.lat = pos.latitude;
                this.lng = pos.longitude;
                this.geocodeService.geocodeByLatLng(this.lat, this.lng).toPromise().then((res) => {
                    this.centerLocation = res;
                });
                this.geocodeService.waitForMapsToLoad().toPromise().then(() => {
                    this.applyFilter();
                });
            });
        }
        this.courseType = this.route.snapshot.data['course_type'];
        this.providers = this.route.snapshot.data['course_providers'];
        this.mapProviders = this.providers.filter(p => p.geolocation_lat && p.geolocation_lng);
        this.mapProviders = this.mapProviders.filter(p => p.name !== 'Full Throttle Motorcycle Training LLC');
        this.courses = this.route.snapshot.data['courses'].map((item: any) => {
            return this.formatCourse(item);
        }).filter((item) => {
            const now = new Date();
            const d = new Date(item.start_at);
            return now.getTime() <= d.getTime() && item.register_data;
        }).sort((a, b) => {
            const av = new Date(a.start_at).getTime();
            const bv = new Date(b.start_at).getTime();
            return av - bv;
        });
        config.autoClose = false;
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Type',
                prop: 'typeDisplay',
                cellTemplate: this.txtCellTemplate,
                width: 200,
                resizeable: false,
            },
            {
                name: 'DATE AND TIME',
                prop: 'start_at',
                cellTemplate: this.dateCellTemplate,
                width: 250,
                resizeable: false,
            },
            {
                name: 'Provider',
                prop: 'provider_name',
                cellTemplate: this.txtCellTemplate,
                width: 250,
                resizeable: false,
            },
            {
                name: 'Location',
                prop: 'locationDisplay',
                cellTemplate: this.locationCellTemplate,
                width: 250,
                resizeable: false,
            },
            {
                name: 'Cost',
                prop: 'costDisplay',
                cellTemplate: this.txtCellTemplate,
                width: 100,
                resizeable: false,
            },
            {
                name: 'Open Seats',
                prop: 'open_seats',
                cellTemplate: this.txtCellTemplate,
                width: 100,
                resizeable: false,
            },
            {
                name: 'Distance(mile)',
                prop: 'distance',
                cellTemplate: this.txtCellTemplate,
                width: 100,
                resizeable: false,
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 100,
                sortable: false,
                resizeable: false,
            },
        ];
    }

    formatCourse(course: any) {
        const provider = this.providers.find((v) => {
            return v.id == course.provider_company;
        });
        course.providerAddress = provider.address;
        course.provider = provider;
        course.origin = JSON.parse(course.origin_data);
        course.provider_name = course.provider.name;
        course.locationDisplay = course.provider.address + '/' + course.origin.classLocation;
        course.costDisplay = '$' + course.cost;
        course.typeDisplay = this.courseType.name;
        return course;
    }

    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.listTable.offset = 0;
        this.mapTable.offset = 0;
    }

    refreshCenter() {
        this.selectedLocation = false;
        if (this.centerLocation) {
            this.geocodeService.geocodeAddress(this.centerLocation).toPromise().then((res) => {
                if (res.lat || res.lng) {
                    this.lat = res.lat;
                    this.lng = res.lng;
                    this.applyFilter();
                }
            });
        } else {
            this.applyFilter();
        }
    }

    applyFilter() {
        this.viewLen = 15;
        const distanceTrack = {};
        this.courses.forEach((item) => {
            item.locationHash = 'lhash_' + item.geolocation_lat + ':' + item.geolocation_lng;
            if (distanceTrack[item.locationHash]) {
                item.distance = distanceTrack[item.locationHash];
            } else {
                const from = {lat: this.lat, lng: this.lng};
                const to = {lat: item.geolocation_lat, lng: item.geolocation_lng};
                const dis = this.geometryService.calculateDistance(from, to);
                item.distance = this.appHelper.meterToMile(dis);
                distanceTrack[item.locationHash] = item.distance;
            }
            item.distance = Number((item.distance).toFixed(2));
            item.distanceAvailable = item.distance <= this.radius;
            item.isFiltered = this.tableSearch.isFiltered(item, this.keyword);
            item.mapOpacity = item.distanceAvailable && item.isFiltered ? 1.0 : 0.2;
        });
        this.mapCourses = this.courses.filter((item) => {
            const d = new Date(item.start_at);
            const day = d.getUTCDay();
            return this.dayTouched ? this.days[(day - 1 + 7) % 7].selected : true;
        });
        this.listCourses = this.courses.filter((item) => {
            if (item.distance <= this.radius) {
                const d = new Date(item.start_at);
                const day = d.getUTCDay();
                return this.dayTouched ? this.days[(day - 1 + 7) % 7].selected : true;
            }
            return false;
        });
        console.log('apply filter');
    }

    applyLocationFilter() {
        this.viewLen = 15;
        this.courses.forEach((item) => {
            item.isFiltered = true;
            item.mapOpacity = 1.0;
        });
        this.mapCourses = this.courses.filter((item) => {
            const d = new Date(item.start_at);
            const day = d.getUTCDay();
            return (item.address === this.selectedLocation || item.providerAddress === this.selectedLocation) && (this.dayTouched ? this.days[(day - 1 + 7) % 7].selected : true);
        });
        this.listCourses = this.courses.filter((item) => {
            const d = new Date(item.start_at);
            const day = d.getUTCDay();
            return (item.address === this.selectedLocation || item.providerAddress === this.selectedLocation) && (this.dayTouched ? this.days[(day - 1 + 7) % 7].selected : true);
        });
        console.log('location filter', this.selectedLocation);
    }

    applyDayFilter() {
        this.dayTouched = true;
        this.mapCourses = this.courses.filter((item) => {
            const d = new Date(item.start_at);
            const day = d.getUTCDay();
            return this.days[day].selected;
        });
        this.listCourses = this.courses.filter((item) => {
            if (item.distance <= this.radius) {
                const d = new Date(item.start_at);
                const day = d.getUTCDay();
                return this.days[(day - 1 + 7) % 7].selected;
            }
            return false;
        });
    }

    tableRecalculate() {
        setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
        }, 200);
    }

    studentRegister(course) {
        if (parseInt(course.open_seats) === 0) {
            alert('This class is sold out!');
            return;
        }
        if (course.register_data) {
            const registerData = JSON.parse(course.register_data);
            if (registerData.type === 'post') {
                const form = jQuery('<form method="post" action="' + registerData.url + '" target="_self" hidden></form>');
                for (const k in registerData.form_data) {
                    form.append('<input name="' + k + '" value="' + registerData.form_data[k] + '"/>');
                }
                form.appendTo('body');
                form.submit().remove();
            } else if (registerData.type === 'get') {
                window.open(registerData.url, '__black');
            }
        }
    }

    scrollToMapTable() {
        const element = document.querySelector('#map-table-wrapper');
        element.scrollIntoView({behavior: 'smooth', block: 'start'});
    }

    setSortBy(s) {
        if (this.mobileSortBy.prop === s.prop) {
            this.mobileSortByDir = !this.mobileSortByDir;
        } else {
            this.mobileSortBy = s;
            this.mobileSortByDir = true;
        }
    }
}

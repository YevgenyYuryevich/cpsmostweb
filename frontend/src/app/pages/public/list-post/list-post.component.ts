import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-list-post',
    templateUrl: './list-post.component.html',
    styleUrls: ['./list-post.component.scss']
})
export class ListPostComponent implements OnInit {
    posts: any[];
    courseTypes: any[];
    socialFeed: any;
    constructor(private route: ActivatedRoute) {
        this.posts = this.route.snapshot.data['posts'].filter((post) => {
            return post.type !== 'audio';
        }).sort(( a, b ) => {
          if (a.id < b.id) {
            return 1;
          } else if (a.id === b.id) {
            return  0;
          } else {
            return  -1;
          }
        });
        this.courseTypes = this.route.snapshot.data['course_types'];
        this.socialFeed = this.route.snapshot.data['social_feed'];
    }

    ngOnInit() {
    }
}

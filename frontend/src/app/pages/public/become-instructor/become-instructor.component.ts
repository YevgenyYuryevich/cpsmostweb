import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {AppCommonService} from '../../../services/app-common.service';

@Component({
    selector: 'app-become-instructor',
    templateUrl: './become-instructor.component.html',
    styleUrls: ['./become-instructor.component.scss']
})
export class BecomeInstructorComponent implements OnInit {

    sets: any = {
        level: '1'
    };
    confirmPassword = '';
    vendorInstructorLevels: any[];
    registerError = '';
    registerSuccess = '';

    constructor(public authService: AuthService, public commonService: AppCommonService) {
        this.vendorInstructorLevels = this.commonService.userLevels.filter(l => {
            const levelName = l.name.toLowerCase();
            return levelName === 'instructor' || levelName === 'vendor';
        });
        if (this.vendorInstructorLevels.length) {
            this.sets.level = this.vendorInstructorLevels[0].id;
        }
    }

    ngOnInit() {
    }

    register() {
        this.authService.register(this.sets).then(res => {
            this.registerSuccess = 'Thanks! Admin will email you soon.';
            this.registerError = '';
        }, err => {
            this.registerError = err;
            this.registerSuccess = '';
        });
    }
}

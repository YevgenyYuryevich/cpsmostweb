import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

declare var jQuery: any;
declare var videojs: any;

@Component({
    selector: 'app-view-post',
    templateUrl: './view-post.component.html',
    styleUrls: ['./view-post.component.scss']
})
export class ViewPostComponent implements OnInit, AfterViewInit {

    @ViewChild('video') video: ElementRef;
    @ViewChild('videoBlog') videoBlog: ElementRef;
    @ViewChild('postWrap') postWrap: ElementRef;
    post: any;
    courseTypes: any[];
    socialFeed: any;
    jqVideoDom: any;
    videoPlayer: any;

    constructor(private route: ActivatedRoute) {
        this.courseTypes = this.route.snapshot.data['course_types'];
        this.socialFeed = this.route.snapshot.data['social_feed'];
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.post = this.route.snapshot.data['post'];
            setTimeout(() => {
                if (this.post.type === 'video') {
                    if (this.videoPlayer) {
                        this.videoPlayer.dispose();
                        this.videoPlayer = false;
                    }
                    this.jqVideoDom = jQuery(this.video.nativeElement).clone().appendTo(this.videoBlog.nativeElement);
                    this.setupVideo();
                } else {
                    this.videoPlayer = false;
                }
            }, 100);
        });
    }
    ngAfterViewInit(): void {
    }
    setupVideo() {
        if (this.post.content.match(/^(https:\/\/www.youtube.com)/)) {
            const setup = {
                techOrder: ['youtube'],
                sources: [
                    { type: 'video/youtube',
                        src: this.post.content
                    }]
            };
            this.jqVideoDom.attr('data-setup', JSON.stringify(setup));
        } else {
            this.jqVideoDom.append('<source src="' + this.post.content + '"/>');
        }
        const pWidth = parseInt(jQuery(this.postWrap.nativeElement).css('width'));
        const padding = parseInt(jQuery(this.postWrap.nativeElement).css('padding-left'));
        const height = (pWidth - padding * 2) * 540 / 960;
        this.videoPlayer = videojs(this.jqVideoDom.get(0), {
            width: (pWidth - padding * 2),
            height: height
        });
    }
}

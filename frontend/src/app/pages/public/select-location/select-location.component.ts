import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-select-location',
    templateUrl: './select-location.component.html',
    styleUrls: ['./select-location.component.scss']
})
export class SelectLocationComponent implements OnInit {

    typeId: any = false;
    constructor(private route: ActivatedRoute, private router: Router) {
        this.typeId = this.route.snapshot.paramMap.get('course_type_id') || false;
    }

    ngOnInit() {
    }
    findCourse(location) {
        if (this.typeId) {
            this.router.navigateByUrl('/find-course/' + this.typeId + '/map/' + location);
        } else {
            this.router.navigateByUrl('/select-course-type/' + location);
        }
    }
}

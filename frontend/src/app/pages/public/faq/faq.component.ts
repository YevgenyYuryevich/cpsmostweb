import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

    faqs: any[];
    courseTypes: any[];
    socialFeed: any;
    constructor(private route: ActivatedRoute) {
        this.faqs = this.route.snapshot.data['faqs'];
        this.courseTypes = this.route.snapshot.data['course_types'];
        this.socialFeed = this.route.snapshot.data['social_feed'];
        this.faqs.sort((a, b) => {
            return parseInt(a.sort) - parseInt(b.sort);
        });
    }

    ngOnInit() {
    }

}

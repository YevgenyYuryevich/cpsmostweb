import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService} from "../services/alert.service";
import {AuthService} from "../../../services/auth.service";
import {AjaxApiService} from "../../../services/ajax-api.service";

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

    model: any = {};
    loading = false;
    returnUrl: string;
    server_error: string = '';
    server_success: string = '';
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthService,
        private alertService: AlertService,
        private ajaxApi: AjaxApiService,
    ) {
    }

    ngOnInit() {
    }
    sendPasswordReset() {
        this.ajaxApi.apiPost('password/email', this.model).then((res) => {
            if (res === 'passwords.sent') {
                this.server_success = 'We have e-mailed your password reset link!';
                this.server_error = '';
            }
            else if (res === 'passwords.user') {
                this.server_success = '';
                this.server_error = 'We can\'t find a user with that e-mail address.';
            }
            else {
                this.server_success = '';
                this.server_error = 'Something went wrong, please reload and try again.';
            }
        })
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { AlertComponent } from './alert/alert.component';
import { FormsModule } from '@angular/forms';
import {AlertService} from './services/alert.service';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

@NgModule({
    imports: [
        CommonModule,
        AuthRoutingModule,
        FormsModule
    ],
    declarations: [LoginComponent, AlertComponent, RegisterComponent, ForgotPasswordComponent, ResetPasswordComponent],
    providers: [AlertService]
})
export class AuthModule { }

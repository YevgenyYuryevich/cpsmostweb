import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from "@angular/router";
import {AlertService} from "../services/alert.service";
import {AuthService} from "../../../services/auth.service";
import {AjaxApiService} from "../../../services/ajax-api.service";

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    server_error: string = '';
    server_success: string = '';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthService,
        private alertService: AlertService,
        private ajaxApi: AjaxApiService,
    ) {
        this.model.token = this.route.snapshot.paramMap.get('token');
    }

    ngOnInit() {
    }
    resetPassword() {
        this.ajaxApi.apiPost('password/reset', this.model).then((res) => {
            console.log(res);
            if (res === 'passwords.reset') {
                this.server_success = 'Your password has been reset!';
                this.server_error = '';
                setTimeout(() => {
                    this.router.navigate(['/auth/dashboard']);
                }, 2000);
            } else if (res === 'passwords.user') {
                this.server_success = '';
                this.server_error = 'We can\'t find a user with that e-mail address.\n';
            } else {
                this.server_success = '';
                this.server_error = 'This password reset token is invalid.';
            }
        });
    }
}

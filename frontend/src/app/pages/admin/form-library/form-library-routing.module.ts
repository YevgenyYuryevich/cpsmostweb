import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ManageFormLibraryComponent} from './manage-form-library/manage-form-library.component';
import {FormLibraryResolveService} from '../../../resolves/form-library-resolve.service';

const routes: Routes = [
    {
        path: '',
        component: ManageFormLibraryComponent,
        resolve: {
            form_library: FormLibraryResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormLibraryRoutingModule { }

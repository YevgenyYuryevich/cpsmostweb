import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormLibraryRoutingModule } from './form-library-routing.module';
import { ManageFormLibraryComponent } from './manage-form-library/manage-form-library.component';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';
import {AppComponentsModule} from '../../../app-components/app-components.module';
import {UiSwitchModule} from 'ngx-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgxDatatableModule,
        PipesModule,
        AppComponentsModule,
        FormLibraryRoutingModule,
        UiSwitchModule
    ],
    declarations: [ManageFormLibraryComponent]
})
export class FormLibraryModule { }

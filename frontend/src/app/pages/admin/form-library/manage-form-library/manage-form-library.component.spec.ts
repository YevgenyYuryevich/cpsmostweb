import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageFormLibraryComponent } from './manage-form-library.component';

describe('ManageFormLibraryComponent', () => {
  let component: ManageFormLibraryComponent;
  let fixture: ComponentFixture<ManageFormLibraryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageFormLibraryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageFormLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

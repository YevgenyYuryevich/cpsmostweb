import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ConfirmaDialogService} from '../../../../app-components/confirm-dialog/confirm-dialog.service';
import {AuthService} from '../../../../services/auth.service';

@Component({
    selector: 'app-manage-form-library',
    templateUrl: './manage-form-library.component.html',
    styleUrls: ['./manage-form-library.component.scss']
})
export class ManageFormLibraryComponent implements OnInit {

    @ViewChild('fileCellTemplate') fileCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('addToVendorCellTemplate') addToVendorCellTemplate: TemplateRef<any>;
    @ViewChild('addToInstructorCellTemplate') addToInstructorCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    rows: any[] = [];
    columns: any[];
    table_page_size: Number = 10;
    search_q = '';
    canEdit = false;

    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService, private confirmDialogService: ConfirmaDialogService, private authService: AuthService) {
        this.rows = route.snapshot.data['form_library'].map(row => {
            row.add_to_vendor = parseInt(row.add_to_vendor, 10);
            row.add_to_instructor = parseInt(row.add_to_instructor, 10);
            return row;
        });
        this.canEdit = this.authService.auth.role.toLowerCase() === 'admin' || true;
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'File',
                prop: 'media_url',
                cellTemplate: this.fileCellTemplate,
                sortable: false,
                width: 50,
            }
        ];
        if (this.authService.auth.role.toLowerCase() === 'admin') {
            this.columns.push({
                    name: 'Add Form to Vendor Renewal',
                    prop: 'add_to_vendor',
                    cellTemplate: this.addToVendorCellTemplate,
                    sortable: false,
                    width: 100,
                });
            this.columns.push({
                name: 'Add Form to Instructor Renewal',
                prop: 'add_to_instructor',
                cellTemplate: this.addToInstructorCellTemplate,
                sortable: false,
                width: 100,
            });
        } else if (this.authService.auth.role.toLowerCase() === 'vendor') {
            this.columns.push({
                name: 'Add Form to Vendor Renewal',
                prop: 'add_to_vendor',
                cellTemplate: this.addToVendorCellTemplate,
                sortable: false,
                width: 100,
            });
        } else if (this.authService.auth.role.toLowerCase() === 'instructor') {
            this.columns.push({
                name: 'Add Form to Instructor Renewal',
                prop: 'add_to_instructor',
                cellTemplate: this.addToInstructorCellTemplate,
                sortable: false,
                width: 100,
            });
        }
        if (this.canEdit) {
            this.columns.push({
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 80,
                sortable: false,
            });
        }
    }

    refreshTable() {
        this.table.offset = 0;
    }

    public confirmationDelete(row) {
        this.confirmDialogService.confirm('Are you sure', 'Do you really want to delete this?', 'Delete', 'Cancel')
            .then((confirmed) => {
                if (confirmed) {
                    this.deleteRow(row);
                }
            })
            .catch(() => {
            });
    }

    deleteRow(row) {
        this.ajaxApi.apiPost('form-library/delete', {where: {id: row.id}}).then(res => {
            this.rows = this.rows.filter(r => r.id !== row.id);
        });
    }

    uploadFile(e) {
        if (e.target.files.length) {
            const formData = new FormData();
            formData.append('media', e.target.files[0]);
            this.ajaxApi.apiMultiPartPost('media/upload', formData, true, 100).then(res => {
                this.createFormLibrary({media_id: res.id});
            });
        }
    }

    createFormLibrary(sets) {
        return this.ajaxApi.apiPost('form-library/insert', {sets: sets}).then(res => {
            this.rows.push(res);
            this.rows = [...this.rows];
        });
    }

    addToVendor(row) {
        this.ajaxApi.apiPost('form-library/update', {
            where: {id: row.id},
            sets: {add_to_vendor: 1}
        });
        this.ajaxApi.apiPost('renewal-application/insert-from-form-library', {
            sets: {from_id: row.id, type: 'vendor'}
        });
    }

    removeFromVendor(row) {
        this.ajaxApi.apiPost('form-library/update', {
            where: {id: row.id},
            sets: {add_to_vendor: 0}
        });
        this.ajaxApi.apiPost('renewal-application/delete', {
            where: {from_id: row.id, type: 'vendor'}
        });
    }

    addToInstructor(row) {
        this.ajaxApi.apiPost('form-library/update', {
            where: {id: row.id},
            sets: {add_to_instructor: 1}
        });
        this.ajaxApi.apiPost('renewal-application/insert-from-form-library', {
            sets: {from_id: row.id, type: 'instructor'}
        });
    }

    removeFromInstructor(row) {
        this.ajaxApi.apiPost('form-library/update', {
            where: {id: row.id},
            sets: {add_to_instructor: 0}
        });
        this.ajaxApi.apiPost('renewal-application/delete', {
            where: {from_id: row.id, type: 'instructor'}
        });
    }
}

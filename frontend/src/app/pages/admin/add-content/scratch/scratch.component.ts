import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';

@Component({
    selector: 'app-scratch',
    templateUrl: './scratch.component.html',
    styleUrls: ['./scratch.component.scss']
})
export class ScratchComponent implements OnInit {

    sets: any = {
        type: 'text',
    };
    editorConfig = {
        editable: true,
        height: '200px',
        minHeight: '150px',
        translate: 'no',
    };
    imageFile: any = false;
    coverImageSrc = '';
    constructor(private ajaxApi: AjaxApiService) { }
    ngOnInit() {
    }
    save(e) {
        const formData = new FormData(e.target);
        formData.append('content', this.sets.content);
        return this.ajaxApi.apiMultiPartPost('posts/insert', formData).then((res) => {
            console.log(res);
        });
    }
    updateCoverImage(event) {
        if (event.target.files && event.target.files[0]) {
            this.imageFile = event.target.files[0];
            const reader: any = new FileReader();
            reader.onload = e => this.coverImageSrc = reader.result;
            reader.readAsDataURL(this.imageFile);
        } else {
            this.imageFile = false;
        }
    }
    onMediaUploaded(media) {
        if (media.url !== this.sets.content) {
            if (media.url + '<br>' === this.sets.content) {
                this.sets.content = media.url;
            } else {
                return false;
            }
        }
        switch (media.type) {
            case 'audio':
                this.sets.type = 'audio';
                break;
            case 'video':
                this.sets.type = 'video';
                break;
            default:
                break;
        }
    }
}

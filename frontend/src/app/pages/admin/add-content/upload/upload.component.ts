import {Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {AddContentCommonService} from '../services/add-content-common.service';
import {Unsubscribable} from 'rxjs';
import {FileUploadRowComponent} from '../../../../app-components/file-upload-row/file-upload-row.component';
import {HelperService} from '../../../../services/helper.service';
import {AjaxApiService} from '../../../../services/ajax-api.service';

@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit, OnDestroy {
    @ViewChildren('itemRow') itemRows: QueryList<FileUploadRowComponent>;
    files: any[] = [];
    fileUploadSubscribe: Unsubscribable;
    constructor(public addContentService: AddContentCommonService, public appHelper: HelperService, public ajaxApi: AjaxApiService) { }

    ngOnInit() {
        this.fileUploadSubscribe = this.addContentService.fileUpload$.subscribe((f) => {
            if ( f ) {
                this.files.push( f );
            }
        } );
    }
    ngOnDestroy() {
        this.fileUploadSubscribe.unsubscribe();
        this.addContentService.setFileUpload(false);
    }
    onItemDeleted(i) {
        this.files.splice(i, 1);
    }
    uploadAll() {
        const uploads = [];
        this.itemRows.map((item) => {
            uploads.push(item.upload());
        });
        return Promise.all(uploads).then((res) => {
            const posts = res.map((item) => {
                return this.appHelper.postFormat(item);
            });
            this.ajaxApi.apiPost('posts/insert_many', {data: posts}).then((r) => {
                console.log(r);
            });
        });
    }
    cancelAll() {
        this.files = [];
    }
}

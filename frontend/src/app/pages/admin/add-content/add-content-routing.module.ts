import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DefaultComponent} from './default/default.component';
import {ScratchComponent} from './scratch/scratch.component';
import {SearchComponent} from './search/search.component';
import {UploadComponent} from './upload/upload.component';

const routes: Routes = [
    {
        'path': '',
        component: DefaultComponent,
        children: [
            {
                path: 'scratch',
                component: ScratchComponent,
            },
            {
                path: 'search/:q',
                component: SearchComponent,
            },
            {
                path: 'search',
                component: SearchComponent,
            },
            {
                path: 'upload',
                component: UploadComponent,
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddContentRoutingModule { }

import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {AddContentCommonService} from '../services/add-content-common.service';

@Component({
    selector: 'app-default',
    templateUrl: './default.component.html',
    styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

    searchText = '';
    fileUpload: any;
    constructor(private router: Router, private addContentService: AddContentCommonService) {
    }

    ngOnInit() {
    }
    searchWeb() {
        if (this.searchText) {
            this.router.navigate(['/admin/add-content/search/' + this.searchText]).then((a) => {
            });
        }
    }
    uploadFile(e) {
        if (e.target.files && e.target.files[0]) {
            const f = e.target.files[0];
            this.addContentService.setFileUpload(f);
        }
    }
}

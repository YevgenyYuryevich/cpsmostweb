import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddContentRoutingModule } from './add-content-routing.module';
import { DefaultComponent } from './default/default.component';
import { ScratchComponent } from './scratch/scratch.component';
import {FormsModule} from '@angular/forms';
import { NgxEditorModule } from '../../../app-components/ngx-editor/src/app/ngx-editor/ngx-editor.module';
import { SearchComponent } from './search/search.component';
import {AddContentCommonService} from './services/add-content-common.service';
import {AppComponentsModule} from '../../../app-components/app-components.module';
import { UploadComponent } from './upload/upload.component';

@NgModule({
    imports: [
        CommonModule,
        AddContentRoutingModule,
        FormsModule,
        NgxEditorModule,
        AppComponentsModule
    ],
    declarations: [DefaultComponent, ScratchComponent, SearchComponent, UploadComponent],
    providers: [
        AddContentCommonService
    ]
})
export class AddContentModule { }

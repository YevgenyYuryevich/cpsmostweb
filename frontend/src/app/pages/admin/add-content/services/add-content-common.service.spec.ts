import { TestBed } from '@angular/core/testing';

import { AddContentCommonService } from './add-content-common.service';

describe('AddContentCommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddContentCommonService = TestBed.get(AddContentCommonService);
    expect(service).toBeTruthy();
  });
});

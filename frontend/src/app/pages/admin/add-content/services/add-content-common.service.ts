import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AddContentCommonService {

    private query: Subject<string> = new BehaviorSubject<string>('');
    private fileUpload: Subject<any> = new BehaviorSubject<any>(false);
    constructor() {
    }
    setQuery(q) {
        this.query.next(q);
    }
    get query$() {
        return this.query.asObservable();
    }
    setFileUpload(f) {
        this.fileUpload.next(f);
    }
    get fileUpload$() {
        return this.fileUpload.asObservable();
    }
}

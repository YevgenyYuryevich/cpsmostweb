import {AfterViewInit, Component, OnChanges, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute} from '@angular/router';
import {WebSearchSelectableComponent} from '../../../../app-components/web-search-selectable/web-search-selectable.component';
import {HelperService} from '../../../../services/helper.service';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, AfterViewInit {
    @ViewChild(WebSearchSelectableComponent)
    private searchSelectableComponent: WebSearchSelectableComponent;
    q = '';
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute, public appHelper: HelperService) {}
    ngOnInit() {
    }
    ngAfterViewInit() {
        this.route.params.subscribe(params => {
            this.q = params['q'] || '';
            if (this.q) {
                this.searchSelectableComponent.searchWeb(this.q);
            }
        });
    }
    saveItems(data) {
        const items = data.map((item) => {
            return this.appHelper.postFormat(item);
        });
        if (items.length) {
            this.ajaxApi.apiPost('posts/insert_many', {data: items}).then((res) => {
            });
        } else {
            alert('there is no new selected');
        }
    }
}

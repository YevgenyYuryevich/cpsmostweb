import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseTypesManageComponent } from './course-types-manage.component';

describe('CourseTypesManageComponent', () => {
  let component: CourseTypesManageComponent;
  let fixture: ComponentFixture<CourseTypesManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseTypesManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseTypesManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

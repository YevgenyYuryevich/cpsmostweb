import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {CourseTypeEditorComponent} from '../../../../app-components/course-type-editor/course-type-editor.component';
import {AjaxApiService} from '../../../../services/ajax-api.service';

@Component({
    selector: 'app-course-types-manage',
    templateUrl: './course-types-manage.component.html',
    styleUrls: ['./course-types-manage.component.scss']
})
export class CourseTypesManageComponent implements OnInit {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('htmlCellTemplate') htmlCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('imgCellTemplate') imgCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('courseTypeEditor') courseTypeEditor: CourseTypeEditorComponent;
    courseTypes: any[];
    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
        this.courseTypes = this.route.snapshot.data['course_types'];
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Image',
                cellTemplate: this.imgCellTemplate,
                sortable: false,
                width: 50,
            },
            {
                name: 'Name',
                cellTemplate: this.txtCellTemplate,
                width: 400,
            },
            {
                name: 'Description',
                cellTemplate: this.htmlCellTemplate,
                width: 800,
            },
            {
                name: 'Origin ID',
                prop: 'origin_id',
                cellTemplate: this.txtCellTemplate,
                width: 100,
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 80,
                sortable: false,
            },
        ];
    }
    editAction(row, index) {
        this.courseTypeEditor.setMode('update');
        this.courseTypeEditor.setCourseType(row);
        this.courseTypeEditor.open();
    }
    deleteAction(row, index) {
        if (confirm('Are you sure to delete?')) {
            this.ajaxApi.apiPost('course-types/delete', {where: {id: row.id}}).then(res => {
                this.courseTypes.splice(index, 1);
                this.courseTypes = [...this.courseTypes];
            });
        }
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
    onUpdate(row) {
        this.courseTypes = [...this.courseTypes];
    }
    onCreate(row) {
        this.courseTypes.push(row);
        this.courseTypes = [...this.courseTypes];
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseTypesRoutingModule } from './course-types-routing.module';
import { CourseTypesManageComponent } from './course-types-manage/course-types-manage.component';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';
import {AppComponentsModule} from '../../../app-components/app-components.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        CourseTypesRoutingModule,
        NgxDatatableModule,
        PipesModule,
        AppComponentsModule
    ],
    declarations: [CourseTypesManageComponent]
})
export class CourseTypesModule { }

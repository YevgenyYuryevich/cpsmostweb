import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CourseTypesManageComponent} from './course-types-manage/course-types-manage.component';
import {CourseTypeResolveService} from '../../../resolves/course-type-resolve.service';

const routes: Routes = [
    {
        path: 'manage',
        component: CourseTypesManageComponent,
        resolve: {
            course_types: CourseTypeResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseTypesRoutingModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemindersSentComponent } from './reminders-sent.component';

describe('RemindersSentComponent', () => {
  let component: RemindersSentComponent;
  let fixture: ComponentFixture<RemindersSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemindersSentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemindersSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

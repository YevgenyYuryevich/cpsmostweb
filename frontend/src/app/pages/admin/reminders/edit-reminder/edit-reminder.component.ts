import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';

@Component({
    selector: 'app-set-reminder',
    templateUrl: './edit-reminder.component.html',
    styleUrls: ['./edit-reminder.component.scss']
})
export class EditReminderComponent implements OnInit {

    sets: any = {
        name: '',
        body: '',
        group: 'instructor&provider',
        type: 'email&sms'
    };
    dateTime: Date;
    reminder: any = false;
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute, private router: Router, private datePipe: DatePipe) {
        this.reminder = this.route.snapshot.data['reminder'];
        this.dateTime = new Date();
        if (this.reminder) {
            Object.assign(this.sets, this.reminder);
            this.dateTime = new Date(this.sets.datetime);
            delete this.sets.id;
        }
    }

    ngOnInit() {
    }
    save() {
        this.sets.datetime = this.datePipe.transform(this.dateTime, 'yyyy-MM-dd');
        if (this.reminder) {
            this.update();
        } else {
            this.insert().then((res) => {
                this.router.navigateByUrl('/admin/reminders/current');
            });
        }
    }
    insert() {
        return this.ajaxApi.apiPost('reminders/insert', {sets: this.sets});
    }
    update() {
        return this.ajaxApi.apiPost('reminders/update', {where: {id: this.reminder.id}, sets: this.sets});
    }
}

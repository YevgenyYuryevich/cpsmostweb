import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import { RemindersRoutingModule } from './reminders-routing.module';
import { EditReminderComponent } from './edit-reminder/edit-reminder.component';
import {FormsModule} from '@angular/forms';
import { CurrentRemindersComponent } from './current-reminders/current-reminders.component';
import { RemindersSentComponent } from './reminders-sent/reminders-sent.component';
import {PipesModule} from '../../../pipes/pipes.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RemindersRoutingModule,
        PipesModule,
        NgxDatatableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
    ],
    declarations: [EditReminderComponent, CurrentRemindersComponent, RemindersSentComponent],
    providers: [
        DatePipe
    ]
})
export class RemindersModule { }

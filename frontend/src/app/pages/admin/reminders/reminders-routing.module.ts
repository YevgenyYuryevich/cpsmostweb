import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EditReminderComponent} from './edit-reminder/edit-reminder.component';
import {CurrentRemindersComponent} from './current-reminders/current-reminders.component';
import {RemindersResolveService} from '../../../resolves/reminders-resolve.service';
import {RemindersSentComponent} from './reminders-sent/reminders-sent.component';
import {ReminderResolveService} from '../../../resolves/reminder-resolve.service';

const routes: Routes = [
    {
        path: 'create',
        component: EditReminderComponent
    },
    {
        path: 'current',
        component: CurrentRemindersComponent,
        resolve: {
            reminders: RemindersResolveService
        }
    },
    {
        path: 'edit/:id',
        component: EditReminderComponent,
        resolve: {
            reminder: ReminderResolveService
        }
    },
    {
        path: 'sent',
        component: RemindersSentComponent,
        resolve: {
            reminders: RemindersResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RemindersRoutingModule { }

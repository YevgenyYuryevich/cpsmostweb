import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentRemindersComponent } from './current-reminders.component';

describe('CurrentRemindersComponent', () => {
  let component: CurrentRemindersComponent;
  let fixture: ComponentFixture<CurrentRemindersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentRemindersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentRemindersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

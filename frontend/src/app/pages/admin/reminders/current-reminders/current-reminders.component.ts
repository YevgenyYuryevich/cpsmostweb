import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../../../services/ajax-api.service';

@Component({
    selector: 'app-current-reminders',
    templateUrl: './current-reminders.component.html',
    styleUrls: ['./current-reminders.component.scss']
})
export class CurrentRemindersComponent implements OnInit {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    rows: any[] = [];
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
        this.rows = this.route.snapshot.data['reminders'].filter(row => {
            return parseInt(row.sent) === 0;
        });
    }
    ngOnInit() {
        this.columns = [
            {
                name: 'Name',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Body',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Group',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Type',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Datetime',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 100,
                sortable: false,
            },
        ];
    }
    deleteAction(row, index) {
        if (confirm('Are you sure to delete?')) {
            this.ajaxApi.apiPost('reminders/delete', {where: {id: row.id}}).then(res => {
                this.rows.splice(index, 1);
                this.rows = [...this.rows];
            });
        }
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
}

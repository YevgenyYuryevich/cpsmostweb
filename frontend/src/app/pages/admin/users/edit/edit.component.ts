import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AjaxApiService} from "../../../../services/ajax-api.service";
import {AppCommonService} from "../../../../services/app-common.service";

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

    user: any;
    password: string = '';
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService, public appCommonService: AppCommonService) {
        this.user = this.route.snapshot.data['user'];
    }
    save() {
        let sets:any = {
            name: this.user.name,
            email: this.user.email,
            level: this.user.level,
        }
        if (this.password) {
            sets.password = this.password;
        }
        this.ajaxApi.apiPost('users/update', {where: this.user.id, sets: sets}).then((res) => {
            console.log(res);
        });
    }
    ngOnInit() {
    }
}

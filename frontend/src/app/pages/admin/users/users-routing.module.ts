import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EditComponent} from './edit/edit.component';
import {EditResolveService} from './services/edit-resolve.service';
import {UsersListComponent} from './users-list/users-list.component';
import {UsersResolveService} from './services/users-resolve.service';
import {AddComponent} from './add/add.component';
import {GetProviderResolveService} from '../../../resolves/get-provider-resolve.service';

const routes: Routes = [
    {
        path: '',
        component: UsersListComponent,
        resolve: {
            users: UsersResolveService,
        }
    },
    {
        path: 'edit/:id',
        component: EditComponent,
        resolve: {
            user: EditResolveService
        }
    },
    {
        path: 'add',
        component: AddComponent,
        resolve: {
            provider: GetProviderResolveService
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersRoutingModule {
}

import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AjaxApiService} from "../../../../services/ajax-api.service";
import {AppCommonService} from "../../../../services/app-common.service";
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('statusCellTemplate') statusCellTemplate: TemplateRef<any>;
    @ViewChild('qualityCellTemplate') qualityCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    users: any[];
    columns: any[];
    table_page_size: any = 10;
    search_q = '';

    constructor(private route: ActivatedRoute, private router: Router, private ajaxApi: AjaxApiService, public appCommonService: AppCommonService) {
        this.users = this.route.snapshot.data['users'];
        this.users.forEach((user) => {
            const role = appCommonService.userLevels.find((l) => {
                return user.level == l.id;
            });
            user.levelName = role ? role.name : '';
            user.approved = user.status === 'approved';
            user.quality_assurance = parseInt(user.quality_assurance);
        });
    }
    editUser(row) {
        this.router.navigate(['/admin/users/edit/' + row.id]);
    }
    onDelete(row) {
        if (window.confirm('Are you sure to remove this user?')) {
            this.ajaxApi.apiGet('users/delete/' + row.id).then((res) => {
                if (res) {
                    this.users.splice(row.index, 1);
                    location.reload();
                }
                else {
                    alert('The Root Administrator can not be deleted');
                }
            });
        }
    }
    updateUser(user, sets) {
        return this.ajaxApi.apiPost('users/update', {where: user.id, sets: sets}).then(res => {
            this.users.map(u => {
                if (u.id === user.id) {
                    Object.assign(u, sets);
                }
            });
        });
    }
    ngOnInit() {
        this.columns = [
            {
                name: 'Name',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'E-mail',
                prop: 'email',
                cellTemplate: this.txtCellTemplate,
                width: 300,
            },
            {
                name: 'Level',
                prop: 'levelName',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Company',
                cellTemplate: this.txtCellTemplate,
            },
            {
                name: 'Address',
                cellTemplate: this.txtCellTemplate
            },
            {
                name: 'Phone',
                cellTemplate: this.txtCellTemplate
            },
            {
                name: 'Status',
                prop: 'approved',
                cellTemplate: this.statusCellTemplate
            },
            {
                name: 'Quality Assurance',
                prop: 'quality_assurance',
                cellTemplate: this.qualityCellTemplate
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 80,
                sortable: false,
            },
        ];
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
}

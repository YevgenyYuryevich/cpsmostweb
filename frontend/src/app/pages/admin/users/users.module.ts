import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { EditComponent } from './edit/edit.component';
import { UsersListComponent } from './users-list/users-list.component';
import {AngularFontAwesomeModule} from "angular-font-awesome";
import {FormsModule} from "@angular/forms";
import {AddComponent} from "./add/add.component";
import {Ng2SmartTableModule} from "ng2-smart-table";
import {PipesModule} from '../../../pipes/pipes.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {UiSwitchModule} from 'ngx-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        UsersRoutingModule,
        Ng2SmartTableModule,
        AngularFontAwesomeModule,
        FormsModule,
        PipesModule,
        NgxDatatableModule,
        UiSwitchModule,
    ],
    declarations: [EditComponent, UsersListComponent, AddComponent]
})
export class UsersModule { }

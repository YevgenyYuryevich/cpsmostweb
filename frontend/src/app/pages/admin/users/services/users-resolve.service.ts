import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AjaxApiService} from '../../../../services/ajax-api.service';

@Injectable({
  providedIn: 'root'
})
export class UsersResolveService implements Resolve<any> {

  constructor(private ajaxApi: AjaxApiService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      return this.ajaxApi.apiPost('users/get-many').then((res) => {
          return res;
      });
  }
}

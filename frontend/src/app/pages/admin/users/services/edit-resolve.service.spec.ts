import { TestBed } from '@angular/core/testing';

import { EditResolveService } from './edit-resolve.service';

describe('EditResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditResolveService = TestBed.get(EditResolveService);
    expect(service).toBeTruthy();
  });
});

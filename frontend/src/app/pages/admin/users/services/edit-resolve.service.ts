import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {AjaxApiService} from "../../../../services/ajax-api.service";

@Injectable({
  providedIn: 'root'
})
export class EditResolveService implements Resolve<any>{

  constructor(private ajaxApi: AjaxApiService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let id = route.paramMap.get('id');
      return this.ajaxApi.apiGet('users/get/' + id, true);
  }
}

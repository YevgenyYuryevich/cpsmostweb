import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppCommonService} from '../../../../services/app-common.service';

@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    sets: any = {
        name: '',
        email: '',
        level: 2,
        password: '',
    };
    provider: any;
    constructor(private ajaxApi: AjaxApiService, private router: Router, public appCommonService: AppCommonService, private route: ActivatedRoute) {
        if (route.snapshot.queryParams.provider) {
            this.provider = route.snapshot.data['provider'];
            this.sets.name = this.provider.contact_person;
            this.sets.email = this.provider.contact_email;
            this.sets.provider = route.snapshot.queryParams.provider;
            const providerLevel = appCommonService.userLevels.find((level) => {
                return level.name === 'provider';
            });
            if (providerLevel) {
                this.sets.level = providerLevel.id;
            }
        }
    }
    save() {
        this.ajaxApi.apiPost('users/insert', this.sets).then((res) => {
            this.router.navigate(['/admin/users']);
        });
    }
    ngOnInit() {
    }
}

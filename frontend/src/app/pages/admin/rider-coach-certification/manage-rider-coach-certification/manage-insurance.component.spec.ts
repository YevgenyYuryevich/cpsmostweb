import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRiderCoachCertificationComponent } from './manage-insurance.component';

describe('ManageInsuranceComponent', () => {
  let component: ManageRiderCoachCertificationComponent;
  let fixture: ComponentFixture<ManageRiderCoachCertificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRiderCoachCertificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRiderCoachCertificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HelperService} from '../../../../services/helper.service';
import {AuthService} from '../../../../services/auth.service';

@Component({
    selector: 'app-manage-rider-coach-certification',
    templateUrl: './manage-rider-coach-certification.component.html',
    styleUrls: ['./manage-rider-coach-certification.component.scss']
})
export class ManageRiderCoachCertificationComponent implements OnInit {

    @ViewChild('mediaCellTemplate') mediaCellTemplate: TemplateRef<any>;
    @ViewChild('instructorCellTemplate') instructorCellTemplate: TemplateRef<any>;
    @ViewChild('stateCellTemplate') stateCellTemplate: TemplateRef<any>;
    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    rows: any[] = [];
    instructors: any[];
    insuranceOptions: any;
    constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer, private modalService: NgbModal, helperService: HelperService, public authService: AuthService) {
        this.instructors = this.route.snapshot.data['instructors'];
        this.insuranceOptions = this.route.snapshot.data['insurance_options'];
        this.rows = this.route.snapshot.data['rows'].map(row => {
            const instructor = this.instructors.find(ist => ist.id == row.instructor);
            row.instructor = instructor;
            row.instructorName = instructor ? instructor.name : 'unassigned';
            row.sourceType = helperService.getSourceTypeFromUrl(row.source);
            row.source = this.sanitizer.bypassSecurityTrustResourceUrl(row.source);
            return row;
        });
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Submission',
                prop: 'source',
                cellTemplate: this.mediaCellTemplate,
                width: 10,
            },
            {
                name: 'Instructor',
                prop: 'instructor',
                cellTemplate: this.instructorCellTemplate,
                width: 200,
            },
            {
                name: 'Submission Date',
                prop: 'submission_date',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'State',
                cellTemplate: this.stateCellTemplate,
                width: 100,
                sortable: false,
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 50,
                sortable: false,
            },
        ];
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
    approve(event) {
    }
    deny(id) {
    }
}

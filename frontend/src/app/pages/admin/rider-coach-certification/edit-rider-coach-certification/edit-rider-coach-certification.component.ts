import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../services/auth.service';
import {HelperService} from '../../../../services/helper.service';

@Component({
    selector: 'app-edit-rider-coach-certification',
    templateUrl: './edit-rider-coach-certification.component.html',
    styleUrls: ['./edit-rider-coach-certification.component.scss']
})
export class EditRiderCoachCertificationComponent implements OnInit {

    row: any;
    media: File;
    mediaType: string;
    instructors: any[];
    sets: any = {};
    constructor(
        private route: ActivatedRoute,
        private ajaxApi: AjaxApiService,
        private router: Router,
        public authService: AuthService,
        public helperService: HelperService
    ) {
        if (route.snapshot.data['row']) {
            this.row = this.route.snapshot.data['row'];
            this.sets.instructor = this.row.instructor;
            this.sets.source = this.row.source;
            this.mediaType = this.helperService.getSourceTypeFromUrl(this.row.source);
        }
        if (this.authService.auth.role.toLowerCase() === 'instructor') {
            this.sets.instructor = this.authService.auth.id;
        } else {
            this.instructors = this.route.snapshot.data['instructors'];
        }
    }

    ngOnInit() {
    }
    insert() {
        const formData = new FormData();
        formData.append('source', this.media);
        formData.append('sets', JSON.stringify(this.sets));
        this.ajaxApi.apiMultiPartPost('rider-coach-certification/insert', formData).then(res => {
            return this.router.navigateByUrl('/admin/rider-coach-certification/manage');
        });
    }
    update() {
        const formData = new FormData();
        formData.append('where', JSON.stringify({id: this.row.id}));
        if (this.media) {
            formData.append('source', this.media);
        }
        formData.append('sets', JSON.stringify(this.sets));
        this.ajaxApi.apiMultiPartPost('rider-coach-certification/update', formData).then(res => {
            return this.router.navigateByUrl('/admin/rider-coach-certification/manage');
        });
    }
    save() {
        if (this.row) {
            this.update();
        } else {
            this.insert();
        }
    }
    setMedia(event) {
        if (event.target.files && event.target.files[0]) {
            this.media = event.target.files[0];
            this.sets.source = URL.createObjectURL(this.media);
            this.sets.source = this.helperService.trustUrl(this.sets.source);
            this.mediaType = this.helperService.getSourceTypeFromUrl(this.media.name);
        } else {
            return false;
        }
    }
}

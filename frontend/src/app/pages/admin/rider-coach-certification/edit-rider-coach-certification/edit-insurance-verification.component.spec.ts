import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRiderCoachCertificationComponent } from './edit-insurance-verification.component';

describe('EditInsuranceVerificationComponent', () => {
  let component: EditRiderCoachCertificationComponent;
  let fixture: ComponentFixture<EditRiderCoachCertificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRiderCoachCertificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRiderCoachCertificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

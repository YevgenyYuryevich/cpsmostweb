import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditRiderCoachCertificationComponent } from './edit-rider-coach-certification/edit-rider-coach-certification.component';
import { InstructorsResolveService } from '../../../resolves/instructors-resolve.service';
import { ManageRiderCoachCertificationComponent } from './manage-rider-coach-certification/manage-rider-coach-certification.component';
import { InsuranceOptionsResolveService } from '../../../resolves/insurance-options-resolve.service';
import {RiderCoachCertificationResolveService} from '../../../resolves/rider-coach-certification-resolve.service';
import {RiderCoachCertificationsResolveService} from '../../../resolves/rider-coach-certifications-resolve.service';

const routes: Routes = [
    {
        path: 'create',
        component: EditRiderCoachCertificationComponent,
        resolve: {
            instructors: InstructorsResolveService
        }
    },
    {
        path: 'edit/:id',
        component: EditRiderCoachCertificationComponent,
        resolve: {
            instructors: InstructorsResolveService,
            row: RiderCoachCertificationResolveService
        }
    },
    {
        path: 'manage',
        component: ManageRiderCoachCertificationComponent,
        resolve: {
            instructors: InstructorsResolveService,
            rows: RiderCoachCertificationsResolveService,
            insurance_options: InsuranceOptionsResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RiderCoachCertificationRoutingModule { }

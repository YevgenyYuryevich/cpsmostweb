import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RiderCoachCertificationRoutingModule } from './rider-coach-certification-routing.module';
import { EditRiderCoachCertificationComponent } from './edit-rider-coach-certification/edit-rider-coach-certification.component';
import {FormsModule} from '@angular/forms';
import { ManageRiderCoachCertificationComponent } from './manage-rider-coach-certification/manage-rider-coach-certification.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';
import {NgbButtonsModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponentsModule} from '../../../app-components/app-components.module';

@NgModule({
    imports: [
        CommonModule,
        RiderCoachCertificationRoutingModule,
        FormsModule,
        NgxDatatableModule,
        PipesModule,
        NgbButtonsModule,
        NgbModalModule,
        AppComponentsModule
    ],
    declarations: [EditRiderCoachCertificationComponent, ManageRiderCoachCertificationComponent]
})
export class RiderCoachCertificationModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CoursesListComponent} from './courses-list/courses-list.component';
import {AddCourseComponent} from './add-course/add-course.component';
import {CourseTypeResolveService} from '../../../resolves/course-type-resolve.service';
import {CourseProviderResolveService} from '../../../resolves/course-provider-resolve.service';
import {CoursesResolveService} from '../../../resolves/courses-resolve.service';

const routes: Routes = [
    {
        path: '',
        component: CoursesListComponent,
        resolve: {
            courses: CoursesResolveService,
            types: CourseTypeResolveService,
            providers: CourseProviderResolveService,
        }
    },
    {
        path: 'add',
        component: AddCourseComponent,
        resolve: {
            types: CourseTypeResolveService,
            providers: CourseProviderResolveService,
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoursesRoutingModule { }

import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {CourseEditorComponent} from '../../../../app-components/course-editor/course-editor.component';
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
    selector: 'app-courses-list',
    templateUrl: './courses-list.component.html',
    styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements OnInit {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('imgCellTemplate') imgCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild(CourseEditorComponent) courseEditor: CourseEditorComponent;
    courses: any[] = [];
    types: any[] = [];
    providers: any[] = [];
    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
        this.courses = this.route.snapshot.data['courses'];
        this.types = this.route.snapshot.data['types'];
        this.providers = this.route.snapshot.data['providers'];
    }
    formatting(course) {
        const type = this.types.find((t) => {
            return parseInt(course.type) === parseInt(t.id);
        });
        course.typeName = type ? type.name : '';
        const provider = course.providerCompanyName = this.providers.find((p) => {
            return parseInt(course.provider_company) === parseInt(p.id);
        });
        course.providerCompanyName = provider ? provider.name : '';
    }
    ngOnInit() {
        this.columns = [
            {
                name: 'Image',
                cellTemplate: this.imgCellTemplate,
                sortable: false,
                width: 50,
            },
            {
                name: 'Name',
                cellTemplate: this.txtCellTemplate,
                width: 300,
            },
            {
                name: 'Type',
                prop: 'typeName',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Provider Company',
                prop: 'providerCompanyName',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Instructor',
                cellTemplate: this.txtCellTemplate,
            },
            {
                name: 'Phone',
                cellTemplate: this.txtCellTemplate
            },
            {
                name: 'Email',
                cellTemplate: this.txtCellTemplate
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 80,
                sortable: false,
            },
        ];
        this.courses.forEach((course) => {
            this.formatting(course);
        });
    }
    editAction(row, index) {
        this.courseEditor.setCourse(row);
        this.courseEditor.open();
    }
    deleteAction(row, index) {
        if (confirm('Are you sure to delete?')) {
            this.ajaxApi.apiPost('courses/delete', {where: {id: row.id}}).then(res => {
                this.courses.splice(index, 1);
                this.courses = [...this.courses];
            });
        }
    }
    onUpdate(row) {
        this.formatting(row);
        this.courses = [...this.courses];
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
}

import {Component, OnInit, ViewChild} from '@angular/core';
import {CourseTypeEditorComponent} from '../../../../app-components/course-type-editor/course-type-editor.component';
import {ActivatedRoute, Route} from '@angular/router';
import {CourseProviderEditorComponent} from '../../../../app-components/course-provider-editor/course-provider-editor.component';
import {AjaxApiService} from '../../../../services/ajax-api.service';

@Component({
    selector: 'app-add-course',
    templateUrl: './add-course.component.html',
    styleUrls: ['./add-course.component.scss']
})
export class AddCourseComponent implements OnInit {

    @ViewChild(CourseTypeEditorComponent) courseTypeEditor: CourseTypeEditorComponent;
    @ViewChild(CourseProviderEditorComponent) courseProviderEditor: CourseProviderEditorComponent;
    sets: any = {};
    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    courseTypes: any[] = [];
    courseProviders: any[] = [];
    coverImageSrc: any;
    imageFile: any;
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
        this.courseTypes = this.route.snapshot.data['types'];
        this.courseProviders = this.route.snapshot.data['providers'];
    }

    ngOnInit() {
    }
    onTypeCreate(t) {
        this.courseTypes.push(t);
    }
    onProviderCreate(p) {
        this.courseProviders.push(p);
    }
    updateCoverImage(event) {
        if (event.target.files && event.target.files[0]) {
            this.imageFile = event.target.files[0];
            const reader: any = new FileReader();
            reader.onload = e => this.coverImageSrc = reader.result;
            reader.readAsDataURL(this.imageFile);
        } else {
            this.imageFile = false;
        }
    }
    save() {
        const ajaxData = new FormData();
        if (this.imageFile) {
            ajaxData.append('image', this.imageFile);
        }
        ajaxData.append('sets', JSON.stringify(this.sets));
        return this.ajaxApi.apiMultiPartPost('courses/insert', ajaxData).then(res => {
           console.log(res);
        });
    }
}

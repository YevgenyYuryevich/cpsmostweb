import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesListComponent } from './courses-list/courses-list.component';
import {AddCourseComponent} from './add-course/add-course.component';
import {FormsModule} from '@angular/forms';
import { NgxEditorModule } from '../../../app-components/ngx-editor/src/app/ngx-editor/ngx-editor.module';
import {CourseTypeResolveService} from '../../../resolves/course-type-resolve.service';
import {CourseProviderResolveService} from '../../../resolves/course-provider-resolve.service';
import {AppComponentsModule} from '../../../app-components/app-components.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';

@NgModule({
    imports: [
        CommonModule,
        CoursesRoutingModule,
        FormsModule,
        NgxEditorModule,
        AppComponentsModule,
        NgxDatatableModule,
        PipesModule,
    ],
    declarations: [CoursesListComponent, AddCourseComponent],
    providers: [
        CourseTypeResolveService,
        CourseProviderResolveService,
    ]
})
export class CoursesModule { }

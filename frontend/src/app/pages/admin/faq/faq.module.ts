import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FaqRoutingModule } from './faq-routing.module';
import { EditFaqComponent } from './edit-faq/edit-faq.component';
import { ManageFaqComponent } from './manage-faq/manage-faq.component';
import {FormsModule} from '@angular/forms';
import {PipesModule} from '../../../pipes/pipes.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {SortablejsModule} from 'angular-sortablejs';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FaqRoutingModule,
        PipesModule,
        NgxDatatableModule,
        SortablejsModule.forRoot({ animation: 150 }),
    ],
    declarations: [EditFaqComponent, ManageFaqComponent]
})
export class FaqModule { }

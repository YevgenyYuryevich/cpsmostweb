import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EditFaqComponent} from './edit-faq/edit-faq.component';
import {FaqResolveService} from '../../../resolves/faq-resolve.service';
import {ManageFaqComponent} from './manage-faq/manage-faq.component';
import {FaqsResolveService} from '../../../resolves/faqs-resolve.service';

const routes: Routes = [
    {
        path: 'add',
        component: EditFaqComponent
    },
    {
        path: 'edit/:id',
        component: EditFaqComponent,
        resolve: {
            faq: FaqResolveService
        }
    },
    {
        path: 'manage',
        component: ManageFaqComponent,
        resolve: {
            faqs: FaqsResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaqRoutingModule { }

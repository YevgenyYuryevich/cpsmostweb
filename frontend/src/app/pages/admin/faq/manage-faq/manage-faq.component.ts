import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-manage-faq',
    templateUrl: './manage-faq.component.html',
    styleUrls: ['./manage-faq.component.scss']
})
export class ManageFaqComponent implements OnInit {
    rows: any[] = [];
    options: any = {
        onUpdate: (event: any) => {
            this.sortUpdate(event);
        }
    };
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute) {
        this.rows = this.route.snapshot.data['faqs'];
        this.rows.sort((a, b) => {
            const av = parseInt(a.sort);
            const bv = parseInt(b.sort);
            return av - bv;
        });
    }
    sortUpdate(event) {
        const updates = [];
        this.rows.forEach((row, i) => {
            if (parseInt(row.sort) !== i + 1) {
                updates.push({
                    where: {id: row.id},
                    sets: {sort: i + 1}
                });
                row.sort = i + 1;
            }
        });
        this.ajaxApi.apiPost('faqs/update-many', {updates: updates}).then(() => {});
    }
    ngOnInit() {
    }
    deleteAction(row, index) {
        if (confirm('Are you sure to delete?')) {
            this.ajaxApi.apiPost('faqs/delete', {where: {id: row.id}}).then(res => {
                this.rows.splice(index, 1);
                this.rows = [...this.rows];
            });
        }
    }
}

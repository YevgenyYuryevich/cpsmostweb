import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-edit-faq',
    templateUrl: './edit-faq.component.html',
    styleUrls: ['./edit-faq.component.scss']
})
export class EditFaqComponent implements OnInit {

    faq: any = false;
    sets: any = {
        question: '',
        answer: '',
    };
    constructor(private ajaxApi: AjaxApiService, private router: Router, private route: ActivatedRoute) {
        this.faq = this.route.snapshot.data['faq'];
        if (this.faq) {
            Object.assign(this.sets, this.faq);
            delete this.sets.id;
        }
    }

    ngOnInit() {
    }
    insert() {
        this.ajaxApi.apiPost('faqs/insert', {sets: this.sets}).then(() => {
            this.router.navigateByUrl('/admin/faq/manage');
        });
    }
    update() {
        this.ajaxApi.apiPost('faqs/update', {where: {id: this.faq.id}, sets: this.sets}).then(() => {
            this.router.navigateByUrl('/admin/faq/manage');
        });
    }
}

import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AppCommonService} from '../../../services/app-common.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AjaxApiService} from '../../../services/ajax-api.service';
import {HelperService} from '../../../services/helper.service';

import {NgZone} from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

declare var google: any;
am4core.useTheme(am4themes_animated);

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('statusCellTemplate') statusCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('mediaCellTemplate') mediaCellTemplate: TemplateRef<any>;
    @ViewChild('insuranceStateCellTemplate') insuranceStateCellTemplate: TemplateRef<any>;
    @ViewChild('renewalStateCellTemplate') renewalStateCellTemplate: TemplateRef<any>;

    courseViews: any[] = [];
    courseClicks: any[] = [];

    courseAllViews = 0;
    courseThisViews = 0;
    courseAllClicks = 0;
    courseThisClicks = 0;
    courseAllConversionRate = 0;
    courseThisConversionRate = 0;
    courseChange = 0;

    users: any[];
    instructors: any[];
    vendors: any[];

    pendingInstructors: any[];
    pendingInstructorColumns: any[];

    insuranceOptions: any;
    pendingInsurances: any[];
    pendingInsuranceColumns: any[];

    renewalApplicationOptions: any;
    pendingRenewals: any[];
    pendingVendorRenewals: any[];
    pendingInstructorRenewals: any[];
    pendingVendorRenewalColumns: any[];
    pendingInstructorRenewalColumns: any[];

    incidents: any[];
    incidentColumns: any[];

    lat = 40;
    lng = -106;

    private map: any = null;
    private heatmap: any = null;

    private chart: am4charts.XYChart;

    constructor(private route: ActivatedRoute, private router: Router, private ajaxApi: AjaxApiService, public appCommonService: AppCommonService, public helperService: HelperService, private zone: NgZone) {
        this.courseViews = this.route.snapshot.data['course_tracks']['views'];
        this.courseAllViews = this.courseViews.length;
        this.courseThisViews = this.courseViews.filter(view => {
            const viewDate = new Date(view.date);
            const now = new Date();
            return viewDate.getUTCFullYear() === now.getUTCFullYear() && viewDate.getUTCMonth() === now.getUTCMonth();
        }).length;

        this.courseClicks = this.route.snapshot.data['course_tracks']['clicks'];
        this.courseAllClicks = this.courseClicks.length;
        this.courseThisClicks = this.courseClicks.filter(click => {
            const clickDate = new Date(click.date);
            const now = new Date();
            return clickDate.getUTCFullYear() === now.getUTCFullYear() && clickDate.getUTCMonth() === now.getUTCMonth();
        }).length;
        this.courseAllConversionRate = this.courseAllClicks / this.courseAllViews * 100;
        this.courseThisConversionRate = this.courseThisClicks / this.courseThisViews * 100;

        this.courseChange = ((this.courseThisClicks / this.courseThisViews) - (this.courseAllClicks / this.courseAllViews)) * 100;
        this.users = this.route.snapshot.data['users'].map(user => {
            user.approved = user.status === 'approved';
            return user;
        });
        this.instructors = this.route.snapshot.data['instructors'].map(instructor => {
            instructor.approved = instructor.status === 'approved';
            return instructor;
        });
        this.vendors = this.route.snapshot.data['vendors'].map(vendor => {
            vendor.approved = vendor.status === 'approved';
            return vendor;
        });
        this.pendingInstructors = this.route.snapshot.data['instructors'].map(instructor => {
            instructor.approved = instructor.status === 'approved';
            return instructor;
        }).filter(instructor => instructor.status === 'pending');
        this.pendingInsurances = this.route.snapshot.data['insurances'].map(insurance => {
            const user = this.users.find(u => u.id == insurance.user_id);
            insurance.user = user;
            insurance.userName = user ? user.name : 'unassigned';
            insurance.sourceType = helperService.getSourceTypeFromUrl(insurance.source);
            // insurance.source = this.sanitizer.bypassSecurityTrustResourceUrl(insurance.source);
            return insurance;
        }).filter(insurance => insurance.state === 'pending');
        this.insuranceOptions = this.route.snapshot.data['insurance_options'];

        this.pendingRenewals = this.route.snapshot.data['renewals'].map(renewal => {
            const user = this.users.find(u => u.id == renewal.user_id);
            renewal.sourceType = 'document';
            renewal.user = user;
            renewal.userName = user ? user.name : 'unassigned';
            return renewal;
        }).filter(renewal => renewal.state === 'pending');
        this.pendingVendorRenewals = this.pendingRenewals.filter(r => r.type === 'vendor');
        this.pendingInstructorRenewals = this.pendingRenewals.filter(r => r.type === 'instructor');
        this.renewalApplicationOptions = this.route.snapshot.data['renewal_options'];

        this.incidents = this.route.snapshot.data['incidents'].map(incident => {
            const vendor = this.vendors.find(v => v.id == incident.vendor);
            incident.vendorName = vendor ? vendor.name : 'unassigned';
            return incident;
        });
    }

    ngOnInit() {
        this.pendingInstructorColumns = [
            {
                name: 'Name',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'E-mail',
                prop: 'email',
                cellTemplate: this.txtCellTemplate,
                width: 300,
            },
            {
                name: 'Company',
                cellTemplate: this.txtCellTemplate,
            },
            {
                name: 'Address',
                cellTemplate: this.txtCellTemplate
            },
            {
                name: 'Phone',
                cellTemplate: this.txtCellTemplate
            },
            {
                name: 'Status',
                prop: 'approved',
                cellTemplate: this.statusCellTemplate
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 80,
                sortable: false,
            },
        ];
        this.pendingInsuranceColumns = [
            {
                name: 'Name',
                prop: 'userName',
                cellTemplate: this.txtCellTemplate,
                width: 100,
            },
            {
                name: 'View',
                prop: 'source',
                cellTemplate: this.mediaCellTemplate,
                width: 100,
            },
            {
                name: 'State',
                cellTemplate: this.insuranceStateCellTemplate,
                width: 100,
                sortable: false,
            },
        ];
        this.pendingVendorRenewalColumns = [
            {
                name: 'Application',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: 'Driving Record',
                prop: 'driving_record',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: 'PDW',
                prop: 'pdw',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: 'Vendor',
                prop: 'userName',
                cellTemplate: this.txtCellTemplate,
                width: 100,
            },
            {
                name: 'State',
                cellTemplate: this.renewalStateCellTemplate,
                width: 100,
                sortable: false,
            },
        ];
        this.pendingInstructorRenewalColumns = [
            {
                name: 'Application',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: 'Driving Record',
                prop: 'driving_record',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: 'PDW',
                prop: 'pdw',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: 'Instructor',
                prop: 'userName',
                cellTemplate: this.txtCellTemplate,
                width: 100,
            },
            {
                name: 'State',
                cellTemplate: this.renewalStateCellTemplate,
                width: 100,
                sortable: false,
            },
        ];
        this.incidentColumns = [
            {
                name: 'Name',
                prop: 'userName',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Location',
                prop: 'address',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Date',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
        ];
    }

    editUser(row) {
        this.router.navigate(['/admin/users/edit/' + row.id]);
    }

    onDeleteUser(row) {
        if (window.confirm('Are you sure to remove this instructor?')) {
            this.ajaxApi.apiGet('users/delete/' + row.id).then((res) => {
                if (res) {
                    this.pendingInstructors = this.pendingInstructors.filter(instructor => {
                        return instructor.id !== row.id;
                    });
                } else {
                    alert('The Root Administrator can not be deleted');
                }
            });
        }
    }

    updateUser(user, sets) {
        return this.ajaxApi.apiPost('users/update', {where: user.id, sets: sets});
    }

    onMapLoad(mapInstance) {
        this.map = mapInstance;

        // here our in other method after you get the coords; but make sure map is loaded

        const coords = [];
        this.incidents.forEach(incident => {
            if (incident.geolocation_lat && incident.geolocation_lng) {
                coords.push(new google.maps.LatLng(parseFloat(incident.geolocation_lat), parseFloat(incident.geolocation_lng)));
            }
        });
        if (coords.length) {
            console.log(google.maps.visualization);
            this.heatmap = new google.maps.visualization.HeatmapLayer({
                map: this.map,
                data: coords
            });
        }
    }

    ngAfterViewInit() {
        const incidentsByDay = [0, 0, 0, 0, 0, 0, 0];
        this.incidents.forEach(incident => {
            const date = new Date(incident.date);
            const day = date.getUTCDay();
            incidentsByDay[(day - 1 + 7) % 7]++;
        });
        this.zone.runOutsideAngular(() => {
            const chart = am4core.create('incidents-chart-by-day', am4charts.XYChart3D);
            chart.data = [{
                'day': 'Monday',
                'income': incidentsByDay[0],
                'color': chart.colors.next()
            }, {
                'day': 'Tuesday',
                'income': incidentsByDay[1],
                'color': chart.colors.next()
            }, {
                'day': 'Wednesday',
                'income': incidentsByDay[2],
                'color': chart.colors.next()
            }, {
                'day': 'Thursday',
                'income': incidentsByDay[3],
                'color': chart.colors.next()
            }, {
                'day': 'Friday',
                'income': incidentsByDay[4],
                'color': chart.colors.next()
            }, {
                'day': 'Saturday',
                'income': incidentsByDay[5],
                'color': chart.colors.next()
            }, {
                'day': 'Sunday',
                'income': incidentsByDay[6],
                'color': chart.colors.next()
            }];
            const categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = 'day';
            // categoryAxis.numberFormatter.numberFormat = '#';
            categoryAxis.renderer.inversed = true;

            const valueAxis = chart.xAxes.push(new am4charts.ValueAxis());

// Create series
            const series = chart.series.push(new am4charts.ColumnSeries3D());
            series.dataFields.valueX = 'income';
            series.dataFields.categoryY = 'day';
            series.name = 'Income';
            series.columns.template.propertyFields.fill = 'color';
            series.columns.template.tooltipText = '{valueX}';
            series.columns.template.column3D.stroke = am4core.color('#fff');
            series.columns.template.column3D.strokeOpacity = 0.2;
            this.chart = chart;
        });

        //  chart for incidents by zip code
        const incidentsByZip = {};
        this.incidents.forEach(incident => {
            if (incidentsByZip[incident.zip_code]) {
                incidentsByZip[incident.zip_code]++;
            } else {
                incidentsByZip[incident.zip_code] = 1;
            }
        });
        this.zone.runOutsideAngular(() => {
            const chart = am4core.create('incidents-chart-by-zip', am4charts.XYChart3D);
            for (const incidentsByZipKey in incidentsByZip) {
                chart.data.push({
                    'zip_code': incidentsByZipKey,
                    'income': incidentsByZip[incidentsByZipKey],
                    'color': chart.colors.next()
                });
            }
            const categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = 'zip_code';
            // categoryAxis.numberFormatter.numberFormat = '#';
            categoryAxis.renderer.inversed = true;

            const valueAxis = chart.xAxes.push(new am4charts.ValueAxis());

// Create series
            const series = chart.series.push(new am4charts.ColumnSeries3D());
            series.dataFields.valueX = 'income';
            series.dataFields.categoryY = 'zip_code';
            series.name = 'Income';
            series.columns.template.propertyFields.fill = 'color';
            series.columns.template.tooltipText = '{valueX}';
            series.columns.template.column3D.stroke = am4core.color('#fff');
            series.columns.template.column3D.strokeOpacity = 0.2;
            this.chart = chart;
        });
    }

    ngOnDestroy() {
        this.zone.runOutsideAngular(() => {
            if (this.chart) {
                this.chart.dispose();
            }
        });
    }
}

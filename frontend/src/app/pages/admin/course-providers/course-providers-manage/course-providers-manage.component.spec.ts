import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseProvidersManageComponent } from './course-providers-manage.component';

describe('CourseProvidersManageComponent', () => {
  let component: CourseProvidersManageComponent;
  let fixture: ComponentFixture<CourseProvidersManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseProvidersManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseProvidersManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

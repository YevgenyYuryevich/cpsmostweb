import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {CourseProviderEditorComponent} from '../../../../app-components/course-provider-editor/course-provider-editor.component';

@Component({
  selector: 'app-course-providers-manage',
  templateUrl: './course-providers-manage.component.html',
  styleUrls: ['./course-providers-manage.component.scss']
})
export class CourseProvidersManageComponent implements OnInit {
    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('imgCellTemplate') imgCellTemplate: TemplateRef<any>;
    @ViewChild('featuredCellTemplate') featuredCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('courseProviderEditor') courseProviderEditor: CourseProviderEditorComponent;
    courseProviders: any[];
    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
        this.courseProviders = this.route.snapshot.data['course_providers'].map(p => {
            p.featured = parseInt(p.featured);
            return p;
        });
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Image',
                cellTemplate: this.imgCellTemplate,
                sortable: false,
                width: 50,
            },
            {
                name: 'Name',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Phone',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Address',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Contact Person',
                prop: 'contact_person',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Contact Email',
                prop: 'contact_email',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Featured',
                prop: 'featured',
                cellTemplate: this.featuredCellTemplate,
                width: 100,
            },
            {
                name: 'Origin ID',
                prop: 'origin_id',
                cellTemplate: this.txtCellTemplate,
                width: 150,
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 100,
                sortable: false,
            },
        ];
    }
    editAction(row, index) {
        this.courseProviderEditor.setMode('update');
        this.courseProviderEditor.setCourseProvider(row);
        this.courseProviderEditor.open();
    }
    deleteAction(row, index) {
        if (confirm('Are you sure to delete?')) {
            this.ajaxApi.apiPost('course-providers/delete', {where: {id: row.id}}).then(res => {
                this.courseProviders.splice(index, 1);
                this.courseProviders = [...this.courseProviders];
            });
        }
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
    onUpdate(row) {
        this.courseProviders = [...this.courseProviders];
    }
    onCreate(row) {
        this.courseProviders.push(row);
        this.courseProviders = [...this.courseProviders];
    }
    updateRow(row, sets) {
        const data = new FormData();
        data.append('where', JSON.stringify({id: row.id}));
        data.append('sets', JSON.stringify(sets));
        this.ajaxApi.apiMultiPartPost('course-providers/update', data).then((res) => {
            Object.assign(row, sets);
        });
    }
}

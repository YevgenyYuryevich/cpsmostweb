import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseProvidersRoutingModule } from './course-providers-routing.module';
import { CourseProvidersManageComponent } from './course-providers-manage/course-providers-manage.component';
import {FormsModule} from '@angular/forms';
import {AppComponentsModule} from '../../../app-components/app-components.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';
import {UiSwitchModule} from 'ngx-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        CourseProvidersRoutingModule,
        FormsModule,
        AppComponentsModule,
        PipesModule,
        NgxDatatableModule,
        UiSwitchModule,
    ],
    declarations: [CourseProvidersManageComponent]
})
export class CourseProvidersModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CourseProvidersManageComponent} from './course-providers-manage/course-providers-manage.component';
import {CourseProviderResolveService} from '../../../resolves/course-provider-resolve.service';

const routes: Routes = [
    {
        path: 'manage',
        component: CourseProvidersManageComponent,
        resolve: {
            course_providers: CourseProviderResolveService,
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseProvidersRoutingModule { }

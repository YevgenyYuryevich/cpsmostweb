import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ManageQualityAssuranceComponent} from './manage-quality-assurance/manage-quality-assurance.component';
import {QualityAssuranceResolveService} from '../../../resolves/quality-assurance-resolve.service';

const routes: Routes = [
    {
        path: '',
        component: ManageQualityAssuranceComponent,
        resolve: {
            quality_assurance: QualityAssuranceResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QualityAssuranceRoutingModule { }

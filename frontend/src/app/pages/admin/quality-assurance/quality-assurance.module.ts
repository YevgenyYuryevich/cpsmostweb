import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QualityAssuranceRoutingModule } from './quality-assurance-routing.module';
import { ManageQualityAssuranceComponent } from './manage-quality-assurance/manage-quality-assurance.component';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';
import {AppComponentsModule} from '../../../app-components/app-components.module';
import {UiSwitchModule} from 'ngx-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgxDatatableModule,
        PipesModule,
        AppComponentsModule,
        QualityAssuranceRoutingModule,
        UiSwitchModule
    ],
    declarations: [ManageQualityAssuranceComponent]
})
export class QualityAssuranceModule { }

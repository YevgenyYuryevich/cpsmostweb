import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ConfirmaDialogService} from '../../../../app-components/confirm-dialog/confirm-dialog.service';
import {AuthService} from '../../../../services/auth.service';

@Component({
    selector: 'app-manage-quality-assurance',
    templateUrl: './manage-quality-assurance.component.html',
    styleUrls: ['./manage-quality-assurance.component.scss']
})
export class ManageQualityAssuranceComponent implements OnInit {

    @ViewChild('fileCellTemplate') fileCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('isPublicCellTemplate') isPublicCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    rows: any[] = [];
    columns: any[];
    table_page_size: Number = 10;
    search_q = '';
    isAdmin = false;

    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService, private confirmDialogService: ConfirmaDialogService, public authService: AuthService) {
        this.rows = route.snapshot.data['quality_assurance'].map(row => {
            row.is_public = parseInt(row.is_public, 10);
            return row;
        });
        this.isAdmin = this.authService.auth.role.toLowerCase() === 'admin';
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'File',
                prop: 'media_url',
                cellTemplate: this.fileCellTemplate,
                sortable: false,
                width: 50,
            },
            {
                name: 'User',
                width: 300,
                prop: 'user_name'
            },
        ];
        if (this.authService.auth.role.toLowerCase() === 'admin') {
            this.columns.push({
                name: 'Public/Private',
                prop: 'is_public',
                cellTemplate: this.isPublicCellTemplate,
                width: 80,
                sortable: false,
            });
        }
        this.columns.push({
            name: 'Actions',
            cellTemplate: this.actionCellTemplate,
            width: 80,
            sortable: false,
        });
    }

    refreshTable() {
        this.table.offset = 0;
    }

    public confirmationDelete(row) {
        // tslint:disable-next-line:triple-equals
        if (!this.isAdmin && row.user_id != this.authService.auth.id) {
            return false;
        }
        this.confirmDialogService.confirm('Are you sure', 'Do you really want to delete this?', 'Delete', 'Cancel')
            .then((confirmed) => {
                if (confirmed) {
                    this.deleteRow(row);
                }
            })
            .catch(() => {
            });
    }

    deleteRow(row) {
        this.ajaxApi.apiPost('quality-assurance/delete', {where: {id: row.id}}).then(res => {
            this.rows = this.rows.filter(r => r.id !== row.id);
        });
    }

    uploadFile(e) {
        if (e.target.files.length) {
            const formData = new FormData();
            formData.append('media', e.target.files[0]);
            this.ajaxApi.apiMultiPartPost('media/upload', formData, true, 100).then(res => {
                this.createQualityAssurance({media_id: res.id});
            });
        }
    }

    createQualityAssurance(sets) {
        return this.ajaxApi.apiPost('quality-assurance/insert', {sets: sets}).then(res => {
            this.rows.push(res);
            this.rows = [...this.rows];
        });
    }

    updateRow(row, sets) {
        return this.ajaxApi.apiPost('quality-assurance/update', {where: {id: row.id}, sets: sets}).then(res => {
            console.log(res);
        });
    }
}

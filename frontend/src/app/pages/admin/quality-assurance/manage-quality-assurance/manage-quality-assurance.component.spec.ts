import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageQualityAssuranceComponent } from './manage-quality-assurance.component';

describe('ManageQualityAssuranceComponent', () => {
  let component: ManageQualityAssuranceComponent;
  let fixture: ComponentFixture<ManageQualityAssuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageQualityAssuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageQualityAssuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {AjaxApiService} from '../../../services/ajax-api.service';
import {AppCommonService} from '../../../services/app-common.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    user: any;
    password = '';
    levelName = '';
    constructor(private authService: AuthService, private ajaxApi: AjaxApiService, public appCommonService: AppCommonService) {
        this.user = authService.auth;
        this.levelName = appCommonService.userLevels.find((le) => {
            return this.user.level == le.id;
        }).name;
    }

    ngOnInit() {
    }
    save() {
        const sets: any = {
            name: this.user.name,
            email: this.user.email,
            level: this.user.level,
            company: this.user.company,
            address: this.user.address,
            phone: this.user.phone,
        }
        if (this.password) {
            sets.password = this.password;
        }
        this.ajaxApi.apiPost('users/update', {where: this.user.id, sets: sets}).then((res) => {
            console.log(res);
        });
    }

}

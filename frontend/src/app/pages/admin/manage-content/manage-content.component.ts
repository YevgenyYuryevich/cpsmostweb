import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../../services/ajax-api.service';
import {PostEditorComponent} from '../../../app-components/post-editor/post-editor.component';

@Component({
    selector: 'app-manage-content',
    templateUrl: './manage-content.component.html',
    styleUrls: ['./manage-content.component.scss']
})
export class ManageContentComponent implements OnInit {
    @ViewChild(PostEditorComponent) postEditorComponent: PostEditorComponent;
    posts: any[] = [];
    tab = 'list';
    options: any = {
        onUpdate: (event: any) => {
            this.sortUpdate(event);
        }
    };
    constructor(private route: ActivatedRoute, public ajaxApi: AjaxApiService) {
        this.posts = this.route.snapshot.data['posts'].map((post) => {
            post.featured = parseInt(post.featured);
            post.permanent_resource = parseInt(post.permanent_resource);
            return post;
        });
    }
    sortUpdate(event) {
        const updates = [];
        this.posts.forEach((post, i) => {
            if (parseInt(post.sort) !== i + 1) {
                updates.push({
                    where: {id: post.id},
                    sets: {sort: i + 1}
                });
                post.sort = i + 1;
            }
        });
        this.ajaxApi.apiPost('posts/update_many', {updates: updates}).then(() => {});
    }
    ngOnInit() {
    }
    afterDelete(index) {
        this.posts.splice(index, 1);
    }
    openUpdate(post) {
        this.postEditorComponent.setPost(post);
        this.postEditorComponent.open();
    }
}

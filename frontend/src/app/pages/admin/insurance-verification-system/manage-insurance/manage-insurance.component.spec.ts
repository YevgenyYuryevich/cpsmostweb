import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageInsuranceComponent } from './manage-insurance.component';

describe('ManageInsuranceComponent', () => {
  let component: ManageInsuranceComponent;
  let fixture: ComponentFixture<ManageInsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageInsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HelperService} from '../../../../services/helper.service';
import {AuthService} from '../../../../services/auth.service';

@Component({
    selector: 'app-manage-insurance',
    templateUrl: './manage-insurance.component.html',
    styleUrls: ['./manage-insurance.component.scss']
})
export class ManageInsuranceComponent implements OnInit {

    @ViewChild('mediaCellTemplate') mediaCellTemplate: TemplateRef<any>;
    @ViewChild('userCellTemplate') userCellTemplate: TemplateRef<any>;
    @ViewChild('stateCellTemplate') stateCellTemplate: TemplateRef<any>;
    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    rows: any[] = [];
    users: any[];
    insuranceOptions: any;
    constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer, private modalService: NgbModal, public helperService: HelperService, public authService: AuthService) {
        this.users = this.route.snapshot.data['users'];
        this.insuranceOptions = this.route.snapshot.data['insurance_options'];
        this.rows = this.route.snapshot.data['insurances'].map(insurance => {
            const user = this.users.find(u => u.id == insurance.user_id);
            insurance.user = user;
            insurance.userName = user ? user.name : 'unassigned';
            insurance.sourceType = helperService.getSourceTypeFromUrl(insurance.source);
            insurance.source = this.sanitizer.bypassSecurityTrustResourceUrl(insurance.source);
            return insurance;
        });
        console.log(this.authService.auth);
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Submission',
                prop: 'source',
                cellTemplate: this.mediaCellTemplate,
                width: 10,
            },
            {
                name: this.authService.auth.role === 'admin' ? 'Vendor/Instructor' : this.helperService.capitalize(this.authService.auth.role),
                prop: 'user',
                cellTemplate: this.userCellTemplate,
                width: 200,
            },
            {
                name: 'Submission Date',
                prop: 'submission_date',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'State',
                cellTemplate: this.stateCellTemplate,
                width: 100,
                sortable: false,
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 50,
                sortable: false,
            },
        ];
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
    approve(event) {
    }
    deny(id) {
    }
}

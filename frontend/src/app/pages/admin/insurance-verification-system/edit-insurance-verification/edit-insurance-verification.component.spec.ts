import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInsuranceVerificationComponent } from './edit-insurance-verification.component';

describe('EditInsuranceVerificationComponent', () => {
  let component: EditInsuranceVerificationComponent;
  let fixture: ComponentFixture<EditInsuranceVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInsuranceVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInsuranceVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../services/auth.service';
import {HelperService} from '../../../../services/helper.service';

@Component({
    selector: 'app-edit-insurance-verification',
    templateUrl: './edit-insurance-verification.component.html',
    styleUrls: ['./edit-insurance-verification.component.scss']
})
export class EditInsuranceVerificationComponent implements OnInit {

    insurance: any;
    media: File;
    mediaType: string;
    users: any[];
    sets: any = {};
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService, private router: Router, public authService: AuthService, public helperService: HelperService) {
        if (route.snapshot.data['insurance']) {
            this.insurance = this.route.snapshot.data['insurance'];
            this.sets.user_id = this.insurance.user_id;
            this.sets.source = this.insurance.source;
            this.mediaType = this.helperService.getSourceTypeFromUrl(this.insurance.source);
        }
        if (this.authService.auth.role.toLowerCase() !== 'admin') {
            this.sets.user_id = this.authService.auth.id;
        } else {
            this.users = this.route.snapshot.data['users'];
        }
    }

    ngOnInit() {
    }
    insert() {
        const formData = new FormData();
        formData.append('source', this.media);
        formData.append('sets', JSON.stringify(this.sets));
        this.ajaxApi.apiMultiPartPost('insurances/insert', formData).then(res => {
            return this.router.navigateByUrl('/admin/insurance/manage');
        });
    }
    update() {
        const formData = new FormData();
        formData.append('where', JSON.stringify({id: this.insurance.id}));
        if (this.media) {
            formData.append('source', this.media);
        }
        formData.append('sets', JSON.stringify(this.sets));
        this.ajaxApi.apiMultiPartPost('insurances/update', formData).then(res => {
            return this.router.navigateByUrl('/admin/insurance/manage');
        });
    }
    save() {
        if (this.insurance) {
            this.update();
        } else {
            this.insert();
        }
    }
    setMedia(event) {
        if (event.target.files && event.target.files[0]) {
            this.media = event.target.files[0];
            this.sets.source = URL.createObjectURL(this.media);
            this.sets.source = this.helperService.trustUrl(this.sets.source);
            this.mediaType = this.helperService.getSourceTypeFromUrl(this.media.name);
        } else {
            return false;
        }
    }
}

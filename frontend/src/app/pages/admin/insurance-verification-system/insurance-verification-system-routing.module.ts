import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EditInsuranceVerificationComponent} from './edit-insurance-verification/edit-insurance-verification.component';
import {ManageInsuranceComponent} from './manage-insurance/manage-insurance.component';
import {InsurancesResolveService} from '../../../resolves/insurances-resolve.service';
import {InsuranceResolveService} from '../../../resolves/insurance-resolve.service';
import {InsuranceOptionsResolveService} from '../../../resolves/insurance-options-resolve.service';
import {UsersResolveService} from '../users/services/users-resolve.service';

const routes: Routes = [
    {
        path: 'create',
        component: EditInsuranceVerificationComponent,
        resolve: {
            users: UsersResolveService
        }
    },
    {
        path: 'edit/:insurance',
        component: EditInsuranceVerificationComponent,
        resolve: {
            insurance: InsuranceResolveService,
            users: UsersResolveService
        }
    },
    {
        path: 'manage',
        component: ManageInsuranceComponent,
        resolve: {
            insurances: InsurancesResolveService,
            insurance_options: InsuranceOptionsResolveService,
            users: UsersResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InsuranceVerificationSystemRoutingModule { }

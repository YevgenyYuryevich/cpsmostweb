import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InsuranceVerificationSystemRoutingModule } from './insurance-verification-system-routing.module';
import { EditInsuranceVerificationComponent } from './edit-insurance-verification/edit-insurance-verification.component';
import {FormsModule} from '@angular/forms';
import { ManageInsuranceComponent } from './manage-insurance/manage-insurance.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';
import {NgbButtonsModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponentsModule} from '../../../app-components/app-components.module';

@NgModule({
    imports: [
        CommonModule,
        InsuranceVerificationSystemRoutingModule,
        FormsModule,
        NgxDatatableModule,
        PipesModule,
        NgbButtonsModule,
        NgbModalModule,
        AppComponentsModule
    ],
    declarations: [EditInsuranceVerificationComponent, ManageInsuranceComponent]
})
export class InsuranceVerificationSystemModule { }

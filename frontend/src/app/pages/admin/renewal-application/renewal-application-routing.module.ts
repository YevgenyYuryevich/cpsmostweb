import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EditRenewalApplicationComponent} from './edit-renewal-application/edit-renewal-application.component';
import {ManageRenewalApplicationComponent} from './manage-renewal-application/manage-renewal-application.component';
import {RenewalsResolveService} from '../../../resolves/renewals-resolve.service';
import {RenewalResolveService} from '../../../resolves/renewal-resolve.service';
import {RenewalOptionsResolveService} from '../../../resolves/renewal-options-resolve.service';
import {UsersResolveService} from '../users/services/users-resolve.service';

const routes: Routes = [
    {
        path: 'create',
        component: EditRenewalApplicationComponent,
        resolve: {
            users: UsersResolveService
        }
    },
    {
        path: 'edit/:id',
        component: EditRenewalApplicationComponent,
        resolve: {
            users: UsersResolveService,
            renewal: RenewalResolveService
        }
    },
    {
        path: 'manage',
        component: ManageRenewalApplicationComponent,
        resolve: {
            renewals: RenewalsResolveService,
            users: UsersResolveService,
            renewal_options: RenewalOptionsResolveService
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RenewalApplicationRoutingModule { }

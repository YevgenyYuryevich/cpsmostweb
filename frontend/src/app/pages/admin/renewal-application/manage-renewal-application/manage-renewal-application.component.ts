import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../../services/auth.service';
import {HelperService} from '../../../../services/helper.service';

@Component({
    selector: 'app-manage-renewal-application',
    templateUrl: './manage-renewal-application.component.html',
    styleUrls: ['./manage-renewal-application.component.scss']
})
export class ManageRenewalApplicationComponent implements OnInit {

    @ViewChild('mediaCellTemplate') mediaCellTemplate: TemplateRef<any>;
    @ViewChild('userCellTemplate') userCellTemplate: TemplateRef<any>;
    @ViewChild('stateCellTemplate') stateCellTemplate: TemplateRef<any>;
    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    rows: any[] = [];
    users: any[];
    renewalApplicationOptions: any;

    constructor(private route: ActivatedRoute, public authService: AuthService, public appHelper: HelperService) {
        this.users = this.route.snapshot.data['users'];
        this.renewalApplicationOptions = this.route.snapshot.data['renewal_options'];
        this.rows = this.route.snapshot.data['renewals'].map(renewal => {
            const user = this.users.find(u => u.id == renewal.user_id);
            renewal.user = user;
            renewal.userName = user ? user.name : 'unassigned';
            return renewal;
        });
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Application',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: 'Driving Record',
                prop: 'driving_record',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: 'PDW',
                prop: 'pdw',
                cellTemplate: this.mediaCellTemplate,
                width: 50,
            },
            {
                name: this.authService.auth.role === 'admin' ? 'Vendor/Instructor' : this.appHelper.capitalize(this.authService.auth.role),
                prop: 'user',
                cellTemplate: this.userCellTemplate,
                width: 200,
            },
            {
                name: 'State',
                cellTemplate: this.stateCellTemplate,
                width: 100,
                sortable: false,
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 50,
                sortable: false,
            },
        ];
    }

    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
}

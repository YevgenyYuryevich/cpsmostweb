import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRenewalApplicationComponent } from './manage-renewal-application.component';

describe('ManageRenewalApplicationComponent', () => {
  let component: ManageRenewalApplicationComponent;
  let fixture: ComponentFixture<ManageRenewalApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRenewalApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRenewalApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

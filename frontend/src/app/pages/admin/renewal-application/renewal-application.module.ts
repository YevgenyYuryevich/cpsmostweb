import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RenewalApplicationRoutingModule } from './renewal-application-routing.module';
import { EditRenewalApplicationComponent } from './edit-renewal-application/edit-renewal-application.component';
import { ManageRenewalApplicationComponent } from './manage-renewal-application/manage-renewal-application.component';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';
import {AppComponentsModule} from '../../../app-components/app-components.module';

@NgModule({
    imports: [
        CommonModule,
        RenewalApplicationRoutingModule,
        FormsModule,
        NgxDatatableModule,
        PipesModule,
        AppComponentsModule
    ],
    declarations: [EditRenewalApplicationComponent, ManageRenewalApplicationComponent]
})
export class RenewalApplicationModule { }

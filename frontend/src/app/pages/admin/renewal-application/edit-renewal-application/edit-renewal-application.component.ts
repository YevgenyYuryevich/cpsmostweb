import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../../services/auth.service';

@Component({
    selector: 'app-edit-renewal-application',
    templateUrl: './edit-renewal-application.component.html',
    styleUrls: ['./edit-renewal-application.component.scss']
})
export class EditRenewalApplicationComponent implements OnInit {

    sets: any = {};
    applicationFile: File;
    drivingRecordFile: File;
    pdwFile: File;
    renewalApplication: any;
    users: any[] = [];
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute, private router: Router, public authService: AuthService) {
        this.users = this.route.snapshot.data['users'];
        if (this.route.snapshot.data['renewal']) {
            this.renewalApplication = this.route.snapshot.data['renewal'];
            Object.assign(this.sets, this.renewalApplication);
            delete this.sets.id;
        }
    }

    ngOnInit() {
    }
    save() {
        if (this.renewalApplication) {
            this.update();
        } else {
            this.insert();
        }
    }
    update() {
        const formData = new FormData();
        formData.append('where', JSON.stringify({id: this.renewalApplication.id}));
        formData.append('sets', JSON.stringify(this.sets));
        if (this.applicationFile) {
            formData.append('application', this.applicationFile);
        }
        if (this.drivingRecordFile) {
            formData.append('driving_record', this.drivingRecordFile);
        }
        if (this.pdwFile) {
            formData.append('pdw', this.pdwFile);
        }
        return this.ajaxApi.apiMultiPartPost('renewal-application/update', formData).then(res => {
            this.router.navigateByUrl('/admin/re-app/manage');
        });
    }
    insert() {
        const formData = new FormData();
        formData.append('sets', JSON.stringify(this.sets));
        if (this.applicationFile) {
            formData.append('application', this.applicationFile);
        }
        if (this.drivingRecordFile) {
            formData.append('driving_record', this.drivingRecordFile);
        }
        if (this.pdwFile) {
            formData.append('pdw', this.pdwFile);
        }
        return this.ajaxApi.apiMultiPartPost('renewal-application/insert', formData).then(res => {
            this.router.navigateByUrl('/admin/re-app/manage');
        });
    }
    setApplication(event) {
        if (event.target.files && event.target.files[0]) {
            this.applicationFile = event.target.files[0];
        } else {
            return false;
        }
    }
    setDrivingRecord(event) {
        if (event.target.files && event.target.files[0]) {
            this.drivingRecordFile = event.target.files[0];
        } else {
            return false;
        }
    }
    setPdw(event) {
        if (event.target.files && event.target.files[0]) {
            this.pdwFile = event.target.files[0];
        } else {
            return false;
        }
    }
}

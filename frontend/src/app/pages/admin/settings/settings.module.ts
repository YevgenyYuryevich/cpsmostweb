import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SocialComponent } from './social/social.component';
import {FormsModule} from '@angular/forms';
import { EventbriteComponent } from './eventbrite/eventbrite.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PipesModule} from '../../../pipes/pipes.module';
import {UiSwitchModule} from 'ngx-ui-switch';
import { InstagramComponent } from './instagram/instagram.component';
import { TwitterComponent } from './twitter/twitter.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { FacebookComponent } from './facebook/facebook.component';
import { InsuranceOptionsComponent } from './insurance-options/insurance-options.component';
import { NgxEditorModule } from '../../../app-components/ngx-editor/src/app/ngx-editor/ngx-editor.module';
import { RenewalOptionsComponent } from './renewal-options/renewal-options.component';
import { AnalyticsCodeComponent } from './analytics-code/analytics-code.component';
import {MonacoEditorModule} from 'ngx-monaco-editor';

@NgModule({
    imports: [
        CommonModule,
        SettingsRoutingModule,
        FormsModule,
        NgxDatatableModule,
        PipesModule,
        UiSwitchModule,
        AngularFontAwesomeModule,
        NgxEditorModule,
        MonacoEditorModule.forRoot(),
    ],
    declarations: [SocialComponent, EventbriteComponent, InstagramComponent, TwitterComponent, FacebookComponent, InsuranceOptionsComponent, RenewalOptionsComponent, AnalyticsCodeComponent]
})
export class SettingsModule { }

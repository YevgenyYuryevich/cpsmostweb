import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SocialComponent} from './social/social.component';
import {SocialResolveService} from '../../../resolves/social-resolve.service';
import {EventbriteComponent} from './eventbrite/eventbrite.component';
import {EventbriteOptionsResolveService} from '../../../resolves/eventbrite-options-resolve.service';
import {InstagramComponent} from './instagram/instagram.component';
import {InstagramTokenResolveService} from '../../../resolves/instagram-token-resolve.service';
import {TwitterComponent} from './twitter/twitter.component';
import {TwitterOptionsResolveService} from '../../../resolves/twitter-options-resolve.service';
import {InstagramOptionsResolveService} from '../../../resolves/instagram-options-resolve.service';
import {FacebookComponent} from './facebook/facebook.component';
import {FacebookRedirectResolveService} from '../../../resolves/facebook-redirect-resolve.service';
import {FacebookTokenResolveService} from '../../../resolves/facebook-token-resolve.service';
import {InsuranceOptionsComponent} from './insurance-options/insurance-options.component';
import {InsuranceOptionsResolveService} from '../../../resolves/insurance-options-resolve.service';
import {RenewalOptionsComponent} from './renewal-options/renewal-options.component';
import {RenewalOptionsResolveService} from '../../../resolves/renewal-options-resolve.service';
import {AnalyticsCodeComponent} from './analytics-code/analytics-code.component';
import {AnalyticsCodeResolveService} from '../../../resolves/analytics-code-resolve.service';
import {FacebookOptionsResolveService} from '../../../resolves/facebook-options-resolve.service';

const routes: Routes = [
    {
        path: 'social',
        component: SocialComponent,
        resolve: {
            social: SocialResolveService
        }
    },
    {
        path: 'eventbrite',
        component: EventbriteComponent,
        resolve: {
            options: EventbriteOptionsResolveService
        }
    },
    {
        path: 'instagram',
        component: InstagramComponent,
        resolve: {
            options: InstagramOptionsResolveService,
            token: InstagramTokenResolveService
        }
    },
    {
        path: 'twitter',
        component: TwitterComponent,
        resolve: {
            twitter_options: TwitterOptionsResolveService
        }
    },
    {
        path: 'facebook',
        component: FacebookComponent,
        resolve: {
            login_url: FacebookRedirectResolveService,
            feeds: FacebookTokenResolveService,
            options: FacebookOptionsResolveService
        }
    },
    {
        path: 'insurance',
        component: InsuranceOptionsComponent,
        resolve: {
            options: InsuranceOptionsResolveService
        }
    },
    {
        path: 'renewal',
        component: RenewalOptionsComponent,
        resolve: {
            options: RenewalOptionsResolveService
        }
    },
    {
        path: 'analytics',
        component: AnalyticsCodeComponent,
        resolve: {
            options: AnalyticsCodeResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }

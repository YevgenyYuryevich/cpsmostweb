import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticsCodeComponent } from './analytics-code.component';

describe('AnalyticsCodeComponent', () => {
  let component: AnalyticsCodeComponent;
  let fixture: ComponentFixture<AnalyticsCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyticsCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticsCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

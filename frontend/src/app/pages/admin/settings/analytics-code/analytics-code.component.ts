import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute} from '@angular/router';
import {AppCommonService} from '../../../../services/app-common.service';

@Component({
    selector: 'app-analytics-code',
    templateUrl: './analytics-code.component.html',
    styleUrls: ['./analytics-code.component.scss']
})
export class AnalyticsCodeComponent implements OnInit {

    options: any;
    editorOptions = {theme: 'vs-dark', language: 'html'};
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute, private appCommonService: AppCommonService) {
        this.options = this.route.snapshot.data['options'];
        this.options.analytics_code = this.options.analytics_code.replace(/safe-script/g, 'script');
    }

    ngOnInit() {
    }
    save() {
        const updates = [];
        for (const k in this.options) {
            updates.push({
                where: {
                    name: k
                },
                sets: {
                    value: this.options.analytics_code.replace(/script/g, 'safe-script')
                }
            });
        }
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates}).then((res) => {
            this.appCommonService.analyticsCode = this.options.analytics_code;
        });
    }
}

import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute} from '@angular/router';
import {HelperService} from '../../../../services/helper.service';

@Component({
    selector: 'app-social',
    templateUrl: './social.component.html',
    styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {

    social: any = {};
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute, private helper: HelperService) {
        this.social = route.snapshot.data['social'];
    }

    ngOnInit() {
    }
    save() {
        const updates = [];
        for (const k in this.social) {
            updates.push({
                where: {
                    name: this.helper.utf8ToBase64(k)
                },
                sets: {
                    value: this.helper.utf8ToBase64(this.social[k])
                },
                encoded: 1
            });
        }
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates}).then((res) => {
            console.log(res);
        });
    }
}

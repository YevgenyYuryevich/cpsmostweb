import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewalOptionsComponent } from './renewal-options.component';

describe('RenewalOptionsComponent', () => {
  let component: RenewalOptionsComponent;
  let fixture: ComponentFixture<RenewalOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewalOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewalOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-renewal-options',
    templateUrl: './renewal-options.component.html',
    styleUrls: ['./renewal-options.component.scss']
})
export class RenewalOptionsComponent implements OnInit {

    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    options: any = {};
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute) {
        this.options = this.route.snapshot.data['options'];
    }

    ngOnInit() {
    }
    save() {
        const updates = [];
        for (const k in this.options) {
            updates.push({
                where: {
                    name: k
                },
                sets: {
                    value: this.options[k]
                }
            });
        }
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates}).then((res) => {
            console.log(res);
        });
    }

}

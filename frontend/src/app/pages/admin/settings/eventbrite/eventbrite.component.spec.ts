import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventbriteComponent } from './eventbrite.component';

describe('EventbriteComponent', () => {
  let component: EventbriteComponent;
  let fixture: ComponentFixture<EventbriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventbriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventbriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {DatatableComponent, TableColumn} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-eventbrite',
    templateUrl: './eventbrite.component.html',
    styleUrls: ['./eventbrite.component.scss']
})
export class EventbriteComponent implements OnInit {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('imgCellTemplate') imgCellTemplate: TemplateRef<any>;
    @ViewChild('flgCellTemplate') flgCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    personalToken;
    organizationId;
    organizations: any[] = [];
    events: any[] = [];
    table_page_size: any = 15;
    search_q = '';
    columns: TableColumn[];
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute) {
        const options = route.snapshot.data['options'];
        this.personalToken = options.eventbrite_personal_token;
        this.organizationId = options.eventbrite_organization;
        this.organizations = JSON.parse(options.eventbrite_organizations);
        this.events = JSON.parse(options.eventbrite_events);
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Title',
                cellTemplate: this.txtCellTemplate,
            },
            {
                name: 'Start Time',
                prop: 'start_time',
                cellTemplate: this.txtCellTemplate,
            },
            {
                name: 'Flag',
                prop: 'featured',
                cellTemplate: this.flgCellTemplate,
            }
        ];
    }
    tokenSaveApply() {
        const updates = [
            {
                where: {name: 'eventbrite_personal_token'},
                sets: {value: this.personalToken}
            }
        ];
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
        this.ajaxApi.apiPost('eventbrite/pull-organizations', {token: this.personalToken}).then((res) => {
            this.organizations = res;
            if (res.length) {
                this.organizationId = res[0].id;
                this.organizationSaveApply();
            }
        });
    }
    organizationSaveApply() {
        let updates = [
            {
                where: {name: 'eventbrite_organization'},
                sets: {value: this.organizationId}
            }
        ];
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
        updates = [
            {
                where: {name: 'eventbrite_organizations'},
                sets: {value: JSON.stringify(this.organizations)}
            }
        ];
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
        this.ajaxApi.apiPost('eventbrite/pull-events', {organization: this.organizationId}).then((res) => {
            this.events = res.map((event) => {
                return this.formatEvent(event);
            });
        });
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
    formatEvent(event) {
        event.title = event.name.text;
        event.start_time = event.start.utc;
        event.featured = event.featured ? parseInt(event.featured) : 0;
        return event;
    }
    updateEvent(event, sets) {
        Object.assign(event, sets);
        const updates = [
            {where: {name: 'eventbrite_events'}, sets: {value: JSON.stringify(this.events)}}
        ];
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
    }
}

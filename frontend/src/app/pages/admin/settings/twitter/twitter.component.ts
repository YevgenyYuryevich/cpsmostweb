import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-twitter',
    templateUrl: './twitter.component.html',
    styleUrls: ['./twitter.component.scss']
})
export class TwitterComponent implements OnInit {

    screenName;
    tweets: any[];
    selectedID;
    constructor(private ajaxApi: AjaxApiService, private route: ActivatedRoute) {
        this.screenName = route.snapshot.data['twitter_options'].twitter_screen_name ? route.snapshot.data['twitter_options'].twitter_screen_name : '';
    }

    ngOnInit() {
    }
    pullRecentTweets() {
        const updates = [
            {
                where: {name: 'twitter_screen_name'},
                sets: {value: this.screenName}
            }
        ];
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
        this.ajaxApi.apiPost('twitter/pull-recent-tweets', {screen_name: this.screenName}).then((res) => {
            this.tweets = res.map((tweet: any) => {
                tweet.link = 'https://twitter.com/' + this.screenName + '/status/' + tweet.id_str;
                return tweet;
            });
        });
    }
    setTweets() {
        const tweets = this.tweets.filter(tweet => {
            return tweet.selected;
        });
        const updates = [
            {
                where: {name: 'twitter_tweets'},
                sets: {value: JSON.stringify(tweets)}
            }
        ];
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
    }
}

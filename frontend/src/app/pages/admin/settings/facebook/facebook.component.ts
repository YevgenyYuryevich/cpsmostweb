import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../../../services/ajax-api.service';

@Component({
    selector: 'app-facebook',
    templateUrl: './facebook.component.html',
    styleUrls: ['./facebook.component.scss']
})
export class FacebookComponent implements OnInit {

    redirectUrl;
    feeds: any[];
    appId: any;
    appSecret: any;
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
        this.redirectUrl = this.route.snapshot.data['login_url']['url'];
        localStorage.setItem('FBRLH_sate', this.route.snapshot.data['login_url']['state']);
        this.feeds = this.route.snapshot.data['feeds'];
        if (this.feeds) {
            this.feeds = this.feeds.map(feed => {
                feed.attachment = feed.attachments.data[0];
                return feed;
            });
        }
        const options = route.snapshot.data['options'];
        this.appId = options.facebook_app_id ? options.facebook_app_id : '';
        this.appSecret = options.facebook_app_secret ? options.facebook_app_secret : '';
    }

    ngOnInit() {
    }
    saveOption(name, value) {
        const updates = [
            {
                where: {name: name},
                sets: {value: value}
            }
        ];
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
    }
}

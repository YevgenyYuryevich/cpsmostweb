import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AjaxApiService} from '../../../../services/ajax-api.service';

@Component({
    selector: 'app-instagram',
    templateUrl: './instagram.component.html',
    styleUrls: ['./instagram.component.scss']
})
export class InstagramComponent implements OnInit {

    connectHref: string;
    connectedTokenObj: any;
    recentImages: string[] = [];
    client_id: string;
    client_secrete: string;
    constructor(private route: ActivatedRoute, private ajaxApi: AjaxApiService) {
        const options = route.snapshot.data['options'];
        this.client_id = options.instagram_client_id ? options.instagram_client_id : '';
        this.client_secrete = options.instagram_client_secrete ? options.instagram_client_secrete : '';
        this.connectHref = 'https://api.instagram.com/oauth/authorize/?client_id=' + this.client_id + '&redirect_uri=' + location.origin + '/admin/settings/instagram' + '&response_type=code';

        const tokenObj = route.snapshot.data['token'];
        this.connectedTokenObj = tokenObj;
        if (tokenObj) {
            this.recentImages = tokenObj.media.data.map(media => {
                return {src: media.images.low_resolution.url, link: media.link, selected: false};
            });
        }
    }

    ngOnInit() {
    }
    saveSecrete() {
        this.connectHref = 'https://api.instagram.com/oauth/authorize/?client_id=' + this.client_id + '&redirect_uri=' + location.origin + '/admin/settings/instagram' + '&response_type=code';
        const updates = [
            {
                where: {name: 'instagram_client_secrete'},
                sets: {value: this.client_secrete}
            }
        ];
        return this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
    }
    saveClientID() {
        this.connectHref = 'https://api.instagram.com/oauth/authorize/?client_id=' + this.client_id + '&redirect_uri=' + location.origin + '/admin/settings/instagram' + '&response_type=code';
        const updates = [
            {
                where: {name: 'instagram_client_id'},
                sets: {value: this.client_id}
            }
        ];
        return this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
    }
    updatePhotos() {
        const selectedPhotos = this.recentImages.filter((item: any) => {
            return item.selected;
        }).map((img: any) => {
            return {image: img.src, link: img.link};
        });
        const updates = [
            {
                where: {name: 'instagram_photos'},
                sets: {value: JSON.stringify(selectedPhotos)}
            }
        ];
        this.ajaxApi.apiPost('options/update-or-create', {updates: updates});
    }
}

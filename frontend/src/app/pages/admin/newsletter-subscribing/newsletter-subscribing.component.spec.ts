import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsletterSubscribingComponent } from './newsletter-subscribing.component';

describe('NewsletterSubscribingsComponent', () => {
  let component: NewsletterSubscribingComponent;
  let fixture: ComponentFixture<NewsletterSubscribingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsletterSubscribingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsletterSubscribingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

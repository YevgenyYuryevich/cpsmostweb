import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-newsletter-subscribing',
    templateUrl: './newsletter-subscribing.component.html',
    styleUrls: ['./newsletter-subscribing.component.scss']
})
export class NewsletterSubscribingComponent implements OnInit {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild('imgCellTemplate') imgCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    rows: any[];
    constructor(private route: ActivatedRoute) {
        this.rows = this.route.snapshot.data['subscribe_emails'];
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Email',
                cellTemplate: this.txtCellTemplate,
                width: 400,
            },
            {
                name: 'Date Time',
                prop: 'datetime',
                cellTemplate: this.txtCellTemplate,
                width: 800,
            },
        ];
    }
    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
}

import { Component, OnInit } from '@angular/core';
import {AjaxApiService} from '../../../../services/ajax-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {AuthService} from '../../../../services/auth.service';

@Component({
    selector: 'app-edit-incident',
    templateUrl: './edit-incident.component.html',
    styleUrls: ['./edit-incident.component.scss'],
})
export class EditIncidentComponent implements OnInit {

    incident: any;
    vendors: any[];
    editorConfig = {
        editable: true,
        height: '10rem',
        minHeight: '5rem',
        translate: 'no',
    };
    sets: any = {};
    media: any[] = [];
    dateObj: Date;
    timeObj: Date;
    constructor(private ajaxApi: AjaxApiService, private datePipe: DatePipe, private router: Router, private route: ActivatedRoute, public authService: AuthService) {
        this.incident = this.route.snapshot.data['incident'];
        this.vendors = this.route.snapshot.data['vendors'];
        if (this.incident) {
            Object.assign(this.sets, this.incident);
            this.dateObj = new Date(this.incident.date);
            this.timeObj = new Date(this.incident.date + ' ' + this.incident.time);
            delete this.sets.id;
            delete this.sets.media;
            this.media = this.incident.media;
        }
        if (this.authService.auth.role.toLowerCase() === 'vendor') {
            this.sets.vendor = this.authService.auth.id;
        }
    }

    ngOnInit() {
    }
    save() {
        this.sets.date = this.datePipe.transform(this.dateObj, 'yyyy-MM-dd');
        this.sets.time = this.datePipe.transform(this.timeObj, 'h:mm a');
        if (this.incident) {
            this.update();
        } else {
            this.insert();
        }
    }
    insert() {
        this.ajaxApi.apiPost('incidents/insert', {sets: this.sets}, true).then(res => {
            this.incident = res;
            this.setMedia().then(() => {
                this.router.navigateByUrl('/admin/incidents/manage');
            });
        });
    }
    update() {
        this.ajaxApi.apiPost('incidents/update', {where: {id: this.incident.id}, sets: this.sets}).then(res => {
            this.setMedia().then(() => {
                this.router.navigateByUrl('/admin/incidents/manage');
            });
        });
    }
    setMedia() {
        const mediaIds = this.media.map(m => {
            return m.id;
        });
        return this.ajaxApi.apiPost('incidents/' + this.incident.id + '/sync-media', {media: mediaIds});
    }
    addMedia(event) {
        if (event.target.files && event.target.files[0]) {
            const mediaFile = event.target.files[0];
            const formData = new FormData();
            formData.append('media', mediaFile);
            this.ajaxApi.apiMultiPartPost('media/upload', formData).then((res) => {
                this.media.push(res);
            });
        } else {
            return false;
        }
    }
    removeMedia(rm) {
        this.media = this.media.filter((m) => {
           return m.id !== rm.id;
        });
    }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EditIncidentComponent} from './edit-incident/edit-incident.component';
import {ManageIncidentComponent} from './manage-incident/manage-incident.component';
import {IncidentsResolveService} from '../../../resolves/incidents-resolve.service';
import {IncidentResolveService} from '../../../resolves/incident-resolve.service';
import {VendorsResolveService} from '../../../resolves/vendors-resolve.service';

const routes: Routes = [
    {
        path: 'edit/:id',
        component: EditIncidentComponent,
        resolve: {
            incident: IncidentResolveService,
            vendors: VendorsResolveService
        }
    },
    {
        path: 'create',
        component: EditIncidentComponent,
        resolve: {
            vendors: VendorsResolveService
        }
    },
    {
        path: 'manage',
        component: ManageIncidentComponent,
        resolve: {
            incidents: IncidentsResolveService,
            vendors: VendorsResolveService
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncidentsRoutingModule { }

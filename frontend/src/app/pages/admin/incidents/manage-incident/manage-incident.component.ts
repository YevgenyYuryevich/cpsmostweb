import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../../services/auth.service';

@Component({
    selector: 'app-manage-incident',
    templateUrl: './manage-incident.component.html',
    styleUrls: ['./manage-incident.component.scss']
})
export class ManageIncidentComponent implements OnInit {
    @ViewChild('mediaCellTemplate') mediaCellTemplate: TemplateRef<any>;
    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    columns: any[];
    table_page_size: any = 10;
    search_q = '';
    rows: any[] = [];
    vendors: any[];

    constructor(private route: ActivatedRoute, public authService: AuthService) {
        this.vendors = this.route.snapshot.data['vendors'];
        this.rows = this.route.snapshot.data['incidents'].map(incident => {
            // tslint:disable-next-line:no-shadowed-variable triple-equals
            const vendor = this.vendors.find(vendor => vendor.id == incident.vendor);
            incident.vendorName = vendor ? vendor.name : 'unassigned';
            return incident;
        });
        if (this.authService.auth.role !== 'admin') {
            this.rows = this.rows.filter(row => {
                return row.vendor == this.authService.auth.id;
            });
        }
    }

    ngOnInit() {
        this.columns = [
            {
                name: 'Media',
                cellTemplate: this.mediaCellTemplate,
                width: 200,
            },
            {
                name: 'Vendor',
                prop: 'vendorName',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Description',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Address',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'City',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Date',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Time',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 100,
                sortable: false,
            },
        ];
    }

    refreshTable() {
        this.table_page_size = parseInt(this.table_page_size);
        this.table.offset = 0;
    }
}

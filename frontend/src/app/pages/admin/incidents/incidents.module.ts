import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import { IncidentsRoutingModule } from './incidents-routing.module';
import { EditIncidentComponent } from './edit-incident/edit-incident.component';
import { NgxEditorModule } from '../../../app-components/ngx-editor/src/app/ngx-editor/ngx-editor.module';
import { FormsModule } from '@angular/forms';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import { ManageIncidentComponent } from './manage-incident/manage-incident.component';
import {PipesModule} from '../../../pipes/pipes.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IncidentsRoutingModule,
        NgxEditorModule,
        AngularFontAwesomeModule,
        NgxDatatableModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        PipesModule,
    ],
    declarations: [EditIncidentComponent, ManageIncidentComponent],
    providers: [
        DatePipe
    ]
})
export class IncidentsModule { }

import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AppCommonService} from '../../../services/app-common.service';
import {AjaxApiService} from '../../../services/ajax-api.service';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {ConfirmaDialogService} from '../../../app-components/confirm-dialog/confirm-dialog.service';

@Component({
    selector: 'app-user-levels',
    templateUrl: './user-levels.component.html',
    styleUrls: ['./user-levels.component.scss']
})
export class UserLevelsComponent implements OnInit {

    @ViewChild('txtCellTemplate') txtCellTemplate: TemplateRef<any>;
    @ViewChild('canFormLibraryCellTemplate') canFormLibraryCellTemplate: TemplateRef<any>;
    @ViewChild('actionCellTemplate') actionCellTemplate: TemplateRef<any>;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    newRole: any = {
        name: ''
    };
    columns: any[];

    constructor(public appCommonService: AppCommonService, private ajaxApi: AjaxApiService, private confirmDialogService: ConfirmaDialogService) { }

    ngOnInit() {
        this.columns = [
            {
                name: 'Name',
                cellTemplate: this.txtCellTemplate,
                width: 200,
            },
            {
                name: 'Form Library Access',
                prop: 'can_form_library',
                cellTemplate: this.canFormLibraryCellTemplate
            },
            {
                name: 'Actions',
                cellTemplate: this.actionCellTemplate,
                width: 80,
                sortable: false,
            },
        ];
    }
    updateRow(row, sets) {
        this.ajaxApi.apiPost('user-levels/update', {where: row.id, sets: sets}).then(() => {
            Object.assign(row, sets);
        });
    }
    addNewRole() {
        this.create(this.newRole).then(res => {
            const level = res.data;
            level.can_form_library = 0;
            this.appCommonService.userLevels.push(level);
            this.appCommonService.userLevels = [...this.appCommonService.userLevels];
        });
    }
    create(sets) {
        return this.ajaxApi.apiPost('user-levels/insert', {sets: sets});
    }
    confirmDelete(row) {
        this.confirmDialogService.confirm('Are you sure', 'Do you really want to delete this?', 'Delete', 'Cancel')
            .then((confirmed) => {
                if (confirmed) {
                    this.deleteRow(row);
                }
            })
            .catch(() => {});
    }
    deleteRow(row) {
        this.ajaxApi.apiGet('user-levels/delete/' + row.id).then(() => {
            this.appCommonService.userLevels = this.appCommonService.userLevels.filter(l => l.id !== row.id);
        });
    }
}

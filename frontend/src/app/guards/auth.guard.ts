import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const url: string = state.url;
        if (this.authService.isLoggedIn) {
            return true;
        }
        return this.checkLogin( url );
    }

    checkLogin(url: string) {

        return this.authService.authorization().then((res) => {
            if (res) {
                return true;
            } else {
                this.authService.redirectUrl = url;
                this.router.navigate(['/auth/login']);
                return false;
            }
        });
    }
}

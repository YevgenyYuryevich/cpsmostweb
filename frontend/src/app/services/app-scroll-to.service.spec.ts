import { TestBed } from '@angular/core/testing';

import { AppScrollToService } from './app-scroll-to.service';

describe('AppScrollToService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppScrollToService = TestBed.get(AppScrollToService);
    expect(service).toBeTruthy();
  });
});

import { TestBed, inject } from '@angular/core/testing';

import { AjaxApiService } from './ajax-api.service';

describe('AjaxApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AjaxApiService]
    });
  });

  it('should be created', inject([AjaxApiService], (service: AjaxApiService) => {
    expect(service).toBeTruthy();
  }));
});

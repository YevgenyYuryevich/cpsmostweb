import { Injectable } from '@angular/core';
import {AjaxApiService} from './ajax-api.service';

@Injectable({
    providedIn: 'root'
})
export class AppCommonService {

    userLevels: any[];
    permanentResources: any[];
    socialLinks: any;
    analyticsCode: string;
    constructor(private ajaxApi: AjaxApiService) {}
    plusCourseViews() {
        this.ajaxApi.apiPost('courses/plus-views', {}, false);
    }
    plusCourseClicks() {
        this.ajaxApi.apiPost('courses/plus-clicks', {}, false);
    }
}

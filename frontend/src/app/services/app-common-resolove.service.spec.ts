import { TestBed } from '@angular/core/testing';

import { AppCommonResolveService } from './app-common-resolve.service';

describe('AppCommonResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppCommonResolveService = TestBed.get(AppCommonResolveService);
    expect(service).toBeTruthy();
  });
});

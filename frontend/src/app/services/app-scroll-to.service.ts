import { Injectable } from '@angular/core';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

@Injectable({
    providedIn: 'root'
})
export class AppScrollToService {

    constructor(private _scrollToService: ScrollToService) {
    }
    public triggerScrollTo($destination) {

        const config: ScrollToConfigOptions = {
            target: $destination
        };

        this._scrollToService.scrollTo(config);
    }
}

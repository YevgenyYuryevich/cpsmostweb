import {Injectable} from '@angular/core';
import {AjaxApiService} from './ajax-api.service';
import {AppCommonService} from './app-common.service';

@Injectable()
export class AuthService {

    auth: any = false;
    isLoggedIn = false;
    redirectUrl: string;

    constructor(private ajaxApi: AjaxApiService, private appCommonService: AppCommonService) {
    }

    authorization() {
        if (this.auth) {
            return Promise.resolve(this.auth);
        } else {
            return this.ajaxApi.apiGet('api-auth/me', false).then((res) => {
                if (res && !res['error']) {
                    this.auth = res;
                    this.isLoggedIn = true;
                    return this.auth;
                } else {
                    this.isLoggedIn = false;
                    return false;
                }
            }, (error) => {
                this.isLoggedIn = false;
                console.log(error);
            });
        }
    }

    login(username: string, password: string) {
        return this.ajaxApi.apiPost('auth/login', {email: username, password: password, remember: 1})
            .then((user: any) => {
                // login successful if there's a jwt token in the response
                if (user) {
                    this.auth = user;
                    this.isLoggedIn = true;
                    return user;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                } else {
                    return false;
                }
            });
    }

    apiLogin(username: string, password: string) {
        return this.ajaxApi.apiPost('api-auth/login', {email: username, password: password})
            .then((res: any) => {
                const user = res.user;
                if (user) {
                    this.auth = user;
                    this.isLoggedIn = true;
                    return user;
                } else {
                    return false;
                }
            });
    }

    register(sets) {
        return this.ajaxApi.apiPost('api-auth/register', sets)
            .then((res: any) => {
                // login successful if there's a jwt token in the response
                const user = res.data;
                if (user) {
                    this.auth = user;
                    this.isLoggedIn = true;
                    return user;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                } else {
                    return false;
                }
            }, (res: any) => {
                const err = res.responseJSON || res.responseText;
                return Promise.reject(err.errors.email[0]);
            });
    }

    logout() {
        this.auth = false;
        this.isLoggedIn = false;
        this.ajaxApi.apiAuthToken = false;
        localStorage.removeItem('api-auth-token');
        this.ajaxApi.apiPost('auth/logout', {}, false);
        // The rest of this code assumes you are not using a library.
        // It can be made less wordy if you use one.
    }
}


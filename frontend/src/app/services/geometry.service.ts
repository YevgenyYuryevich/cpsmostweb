import { Injectable } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
declare var google: any;

@Injectable({
    providedIn: 'root'
})
export class GeometryService {

    geometry = false;
    constructor(private mapLoader: MapsAPILoader) { }
    calculateDistance(from, to) {
        const p1 =  new google.maps.LatLng(parseFloat(from.lat), parseFloat(from.lng));
        const p2 =  new google.maps.LatLng(parseFloat(to.lat), parseFloat(to.lng));
        return google.maps.geometry.spherical.computeDistanceBetween(p1, p2);
    }
}

import { Injectable } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Injectable({
    providedIn: 'root'
})
export class HelperService {

    postFields: any = {
        id: 'id',
        title: 'title',
        content: 'content',
        image: 'image',
        type: 'type',
        sort: 'sort',
        duration: 'duration',
        keywords: 'keywords',
    };
    constructor(private sanitizer: DomSanitizer) { }
    formatWebItem(itemData, from = 'default') {
        const fData: any = {};
        switch (from) {
            case 'youtube':
                fData.title = itemData.snippet.title;
                fData.summary = itemData.snippet.description;
                fData.content = this.makeYoutubeUrlById(itemData.id.videoId ? itemData.id.videoId : itemData.id);
                fData.image = itemData.snippet.thumbnails.high.url;
                if (itemData.snippet.tags) {
                    fData.keyword = itemData.snippet.tags;
                }
                fData.type = 'video';
                break;
            case 'podcasts':
                fData.title = itemData.title;
                fData.summary = '';
                fData.content = this.makeSpreakerUrlById(itemData.episode_id);
                fData.image = itemData.image_url;
                fData.type = 'audio';
                break;
            case 'blog':
                fData.title = itemData.name;
                fData.summary = '';
                fData.content = itemData.body;
                fData.image = itemData.image;
                fData.type = 'text';
                break;
            case 'recipes':
                fData.title = itemData.title;
                fData.summary = '';
                fData.content = itemData.source_url;
                fData.image = itemData.image_url;
                fData.type = 'text';
                break;
            default:
                fData.title = itemData.title;
                fData.summary = itemData.summary;
                fData.content = itemData.body;
                fData.image = itemData.image;
                fData.type = itemData.type || 'text';
                break;
        }
        fData.origin = itemData;
        return fData;
    }
    postFormat(data) {
        const fData = {};
        for (const k in data) {
            if (this.postFields[k]) {
                fData[this.postFields[k]] = data[k];
            }
        }
        return fData;
    }
    makeYoutubeUrlById(id) {
        return 'https://www.youtube.com/watch?v=' + id;
    }
    makeSpreakerUrlById(id) {
        return 'https://api.spreaker.com/v2/episodes/' + id + '/play';
    }
    extractTypeFromFileType(fType: string) {
        const ar = fType.split('/');
        return ar[0];
    }
    mileToMeter( mile: number ) {
        return mile * 1609.34;
    }
    meterToMile(meter: number) {
        return meter / 1609.34;
    }
    getSourceTypeFromUrl(url) {
        if (url.match(/\.(jpeg|jpg|gif|png)$/) != null) {
            return 'image';
        }
        if (url.match(/\.(mp4|avi)$/)) {
            return 'video';
        }
        if (url.match(/\.(pdf|text|txt)$/)) {
            return 'document';
        }
        return 'other';
    }
    trustUrl(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
    utf8ToBase64( str ) {
        const str64 = window.btoa(unescape(encodeURIComponent( str )));
        let str64n = '';
        for (const c of str64) {
            str64n = c + str64n;
        }
        return str64n;
    }
    isYoutubeEmbed(url) {
        const ytRegExp = /^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/embed\/.+/;
        return ytRegExp.test(url);
    }
    getYoutubeId(url) {
        const video_id = url.split('v=')[1];
        const ampersandPosition = video_id.indexOf('&');
        if (ampersandPosition !== -1) {
            return video_id.substring(0, ampersandPosition);
        }
        return video_id;
    }
    getYoutubeEmbed(url) {
        return 'https://www.youtube.com/embed/' + this.getYoutubeId(url);
    }
    capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}

import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
declare var $: any;

@Injectable()
export class AjaxApiService {

    BASE_URL: string = window.location.protocol + '//' + window.location.host;
    // BASE_URL = 'http://job.ncms.com';

    ROOT_URL: string = this.BASE_URL + '/backend/public';
    API_ROOT_URL: string = this.ROOT_URL + '/api';
    PAGE_ROOT_URL: string = this.ROOT_URL + '/page';
    requestCnt = 0;
    showSpinner = true;
    token = 'noToken';
    _token = 'noToken';
    apiAuthToken: any = false;
    showSpinner$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    hideSpinnerDelay: any = 0;
    constructor() {
        if (localStorage.getItem('api-auth-token')) {
            this.apiAuthToken = localStorage.getItem('api-auth-token');
        }
        $.ajaxSetup({
            beforeSend: (xhr) => {
                this.requestCnt ++;
                if (this.showSpinner) {
                    setTimeout(() => {
                        this.showSpinner$.next(true);
                    }, 0);
                }
            },
            complete: (res, status) => {
                if (status === 'success') {
                    res.then((d: any) => {
                        if (d.token) {
                            this.token = d.token;
                        }
                        if (d._token) {
                            this.token = d._token;
                        }
                        if (d.api_auth_token) {
                            this.apiAuthToken = d.api_auth_token;
                            localStorage.setItem('api-auth-token', this.apiAuthToken);
                        }
                        this.requestCnt --;
                        if (this.requestCnt === 0) {
                            setTimeout(() => {
                                this.showSpinner$.next(false);
                                this.hideSpinnerDelay = 0;
                            }, this.hideSpinnerDelay);
                        }
                    }, () => {
                        this.requestCnt --;
                        if (this.requestCnt === 0) {
                            setTimeout(() => {
                                this.showSpinner$.next(false);
                                this.hideSpinnerDelay = 0;
                            }, this.hideSpinnerDelay);
                        }
                    });
                }
            },
            error: (error) => {
                this.requestCnt --;
                if (this.requestCnt === 0) {
                    setTimeout(() => {
                        this.showSpinner$.next(false);
                        this.hideSpinnerDelay = 0;
                    }, this.hideSpinnerDelay);
                }
            }
        });
    }
    post(absUrl, data: any = {}, showSpinner = true, hideSpinnerDelay: any = false) {
        if (hideSpinnerDelay !== false) {
            this.hideSpinnerDelay = hideSpinnerDelay;
        }
        this.showSpinner = showSpinner;
        data._token = this.token;
        if (this.apiAuthToken) {
            data.api_token = this.apiAuthToken;
        }
        return $.ajax({
            url: absUrl,
            data: data,
            type: 'post',
            dataType: 'json',
        }).then((res) => {
            if (res.token || res._token) {
                return res.data;
            }
            return res;
        });
    }
    get(absUrl: string, showSpinner = true, hideSpinnerDelay: any = false) {
        if (hideSpinnerDelay !== false) {
            this.hideSpinnerDelay = hideSpinnerDelay;
        }
        if (this.apiAuthToken) {
            if (absUrl.indexOf('?') > -1) {
                absUrl = absUrl + '&api_token=' + encodeURIComponent(this.apiAuthToken);
            } else {
                absUrl = absUrl + '?api_token=' + encodeURIComponent(this.apiAuthToken);
            }
        }
        this.showSpinner = showSpinner;
        return $.ajax({
            url: absUrl,
            type: 'get',
            headers: {
                'X-auth-token': this._token
            }
        }).then((res) => {
            if (res.token || res._token) {
                return res.data;
            }
            return res;
        });
    }

    multiPartPost(url, data, showSpinner = true, hideSpinnerDelay: any = false) {
        this.showSpinner = showSpinner;
        if (hideSpinnerDelay !== false) {
            this.hideSpinnerDelay = hideSpinnerDelay;
        }
        data.append('_token', this.token);
        if (this.apiAuthToken) {
            data.append('api_token', this.apiAuthToken);
        }
        return $.ajax({
            url: url,
            type: 'post',
            data: data,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
        }).then(function (res) {
            if (res.token) {
                return res.data;
            }
            return res;
        });
    }
    apiMultiPartPost(endUrl, data, showSpinner = true, hideSpinnerDelay: any = false) {
        return this.multiPartPost(this.API_ROOT_URL + '/' + endUrl, data, showSpinner, hideSpinnerDelay);
    }
    apiGet(endUrl, showSpinner = true, hideSpinnerDelay: any = false) {
        return this.get(this.API_ROOT_URL + '/' + endUrl, showSpinner, hideSpinnerDelay);
    }
    apiPost(endUrl, data = {}, showSpinner = true, hideSpinnerDelay: any = false) {
        return this.post(this.API_ROOT_URL + '/' + endUrl, data, showSpinner, hideSpinnerDelay);
    }
    pagePost(endUrl, data = {}, showSpinner = true, hideSpinnerDelay: any = false) {
        return this.post(this.PAGE_ROOT_URL + '/' + endUrl, data, showSpinner, hideSpinnerDelay);
    }
    reqCnt() {
        return this.requestCnt;
    }
    setReqCnt(cnt: number) {
        this.requestCnt = cnt >= 0 ? cnt : 0;
        return this.requestCnt;
    }
}

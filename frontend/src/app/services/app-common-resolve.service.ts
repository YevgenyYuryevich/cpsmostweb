import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AjaxApiService} from './ajax-api.service';

@Injectable({
  providedIn: 'root'
})
export class AppCommonResolveService implements Resolve<any> {

    constructor(private ajaxApi: AjaxApiService) { }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.ajaxApi.apiGet('app_common').then((res) => {
            return res;
        });
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AjaxApiService} from './ajax-api.service';
import {AuthService} from './auth.service';
import {HelperService} from './helper.service';
import {GeocodeService} from './geocode.service';
import {GeometryService} from './geometry.service';
import {AppScrollToService} from './app-scroll-to.service';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [],
    providers: [
        AjaxApiService,
        AuthService,
        HelperService,
        GeocodeService,
        GeometryService,
        AppScrollToService
    ]
})
export class ServicesModule { }

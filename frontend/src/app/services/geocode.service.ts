import { Injectable } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { tap, map, switchMap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { Location } from '../interfaces/location';

declare var google: any;

@Injectable()
export class GeocodeService {
    geocoder: any;
    currentPosition: Promise<any> = Promise.resolve(false);
    constructor(private mapLoader: MapsAPILoader) {
        if (navigator.geolocation) {
            this.currentPosition = new Promise((resolve, reject) => {
                navigator.geolocation.getCurrentPosition((position) => {
                    resolve(position.coords);
                }, () => {
                    resolve(false);
                });
            });
        }
    }

    private initGeocoder() {
        this.geocoder = new google.maps.Geocoder();
    }

    public waitForMapsToLoad(): Observable<boolean> {
        if ( !this.geocoder ) {
            return fromPromise(this.mapLoader.load())
                .pipe(
                    tap(() => this.initGeocoder()),
                    map(() => true)
                );
        }
        return of(true);
    }

    geocodeAddress(location: string): Observable<Location> {
        return this.waitForMapsToLoad().pipe(
            // filter(loaded => loaded),
            switchMap(() => {
                return new Observable(observer => {
                    this.geocoder.geocode({'address': location}, (results, status) => {
                        if (status == google.maps.GeocoderStatus.OK) {
                            console.log('Geocoding complete!');
                            observer.next({
                                lat: results[0].geometry.location.lat(),
                                lng: results[0].geometry.location.lng()
                            });
                        } else {
                            console.log('Error - ', results, ' & Status - ', status);
                            observer.next({ lat: 0, lng: 0 });
                        }
                        observer.complete();
                    });
                });
            })
        );
    }
    geocodeByLatLng(lat, lng): Observable<any> {
        return this.waitForMapsToLoad().pipe(
            switchMap(() => {
                const latLng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                return new Observable(observer => {
                    this.geocoder.geocode({latLng: latLng}, (results, status) => {
                        if (status == google.maps.GeocoderStatus.OK) {
                            let postalCode = false;
                            const addressComponents = results[0].address_components;
                            addressComponents.forEach((address) => {
                               const types = address.types;
                               types.forEach((type) => {
                                   if (type === 'postal_code') {
                                       postalCode = postalCode || address.long_name;
                                   }
                               });
                            });
                            observer.next(postalCode);
                        } else {
                            console.log('Error - ', results, ' & Status - ', status);
                            observer.next(false);
                        }
                        observer.complete();
                    });
                });
            })
        );
    }
}

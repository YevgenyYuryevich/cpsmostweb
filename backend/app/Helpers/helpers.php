<?php

namespace Helpers;

use GuzzleHttp\Client;

function parseStringToArray($data){
    $data = json_decode($data);
    return json_decode(json_encode($data), true);
}
function getGeolocation($address) {
    $client = new Client();
    $res = $client->get('https://maps.googleapis.com/maps/api/geocode/json', [
        'query' => [
            'address' => $address,
            'key' => env('GOOGLE_MAP_API_KEY')
        ]
    ]);
    $resArr = parseStringToArray($res->getBody()->getContents());
    return isset($resArr['results']) && count($resArr['results']) ? $resArr['results'][0]['geometry']['location'] : false;
}
function appBase64Decode($str) {
    return base64_decode(strrev($str));
}
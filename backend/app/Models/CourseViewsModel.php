<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseViewsModel extends Model
{
    //
    protected $table = 'course_views';
    public $timestamps = false;
}

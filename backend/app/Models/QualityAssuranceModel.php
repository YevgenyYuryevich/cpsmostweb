<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QualityAssuranceModel extends Model
{
    //
    protected $table = 'quality_assurance';
    protected $fillable = ['user_id', 'media_id'];
    public $timestamps = false;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormLibraryModel extends Model
{
    //
    protected $table = 'form_library';
    protected $fillable = ['user_id', 'media_id', 'add_to_vendor', 'add_to_instructor'];
    public $timestamps = false;
}

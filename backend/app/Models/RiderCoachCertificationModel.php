<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiderCoachCertificationModel extends Model
{
    //
    protected $table = 'rider_coach_certification';
    public $timestamps = false;
    protected $fillable = [
        'instructor',
        'source',
        'state',
        'valid_from',
        'valid_to',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncidentsModel extends Model
{
    //
    protected $table = 'incidents';
    protected $fillable = [
        'vendor',
        'description',
        'address',
        'city',
        'zip_code',
        'geolocation_lat',
        'geolocation_lng',
        'date',
        'time'
    ];
    public $timestamps = false;
    public function media() {
        return $this->belongsToMany('App\Models\MediaModel', 'incident_media', 'incident_id', 'media_id');
    }
}

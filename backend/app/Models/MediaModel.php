<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediaModel extends Model
{
    //
    protected $table = 'media';
    protected $fillable = [
        'source',
        'type',
        'name'
    ];
    public $timestamps = false;
    public function incidents() {
        return $this->belongsToMany('App\Models\IncidentsModel', 'incident_media', 'media_id', 'incident_id');
    }
}

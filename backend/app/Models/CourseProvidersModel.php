<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseProvidersModel extends Model
{
    //
    protected $table = 'course_providers';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'phone',
        'address',
        'city',
        'state',
        'zip',
        'contact_person',
        'contact_email',
        'image',
        'url',
        'geolocation_lat',
        'geolocation_lng',
        'origin_id'
    ];
}

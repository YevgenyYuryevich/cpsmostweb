<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseClicksModel extends Model
{
    //
    protected $table = 'course_clicks';
    public $timestamps = false;
}

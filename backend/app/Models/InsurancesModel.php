<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsurancesModel extends Model
{
    //
    protected $table = 'insurances';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'source',
        'state',
        'valid_from',
        'valid_to',
    ];
}

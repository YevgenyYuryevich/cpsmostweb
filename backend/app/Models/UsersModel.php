<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersModel extends Model
{
    //
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password', 'status', 'provider', 'company', 'address', 'phone', 'quality_assurance'
    ];
    public function role() {
        return $this->hasOne('App\Models\UserLevelsModel', 'id', 'level');
    }
    public function hasRole($role) {
        $roleRow = $this->role()->first();
        return strtolower($roleRow->name) === strtolower($role);
    }
}

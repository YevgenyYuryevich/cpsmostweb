<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseTypesModel extends Model
{
    //
    protected $table = 'course_types';
    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'image',
        'origin_id',
    ];
}

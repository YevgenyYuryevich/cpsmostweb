<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RenewalApplicationModel extends Model
{
    //
    protected $table = 'renewal_application';
    public $timestamps = false;
    protected $fillable = [
        'application',
        'driving_record',
        'pdw',
        'state',
        'valid_from',
        'valid_to',
        'user_id',
        'type',
        'from_id'
    ];
}

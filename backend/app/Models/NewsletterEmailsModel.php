<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterEmailsModel extends Model
{
    //
    protected $table = 'newsletter_emails';
    public $timestamps = false;
    protected $fillable = [
        'email'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemindersModel extends Model
{
    //
    protected $table = 'reminders';
    protected $fillable = [
        'name',
        'body',
        'group',
        'type',
        'datetime'
    ];
}

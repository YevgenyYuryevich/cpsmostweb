<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoursesModel extends Model
{
    //
    protected $table = 'courses';
    protected $fillable = [
        'name',
        'type',
        'provider_company',
        'description',
        'instructor',
        'date_time',
        'address',
        'city',
        'state',
        'zip',
        'phone',
        'email',
        'image',
        'geolocation_lat',
        'geolocation_lng',
        'open_seats',
        'start_at',
        'end_at',
        'origin_id',
        'origin_data',
        'cost',
        'register_data'
    ];
}

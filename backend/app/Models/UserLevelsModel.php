<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLevelsModel extends Model
{
    //
    protected $table = 'user_levels';
    protected $fillable = ['name', 'can_form_library'];
    public $timestamps = false;
    public function users() {
        return $this->hasMany('App\Models\UsersModel', 'level');
    }
}

<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
//        'api/auth/get',
        'api/auth/register',
        'api/auth/login',
        'api/auth/logout',
        'api/password/email',
        'api/password/reset',
        'api/users/update',
        'api/users/insert',
        'api/user-levels/insert',
        'api/user-levels/update',
        'api/user-levels/delete',
    ];
}

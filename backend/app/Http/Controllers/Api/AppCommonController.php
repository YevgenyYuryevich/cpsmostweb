<?php

namespace App\Http\Controllers\api;

use App\Models\UserLevelsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppCommonController extends Controller
{
    //
    public function __construct()
    {
//        $this->middleware('auth');
    }
    public function index() {
        $levels = UserLevelsModel::all();
        return Response()->json(['token' => csrf_token(), 'data' => [
            'user_levels' => $levels,
        ]]);
    }
}

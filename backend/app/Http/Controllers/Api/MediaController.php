<?php

namespace App\Http\Controllers\Api;

use App\Models\MediaModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    //
    public function upload(Request $request) {
        $file = $request->file('media');
        $mimeType = $file->getMimeType();
        $name = $file->getClientOriginalName();
        $mediaType = str_before($mimeType, '/');
        $where = 'media/' . $mediaType;
        $path = $file->store($where);
        $uploadUrl = url( 'storage/' . $path );
        $row = MediaModel::create([
            'source' => $uploadUrl,
            'type' => $mediaType,
            'name' => $name,
        ]);
        return Response()->json($row);
    }
}

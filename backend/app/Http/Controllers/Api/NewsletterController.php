<?php

namespace App\Http\Controllers\Api;

use App\Models\NewsletterEmailsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    //
    public function subscribe(Request $request) {
        $email = $request->get('email');
        $row = NewsletterEmailsModel::create(['email' => $email]);
        return Response()->json($row);
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request['where'] : [];
        $rows = NewsletterEmailsModel::where($where)->get();
        return Response()->json($rows);
    }
}

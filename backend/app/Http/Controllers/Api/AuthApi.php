<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthApi extends Controller
{
    //
    use AuthenticatesUsers;
    protected $redirectTo = '/admin';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function get(Request $request) {
        if (!Auth::check()) {
            $user = User::find(1);
            Auth::login($user);
        }
        $user = Auth::user();
        return Response()->json(['data' => $user, 'token' => csrf_token()]);
    }
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: Response()->json(['data' => $this->guard()->user(), 'token' => csrf_token()]);
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: Response()->json(['token' => csrf_token()]);
    }
}

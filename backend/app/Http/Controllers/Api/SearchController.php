<?php

namespace App\Http\Controllers\Api;

use Crew\Unsplash\HttpClient;
use Crew\Unsplash\Photo;
use Crew\Unsplash\Search;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    //
    public $youtubeApi;
    public $unsplash;
    public $unsplashSearch;
    private $azureKey;
    private $defaultPhoto = '';
    public function __construct()
    {
        $client = new \Google_Client();
        $client->setDeveloperKey(env('GOOGLE_DEVELOPER_KEY'));
        $this->youtubeApi = new \Google_Service_YouTube($client);
        HttpClient::init([
            'applicationId'	=> '98ed5ecd389a5b19e3ae817fdcec9b184388937388ba185cd76fadb9d27eca87',
            'utmSource' => 'yevgeny photo web'
        ]);
        $this->unsplash = new Photo();
        $this->unsplashSearch = new Search();
        $this->azureKey = env('AZURE_BING_KEY');
    }

    public function index(Request $request) {
        $keyword = utf8_encode($request->get('q'));
        $size = (int) $request->get('size');
        $items = $request->get('items');
        $data = [];
        foreach ($items as $item){
            $token = isset($item['token']) ? $item['token'] : false;
            $token = $token == 'false' ? false : $token;

            switch ($item['name']){
                case 'youtube':
                    $posts = $this->searchYoutube($keyword, $size, $token);
                    break;
                case 'spreaker':
                    $posts = $this->searchSpreaker($keyword, $size, $token);
                    break;
                case 'blogs':
                    $posts = $this->searchAzureBing($keyword, $size, $token ? $token : 0);
                    break;
            }
            $data[$item['name']] = $posts;
        }
        return Response()->json(['data' => $data]);
    }
    public function searchYoutube($keyword, $size = 10, $pageToken = false){
        if (!!$pageToken){
            $searchResponse = $this->youtubeApi->search->listSearch('snippet', [
                'type' => 'video',
                'q' => $keyword,
                'maxResults' => $size,
                'pageToken' => $pageToken
            ]);
        }
        else{
            $searchResponse = $this->youtubeApi->search->listSearch('snippet', [
                'type' => 'video',
                'q' => $keyword,
                'maxResults' => $size
            ]);
        }
        $items = [];
        foreach ($searchResponse->items as $item){
            array_push($items, $item);
        }
        return ['items' => $items, 'nextPageToken' => $searchResponse->nextPageToken];
    }
    public function searchSpreaker($keyword, $size = 10, $pageToken = false) {
        $curl = curl_init();
        if ($pageToken){
            $url = $pageToken;
        }
        else{
            $keyword = urlencode($keyword);
            $url = 'https://api.spreaker.com/v2/search?type=episodes&q='. $keyword .'&limit=' . $size;
        }
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $resObj = json_decode(curl_exec($curl));
        if (isset($resObj->response->error)) {
            return ['items' => [], 'nextPageToken' => false];
        }
        return ['items' => $resObj->response->items, 'nextPageToken' => $resObj->response->next_url];
    }
    public function searchAzureBing($keyword = 'rand', $size = 10, $pageToken = 0) {
        $keyword = $keyword ? $keyword : 'rand';
        $headers = "Ocp-Apim-Subscription-Key: $this->azureKey\r\n";
        $options = array ('http' => array (
            'header' => $headers,
            'method' => 'GET'));

        // Perform the Web request and get the JSON response
        $context = stream_context_create($options);
        $result = file_get_contents("https://api.cognitive.microsoft.com/bing/v7.0/search?q=" . urlencode($keyword) . "&offset=" . $pageToken . "&count=" . $size, false, $context);
        $result = \Helpers\parseStringToArray($result);
        $items = isset($result) && isset($result['webPages']) ? $result['webPages']['value'] : [];

        $imgs = $items ? $this->getPhotosFromUsp($keyword, $size, $pageToken) : [];
        $img_ct = count($imgs);
        foreach ($items as $key => &$post){
            $imgUrl = $img_ct ? $imgs[$key % $img_ct]['urls']['small'] : $this->defaultPhoto;
            $post['image'] = $imgUrl;
            $post['body'] = $this->makeBodyFromAzureBing($post);
        }
        return ['items' => $items, 'nextPageToken' => count($items) + (int) $pageToken];
    }
    public function getPhotosFromUsp($query, $size = 10, $pageToken = 0){
        $result = $this->unsplashSearch::photos($query, $pageToken, $size);
        return $result->getResults();
    }
    private function makeBodyFromAzureBing($item) {
        return $item['snippet'] . '<div><a href="'. $item['url'] .'">View Detail</a></div>';
    }
}

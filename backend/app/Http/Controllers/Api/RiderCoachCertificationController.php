<?php

namespace App\Http\Controllers\Api;

use App\Mail\ApproveInsurance;
use App\Mail\DenyInsurance;
use App\Models\RiderCoachCertificationModel;
use App\Models\UsersModel;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class RiderCoachCertificationController extends Controller
{
    //
    public function insert(Request $request) {
        $sourceFile = $request->file('source');
        $path = $sourceFile->store('rider-coach-certification');
        $source = url('storage/' . $path);
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        $sets['source'] = $source;
        $me = $request->user();
        if ($me->hasRole('instructor')) {
            $sets['instructor'] = $me['id'];
        }
        $row = RiderCoachCertificationModel::create($sets);
        return Response()->json($row);
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $where = parseStringToArray($where);
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        $me = $request->user();
        if ($me->hasRole('instructor')) {
            $rows = RiderCoachCertificationModel::where($where)->get();
            foreach ($rows as $row) {
                if ((int) $row['instructor'] !== (int) $me['id']) {
                    return Response()->json('access denied');
                }
            }
            if (isset($sets['instructor'])) {
                $sets['instructor'] = $me['id'];
            }
        }
        if ($request->hasFile('source')) {
            $sourceFile = $request->file('source');
            $path = $sourceFile->store('rider-coach-certification');
            $sets['source'] = url('storage/' . $path);
        }
        RiderCoachCertificationModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function get(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $row = RiderCoachCertificationModel::where($where)->first();
        return Response()->json($row);
    }
    public function getMany(Request $request) {
        $me = $request->user();
        $where = $request->has('where') ? $request->get('where') : [];
        if ($me->hasRole('instructor') || $me->hasRole('provider')) {
            $where['instructor'] = $me['id'];
        }
        $rows = RiderCoachCertificationModel::where($where)->get();
        return Response()->json($rows);
    }
    public function approve(Request $request) {
        $id = $request->get('id');
        $msg = $request->get('message');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $row = RiderCoachCertificationModel::find($id);
        $user = UsersModel::find($row['instructor']);
        Mail::to($user['email'])->send(new ApproveInsurance($msg));
        RiderCoachCertificationModel::where('id', $id)->update(['valid_to' => $endDate, 'valid_from' => $startDate, 'state' => 'approved']);
        return Response()->json($user);
    }
    public function deny(Request $request) {
        $id = $request->get('id');
        $msg = $request->get('message');
        $row = RiderCoachCertificationModel::find($id);
        $user = UsersModel::find($row['instructor']);
        Mail::to($user['email'])->send(new DenyInsurance($msg));
        RiderCoachCertificationModel::where('id', $id)->update(['state' => 'denied']);
        return Response()->json($user);
    }
}

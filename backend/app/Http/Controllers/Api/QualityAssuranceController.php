<?php

namespace App\Http\Controllers\Api;

use App\Models\MediaModel;
use App\Models\QualityAssuranceModel;
use App\Models\UsersModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QualityAssuranceController extends Controller
{
    //
    private $model;
    public function __construct()
    {
        $this->model = new QualityAssuranceModel();
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $me = $request->user();
        $isAdmin = $me->hasRole('admin');
        $rows = [];
        $all = $this->model->where($where)->get();
        foreach ($all as & $row) {
            if (!$isAdmin) {
                if ($row['user_id'] != $me['id'] && ((int) $row['is_public'] == 0)) {
                    continue;
                }
            }
            $this->format($row);
            array_push($rows, $row);
        }
        return Response()->json($rows);
    }
    public function insert(Request $request) {
        $sets = $request->get('sets');
        $user = $request->user();
        $defaultSets = [
            'user_id' => $user['id'],
        ];
        $sets = array_merge($defaultSets, $sets);
        $row = $this->model->create($sets);
        $this->format($row);
        return Response()->json($row);
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $sets = $request->get('sets');
        QualityAssuranceModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function delete(Request $request) {
        $where = $request->get('where');
        $this->model->where($where)->delete();
        return Response()->json(['where' => $where]);
    }
    public function format(& $row) {
        $user = UsersModel::find($row['user_id']);
        $row['user_name'] = $user['name'];
        $media = MediaModel::find($row['media_id']);
        $row['media_url'] = $media['source'];
        $row['is_public'] = (int) $row['is_public'];
        return $row;
    }
}

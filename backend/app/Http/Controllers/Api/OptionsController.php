<?php

namespace App\Http\Controllers\Api;

use App\Models\OptionsModel;
use function Helpers\appBase64Decode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OptionsController extends Controller
{
    //
    public function getSocial() {
        $rows = OptionsModel::where('name', 'social_link_twitter')
            ->orWhere('name', 'social_link_facebook')
            ->orWhere('name', 'social_link_google')
            ->orWhere('name', 'social_link_pinterest')
            ->orWhere('name', 'social_link_instagram')->get();
        $rtn = [];
        foreach ($rows as $row) {
            $rtn[$row['name']] = $row['value'];
        }
        return Response()->json($rtn);
    }
    public function updateMany(Request $request) {
        $updates = $request->get('updates');
        foreach ($updates as $update) {
            OptionsModel::where($update['where'])->update($update['sets']);
        }
        return Response()->json(true);
    }
    public function updateOrCreate(Request $request) {
        $updates = $request->get('updates');
        foreach ($updates as $update) {
            if (isset($update['encoded']) && (int) $update['encoded']) {
                foreach ($update['sets'] as &$set) {
                    $set = appBase64Decode($set);
                }
                foreach ($update['where'] as &$where) {
                    $where = appBase64Decode($where);
                }
            }
            OptionsModel::updateOrCreate($update['where'], $update['sets']);
        }
        return Response()->json(true);
    }
    public function getOptions(Request $request) {
        $options = $request->get('options');
        $rlt = [];
        foreach ($options as $option) {
            $row = OptionsModel::where('name', $option)->first();
            $rlt[$option] = $row ? $row['value'] : false;
        }
        return Response()->json($rlt);
    }
}

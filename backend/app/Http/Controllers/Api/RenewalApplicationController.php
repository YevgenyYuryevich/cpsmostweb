<?php

namespace App\Http\Controllers\Api;

use App\Mail\ApproveRenewalApplication;
use App\Mail\DenyRenewalApplication;
use App\Models\FormLibraryModel;
use App\Models\MediaModel;
use App\Models\RenewalApplicationModel;
use App\Models\UsersModel;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class RenewalApplicationController extends Controller
{
    //
    public function getMany(Request $request)
    {
        $where = $request->has('where') ? $request->get('where') : [];
        $me = $request->user();
        if (!$me->hasRole('admin')) {
            $where['user_id'] = $me['id'];
        }
        $rows = RenewalApplicationModel::where($where)->get();
        return Response()->json($rows);
    }

    public function get(Request $request)
    {
        $where = $request->has('where') ? $request->get('where') : [];
        $row = RenewalApplicationModel::where($where)->first();
        return Response()->json($row);
    }

    public function insert(Request $request)
    {
        $sets = $request->has('sets') ? $request->get('sets') : [];
        $sets = parseStringToArray($sets);
        $me = $request->user();
        if (!$me->hasRole('admin')) {
            $sets['user_id'] = $me['id'];
        }
        if (!isset($sets['type'])) {
            $role = UsersModel::find($sets['user_id'])->role()->first();
            $sets['type'] = strtolower($role->name);
        }
        if ($request->hasFile('application')) {
            $path = $request->file('application')->store('renewal-application');
            $sets['application'] = url('storage/' . $path);
        }
        if ($request->hasFile('driving_record')) {
            $path = $request->file('driving_record')->store('renewal-application');
            $sets['driving_record'] = url('storage/' . $path);
        }
        if ($request->hasFile('pdw')) {
            $path = $request->file('pdw')->store('renewal-application');
            $sets['pdw'] = url('storage/' . $path);
        }
        $row = RenewalApplicationModel::create($sets);
        return Response()->json($row);
    }

    public function insertFromFormLibrary(Request $request) {
        $sets = $request->get('sets');
        $formLibrary = FormLibraryModel::find($sets['from_id']);
        $media = MediaModel::find($formLibrary['media_id']);
        $sets['application'] = $media->source;
        $sets['user_id'] = $request->user()->id;
        $row = RenewalApplicationModel::create($sets);
        return Response()->json($row);
    }

    public function update(Request $request)
    {
        $where = $request->get('where');
        $where = parseStringToArray($where);
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        $me = $request->user();
        if (!$me->hasRole('admin')) {
            $rows = RenewalApplicationModel::where($where)->get();
            foreach ($rows as $row) {
                if ((int)$row['user_id'] !== (int)$me['id']) {
                    return Response()->json('access denied');
                }
            }
            if (isset($sets['user_id'])) {
                $sets['user_id'] = $me['id'];
            }
        }
        if ($request->hasFile('application')) {
            $path = $request->file('application')->store('renewal-application');
            $sets['application'] = url('storage/' . $path);
        }
        if ($request->hasFile('driving_record')) {
            $path = $request->file('driving_record')->store('renewal-application');
            $sets['driving_record'] = url('storage/' . $path);
        }
        if ($request->hasFile('pdw')) {
            $path = $request->file('pdw')->store('renewal-application');
            $sets['pdw'] = url('storage/' . $path);
        }
        RenewalApplicationModel::where($where)->update($sets);
        return Response()->json(array_merge($sets, $where));
    }
    public function delete(Request $request) {
        $where = $request->get('where');
        RenewalApplicationModel::where($where)->delete();
        return Response()->json(['where' => $where]);
    }
    public function approve(Request $request)
    {
        $id = $request->get('id');
        $msg = $request->get('message');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $row = RenewalApplicationModel::find($id);
        $user = UsersModel::find($row['user_id']);
        Mail::to($user['email'])->send(new ApproveRenewalApplication($msg));
        RenewalApplicationModel::where('id', $id)->update(['valid_to' => $endDate, 'valid_from' => $startDate, 'state' => 'approved']);
        return Response()->json($user);
    }

    public function deny(Request $request)
    {
        $id = $request->get('id');
        $msg = $request->get('message');
        $row = RenewalApplicationModel::find($id);
        $user = UsersModel::find($row['user_id']);
        Mail::to($user['email'])->send(new DenyRenewalApplication($msg));
        RenewalApplicationModel::where('id', $id)->update(['state' => 'denied']);
        return Response()->json($user);
    }
}

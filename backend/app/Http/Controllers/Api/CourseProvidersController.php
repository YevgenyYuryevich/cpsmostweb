<?php

namespace App\Http\Controllers\Api;

use App\Models\CourseProvidersModel;
use function Helpers\getGeolocation;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseProvidersController extends Controller
{
    //
    public function getMany() {
        $providers = CourseProvidersModel::all();
        return Response()->json($providers);
    }
    public function get(Request $request) {
        $where = $request->get('where');
        $provider = CourseProvidersModel::where($where)->first();
        return Response()->json($provider);
    }
    public function insert(Request $request) {
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('courses/providers');
            $sets['image'] = url('storage/' . $path);
        }
        $data = CourseProvidersModel::create($sets);
        return Response()->json($data);
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $where = parseStringToArray($where);

        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('courses/providers');
            $sets['image'] = url('storage/' . $path);
        }
        CourseProvidersModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function delete(Request $request) {
        $where = $request->get('where');
        CourseProvidersModel::where($where)->delete();
        return Response()->json(true);
    }
    public function fillGeolocation() {
        $providers = CourseProvidersModel::all();
        foreach ($providers as $provider) {
            $location = $provider['address'] ? $provider['address'] : $provider['zip'];
            if ($location) {
                $geo = getGeolocation($location);
                if ($geo) {
                    CourseProvidersModel::where('id', $provider['id'])->update([
                        'geolocation_lat' => $geo['lat'],
                        'geolocation_lng' => $geo['lng'],
                    ]);
                }
            }
        }
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Models\UserLevelsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserLevelsController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }
    public function update(Request $request) {
        $id = $request->get('where');
        $sets = $request->get('sets');
        UserLevelsModel::where('id', $id)->update($sets);
        return Response()->json(['token' => csrf_token(), 'data' => $sets]);
    }
    public function insert(Request $request) {
        $sets = $request->get('sets');
        $row = UserLevelsModel::create($sets);
        return Response()->json(['data' => $row]);
    }
    public function delete($id) {
        UserLevelsModel::where('id', $id)->delete();
        return Response()->json(['data' => true]);
    }
}

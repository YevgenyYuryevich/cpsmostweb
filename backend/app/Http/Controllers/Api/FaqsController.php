<?php

namespace App\Http\Controllers\Api;

use App\Models\FaqsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqsController extends Controller
{
    //
    public function insert(Request $request) {
        $sort = FaqsModel::max('sort');
        $sets = $request->get('sets');
        $sets = array_merge(['sort' => $sort], $sets);
        $row = FaqsModel::create($sets);
        return Response()->json($row);
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $sets = $request->get('sets');
        FaqsModel::where($where)->update($sets);
        return Response()->json(true);
    }
    public function updateMany(Request $request) {
        $updates = $request->all('updates')['updates'];
        foreach ($updates as $update) {
            FaqsModel::where($update['where'])->update($update['sets']);
        }
        return Response()->json(true);
    }
    public function delete(Request $request) {
        $where = $request->get('where');
        FaqsModel::where($where)->delete();
        return Response()->json(true);
    }
    public function get(Request $request) {
        $where = $request->get('where');
        $row = FaqsModel::where($where)->first();
        return Response()->json($row);
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $rows = FaqsModel::where($where)->get();
        return Response()->json($rows);
    }
}

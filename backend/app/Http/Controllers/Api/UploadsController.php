<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UploadsController extends Controller
{
    //
    public function index(Request $request) {
        $file = $request->file('file');
        $where = $request->has('where') ? $request->get('where') : 'media';
        $path = $file->store($where);
        $url = 'storage/' . $path;
        return Response()->json(['data' => url($url)]);
    }
}

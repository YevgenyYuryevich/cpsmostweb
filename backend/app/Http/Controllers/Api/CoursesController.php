<?php

namespace App\Http\Controllers\Api;

use App\Models\CourseClicksModel;
use App\Models\CoursesModel;
use App\Models\CourseViewsModel;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{
    //
    public function insert(Request $request) {
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('courses');
            $sets['image'] = url('storage/' . $path);
        }
        $data = CoursesModel::create($sets);
        return Response()->json($data);
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $courses = CoursesModel::where($where)->get();
        return Response()->json($courses);
    }
    public function delete(Request $request) {
        $where = $request->get('where');
        CoursesModel::where($where)->delete();
        return Response()->json(true);
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $where = parseStringToArray($where);
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('courses');
            $sets['image'] = url('storage/' . $path);
        }
        CoursesModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function plusViews() {
        $row = CourseViewsModel::create([]);
        return Response()->json($row);
    }
    public function plusClicks() {
        $row = CourseClicksModel::create([]);
        return Response()->json($row);
    }
    public function getTracks() {
        $views = CourseViewsModel::all();
        $clicks = CourseClicksModel::all();
        return Response()->json([
            'views' => $views,
            'clicks' => $clicks
        ]);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\OptionsModel;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacebookController extends Controller
{
    //
    public function getFeed() {
        $fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID'),
            'app_secret' => env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v3.1'
        ]);
        try {
            $res = $fb->get('/me/feed', env('FACEBOOK_ACCESS_TOKEN'));
            var_dump($res->getGraphNode());
            die();
        } catch (FacebookResponseException $e) {
            var_dump('fr');
            exit(301);
        } catch (FacebookSDKException $e) {
            var_dump('fs');
            exit(302);
        }
    }
    public function loginUrl(Request $request) {
        if(!session_id()) {
            session_start();
        }

        $appIdOption = OptionsModel::where('name', 'facebook_app_id')->first();
        $appSecretOption = OptionsModel::where('name', 'facebook_app_secret')->first();

        $fb = new Facebook([
            'app_id' => $appIdOption ? $appIdOption['value'] : env('FACEBOOK_APP_ID'),
            'app_secret' => $appSecretOption ? $appSecretOption['value'] : env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v3.2'
        ]);
        $redirectUrl = $request->get('redirect_url');
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email', 'user_posts'];
        $loginUrl = $helper->getLoginUrl($redirectUrl, $permissions);
        return Response()->json(['url' => $loginUrl, 'state' => $_SESSION['FBRLH_state']]);
    }
    public function getToken(Request $request) {
        if(!session_id()) {
            session_start();
        }
        if ($request->has('FBRLH_state')) {
            $_SESSION['FBRLH_state'] = $request->get('FBRLH_state');
            OptionsModel::updateOrCreate(['name' => 'facebook_FBRLH_state'], ['value' => $_SESSION['FBRLH_state']]);
        } else {
        }
        $appIdOption = OptionsModel::where('name', 'facebook_app_id')->first();
        $appSecretOption = OptionsModel::where('name', 'facebook_app_secret')->first();
        $fb = new Facebook([
            'app_id' => $appIdOption ? $appIdOption['value'] : env('FACEBOOK_APP_ID'),
            'app_secret' => $appSecretOption ? $appSecretOption['value'] : env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v3.2'
        ]);

        $helper = $fb->getRedirectLoginHelper();
        try {
            $redirectUrl = $request->get('redirect_url');
            OptionsModel::updateOrCreate(['name' => 'facebook_redirect_url'], ['value' => $redirectUrl]);
            $accessToken = $helper->getAccessToken($redirectUrl);
            $token = $accessToken->getValue();
            OptionsModel::updateOrCreate(['name' => 'facebook_access_token'], ['value' => $token]);
            $response = $fb->get('/me/feed?fields=id,permalink_url,attachments{media_type,title,description,url_unshimmed,media{source}},message,full_picture', $token);
            $user_posts = $response->getDecodedBody();
            OptionsModel::updateOrCreate(['name' => 'facebook_feeds'], ['value' => json_encode($user_posts['data'])]);
            return Response()->json($user_posts['data']);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return Response()->json('Graph returned an error: ' . $e->getMessage());
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            return Response()->json('Facebook SDK returned an error: ' . $e->getMessage());
        }
    }
    public function feed() {
        if(!session_id()) {
            session_start();
        }
        $row = OptionsModel::where('name', 'facebook_FBRLH_state')->first();
        $_SESSION['FBRLH_state'] = $row->value;

        $appIdOption = OptionsModel::where('name', 'facebook_app_id')->first();
        $appSecretOption = OptionsModel::where('name', 'facebook_app_secret')->first();
        $fb = new Facebook([
            'app_id' => $appIdOption ? $appIdOption['value'] : env('FACEBOOK_APP_ID'),
            'app_secret' => $appSecretOption ? $appSecretOption['value'] : env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v3.2'
        ]);
        try {
            $row = OptionsModel::where('name', 'facebook_access_token')->first();
            $token = $row->value;
            $response = $fb->get('/me/feed?fields=id,permalink_url,attachments{media_type,title,description,url_unshimmed,media{source}},message,full_picture', $token);
            $user_posts = $response->getDecodedBody();
            OptionsModel::updateOrCreate(['name' => 'facebook_feeds'], ['value' => json_encode($user_posts['data'])]);
            return Response()->json($user_posts['data']);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return Response()->json('Graph returned an error: ' . $e->getMessage());
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            return Response()->json('Facebook SDK returned an error: ' . $e->getMessage());
        }
    }
}

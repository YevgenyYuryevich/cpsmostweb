<?php

namespace App\Http\Controllers\Api;

use App\Models\FormLibraryModel;
use App\Models\MediaModel;
use App\Models\UsersModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormLibraryController extends Controller
{
    //
    private $model;
    public function __construct()
    {
        $this->model = new FormLibraryModel();
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $me = $request->user();
        if (!$me->hasRole('admin')) {
            $where['user_id'] = $me['id'];
        }
        $rows = $this->model->where($where)->get();
        foreach ($rows as & $row) {
            $this->format($row);
        }
        return Response()->json($rows);
    }
    public function insert(Request $request) {
        $sets = $request->get('sets');
        $user = $request->user();
        $defaultSets = [
            'user_id' => $user['id'],
        ];
        $sets = array_merge($defaultSets, $sets);
        $row = $this->model->create($sets);
        $this->format($row);
        return Response()->json($row);
    }
    public function delete(Request $request) {
        $where = $request->get('where');
        $this->model->where($where)->delete();
        return Response()->json(['where' => $where]);
    }
    public function format(& $row) {
        $user = UsersModel::find($row['user_id']);
        $row['user_name'] = $user['name'];
        $media = MediaModel::find($row['media_id']);
        $row['media_url'] = $media['source'];
        $row['media'] = $media;
        return $row;
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $sets = $request->get('sets');
        FormLibraryModel::where($where)->update($sets);
        return Response()->json($sets);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\RemindersModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RemindersController extends Controller
{
    //
    public function insert(Request $request) {
        $sets = $request->get('sets');
        $reminder = RemindersModel::create($sets);
        return Response()->json($reminder);
    }
    public function update(Request $request) {
        $sets = $request->get('sets');
        $where = $request->get('where');
        RemindersModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function delete(Request $request) {
        $where = $request->get('where');
        RemindersModel::where($where)->delete();
        return Response()->json(true);
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $rows = RemindersModel::where($where)->get();
        return Response()->json($rows);
    }
    public function get(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $row = RemindersModel::where($where)->first();
        return Response()->json($row);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\PostsModel;
use function Helpers\appBase64Decode;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    //
    public function __construct()
    {
        // $this->middleware('auth');
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $posts = PostsModel::where($where)->get();
        return Response()->json($posts);
    }
    public function permanentResources() {
        $posts = PostsModel::where('permanent_resource', 1)->get();
        return Response()->json($posts);
    }
    public function get($id) {
        $post = PostsModel::find($id);
        return Response()->json($post);
    }
    public function insert(Request $request) {
        $sets = $request->all();
        unset($sets['_token']);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('posts');
            $url = url('storage/' . $path);
            $sets['image'] = $url;
        }
        $row = PostsModel::create($sets);
        return Response()->json(['data' => $row, '_token' => csrf_token()]);
    }
    public function insertMany(Request $request) {
        $items = $request->all('data')['data'];
        $rows = [];
        foreach ($items as $item) {
            $row = PostsModel::create($item);
            $rows[] = $row;
        }
        return Response()->json(['data' => $rows, 'token' => csrf_token()]);
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $sets = $request->get('sets');
        if ($request->has('encoded')) {
            $where = appBase64Decode($where);
            $sets = appBase64Decode($sets);
        }
        $where = parseStringToArray($where);
        $sets = parseStringToArray($sets);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('posts');
            $sets['image'] = url ('storage/' . $path);
        }
        PostsModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function updateMany(Request $request) {
        $updates = $request->all('updates')['updates'];
        foreach ($updates as $update) {
            PostsModel::where($update['where'])->update($update['sets']);
        }
        return Response()->json(true);
    }
    public function delete($id) {
        PostsModel::find($id)->delete();
        return Response()->json(true);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\OptionsModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class InstagramController extends Controller
{
    //
    public function getToken($code, Request $request) {

        $client = new Client();
        $rUri = $request->get('redirect_uri');

        $clientIdOption = OptionsModel::where('name', 'instagram_client_id')->first();
        $clientSecreteOption = OptionsModel::where('name', 'instagram_client_secrete')->first();

        $res = $client->post('https://api.instagram.com/oauth/access_token', [
            'form_params' => [
                'client_id' => $clientIdOption ? $clientIdOption['value'] : env('INSTAGRAM_CLIENT_ID'),
                'client_secret' => $clientSecreteOption ? $clientSecreteOption['value'] : env('INSTAGRAM_CLIENT_SECRETE'),
                'grant_type' => 'authorization_code',
                'redirect_uri' => $rUri,
                'code' => $code
            ]
        ]);
        $obj = json_decode($res->getBody()->getContents());
        $token = $obj->access_token;
        OptionsModel::updateOrCreate(['name' => 'instagram_access_token'], ['value' => $token]);
        $res = $client->get('https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $token);
        $obj->media = json_decode($res->getBody()->getContents());
        return Response()->json($obj);
    }
    public function feed() {
        $client = new Client();
        $row = OptionsModel::where(['name' => 'instagram_access_token'])->first();
        $token = $row->value;
        $res = $client->get('https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $token);
        $media = json_decode($res->getBody()->getContents());
        return Response()->json($media);
    }
}

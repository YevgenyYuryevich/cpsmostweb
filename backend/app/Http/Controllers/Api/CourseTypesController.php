<?php

namespace App\Http\Controllers\Api;

use App\Models\CourseTypesModel;
use function Helpers\appBase64Decode;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseTypesController extends Controller
{
    //
    public function getMany() {
        $types = CourseTypesModel::all();
        $purifier = new \HTMLPurifier();
        foreach ($types as & $type) {
            $subStr = trim(substr($type['description'], 0, 400));
            $fltStr = stripslashes($subStr . '...');
            $type['sub_description'] = $purifier->purify($fltStr);

            $subStr = trim(substr($type['description'], 0, 50));
            $fltStr = stripslashes($subStr . '...');
            $type['small_description'] = $purifier->purify($fltStr);
        }
        return Response()->json($types);
    }
    public function get(Request $request) {
        $where = $request->get('where');
        $row = CourseTypesModel::where($where)->first();
        return Response()->json($row);
    }
    public function insert(Request $request) {
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        if (isset($sets['description'])) {
            $sets['description'] = appBase64Decode($sets['description']);
        }
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('courses/types');
            $sets['image'] = url('storage/' . $path);
        }
        $data = CourseTypesModel::create($sets);
        return Response()->json($data);
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $where = parseStringToArray($where);
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        if (isset($sets['description'])) {
            $sets['description'] = appBase64Decode($sets['description']);
        }
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('courses/types');
            $sets['image'] = url('storage/' . $path);
        }
        CourseTypesModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function delete(Request $request) {
        $where = $request->get('where');
        CourseTypesModel::where($where)->delete();
        return Response()->json(true);
    }
}

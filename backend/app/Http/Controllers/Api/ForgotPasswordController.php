<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
    //
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return Response()->json($response);
    }
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return Response()->json($response);
    }
}
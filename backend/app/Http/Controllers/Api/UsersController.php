<?php

namespace App\Http\Controllers\Api;

use App\Models\UserLevelsModel;
use App\Models\UsersModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    //
    public function __construct()
    {
//        $this->middleware('auth');
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $users = UsersModel::where($where)->get();
        return Response()->json($users);
    }
    public function getInstructors() {
        $users = UserLevelsModel::where('name', 'instructor')->first()->users()->get();
        return Response()->json($users);
    }
    public function getVendors() {
        $vendors = UserLevelsModel::where('name', 'vendor')->first()->users()->get();
        return Response()->json($vendors);
    }
    public function get($id) {
        $user = UsersModel::find($id);
        return Response()->json(['token' => csrf_token(), 'data' => $user]);
    }
    public function update(Request $request) {
        $id = $_POST['where'];
        $sets = $_POST['sets'];
        if (isset($sets['password'])) {
            $sets['password'] = Hash::make($sets['password']);
        }
        UsersModel::where('id', $id)->update($sets);
        return Response()->json(['token' => csrf_token(), 'data' => $sets]);
    }
    public function insert(Request $request) {
        $sets = $_POST;
        unset($sets['_token']);
        $sets['password'] = Hash::make($sets['password']);
        $row = UsersModel::where('email', $sets['email'])->first();
        if ($row) {
            return Response()->json(['error' => $sets['email'] . ' already exists']);
        }
        UsersModel::insert($sets);
        return Response()->json(['token' => csrf_token(), 'data' => $sets]);
    }
    public function delete($id) {
        if ((int)$id !== 1) {
            UsersModel::where('id', $id)->delete();
            return Response()->json(['token' => csrf_token(), 'data' => true]);
        }
        else {
            return Response()->json(['token' => csrf_token(), 'data' => false]);
        }
    }
}

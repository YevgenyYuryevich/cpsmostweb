<?php

namespace App\Http\Controllers\Api;

use App\Models\OptionsModel;
use GuzzleHttp\Client;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventbriteController extends Controller
{
    //
    public function pullOrganizations(Request $request) {
        $token = $request->get('token');
        $client = new Client();
        $res = $client->get('https://www.eventbriteapi.com/v3/users/me/organizations', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            ]
        ]);
        $rlt = json_decode($res->getBody()->getContents());
        return Response()->json($rlt->organizations);
    }
    public function pullEvents(Request $request) {
        $row = OptionsModel::where('name', 'eventbrite_personal_token')->first();
        $token = $row->value;

        $row = OptionsModel::where('name', 'eventbrite_organization')->first();
        $organization = $request->has('organization') ? $request->get('organization') : $row->value;
        $client = new Client();
        $res = $client->get('https://www.eventbriteapi.com/v3/organizations/'. $organization .'/events', [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            ],
            'query' => [
                'token' => $token
            ]
        ]);
        $rlt = json_decode($res->getBody()->getContents());
        return Response()->json($rlt->events);
    }
}

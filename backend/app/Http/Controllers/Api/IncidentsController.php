<?php

namespace App\Http\Controllers\Api;

use App\Models\IncidentsModel;
use App\Models\UserLevelsModel;
use function Helpers\getGeolocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IncidentsController extends Controller
{
    //
    public function __construct()
    {
//        $this->middleware('guest')->except('logout');
    }
    public function getMany(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $me = Auth::check() ? Auth::user() : false;
        if ($me) {
            $level = UserLevelsModel::where('id', $me['level'])->first();
            if ($level['name'] === 'admin') {
            } else if ($level['name'] === 'instructor' || $level['name'] === 'provider'){
                $where['instructor'] = $me['id'];
            }
            $rows = IncidentsModel::where($where)->get();
            foreach ($rows as &$row) {
                $row['media'] = $row->media()->get();
            }
            return Response()->json($rows);
        }
        return Response()->json(false);
    }
    public function get(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $row = IncidentsModel::where($where)->first();
        $row['media'] = $row->media()->get();
        return Response()->json($row);
    }
    public function insert(Request $request) {
        $sets = $request->get('sets');
        $me = $request->user();
        if ($me->hasRole('instructor')) {
            $sets['instructor'] = $me['id'];
        }
        $location = isset($sets['address']) ? $sets['address'] : (isset($sets['zip_code']) ? $sets['zip_code'] : false);
        $geoLocation = $location ? getGeolocation($location) : false;
        if ($geoLocation) {
            $sets['geolocation_lat'] = $geoLocation['lat'];
            $sets['geolocation_lng'] = $geoLocation['lng'];
        }
        $row = IncidentsModel::create($sets);
        return Response()->json($row);
    }
    public function update(Request $request) {
        $me = $request->user();
        $where = $request->get('where');
        $sets = $request->get('sets');
        if ($me->hasRole('instructor')) {
            $incidents = IncidentsModel::where($where)->get();
            foreach ($incidents as $incident) {
                if ((int) $incident['instructor'] !== (int) $me['id']) {
                    return Response()->json('access denied');
                }
            }
            if (isset($sets['instructor'])) {
                $sets['instructor'] = $me['id'];
            }
        }
        $location = isset($sets['address']) ? $sets['address'] : (isset($sets['zip_code']) ? $sets['zip_code'] : false);
        $geoLocation = $location ? getGeolocation($location) : false;
        if ($geoLocation) {
            $sets['geolocation_lat'] = $geoLocation['lat'];
            $sets['geolocation_lng'] = $geoLocation['lng'];
        }
        IncidentsModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function syncMedia($id, Request $request) {
        $media = $request->get('media');
        if (is_string($media)) {
            $media = preg_split('/[\,\s]+/', $media);
        }
        $incident = IncidentsModel::find($id);
        $incident->media()->sync($media);
        return Response()->json($media);
    }
}

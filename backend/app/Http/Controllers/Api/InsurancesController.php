<?php

namespace App\Http\Controllers\Api;

use App\Mail\ApproveInsurance;
use App\Mail\DenyInsurance;
use App\Models\InsurancesModel;
use App\Models\UsersModel;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class InsurancesController extends Controller
{
    //
    public function insert(Request $request) {
        $sourceFile = $request->file('source');
        $path = $sourceFile->store('insurances');
        $source = url('storage/' . $path);
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        $sets['source'] = $source;
        $me = $request->user();
        if (!$me->hasRole('admin')) {
            $sets['user_id'] = $me['id'];
        }
        $row = InsurancesModel::create($sets);
        return Response()->json($row);
    }
    public function update(Request $request) {
        $where = $request->get('where');
        $where = parseStringToArray($where);
        $sets = $request->get('sets');
        $sets = parseStringToArray($sets);
        $me = $request->user();
        if (!$me->hasRole('admin')) {
            $rows = InsurancesModel::where($where)->get();
            foreach ($rows as $row) {
                if ((int) $row['user_id'] !== (int) $me['id']) {
                    return Response()->json('access denied');
                }
            }
            if (isset($sets['user_id'])) {
                $sets['user_id'] = $me['id'];
            }
        }
        if ($request->hasFile('source')) {
            $sourceFile = $request->file('source');
            $path = $sourceFile->store('insurances');
            $sets['source'] = url('storage/' . $path);
        }
        InsurancesModel::where($where)->update($sets);
        return Response()->json($sets);
    }
    public function get(Request $request) {
        $where = $request->has('where') ? $request->get('where') : [];
        $row = InsurancesModel::where($where)->first();
        return Response()->json($row);
    }
    public function getMany(Request $request) {
        $me = $request->user();
        $where = $request->has('where') ? $request->get('where') : [];
        if (!$me->hasRole('admin')) {
            $where['user_id'] = $me['id'];
        }
        $rows = InsurancesModel::where($where)->get();
        return Response()->json($rows);
    }
    public function approve(Request $request) {
        $id = $request->get('id');
        $msg = $request->get('message');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $insuranceRow = InsurancesModel::find($id);
        $user = UsersModel::find($insuranceRow['user_id']);
        Mail::to($user['email'])->send(new ApproveInsurance($msg));
        InsurancesModel::where('id', $id)->update(['valid_to' => $endDate, 'valid_from' => $startDate, 'state' => 'approved']);
        return Response()->json($user);
    }
    public function deny(Request $request) {
        $id = $request->get('id');
        $msg = $request->get('message');
        $insuranceRow = InsurancesModel::find($id);
        $user = UsersModel::find($insuranceRow['user_id']);
        Mail::to($user['email'])->send(new DenyInsurance($msg));
        InsurancesModel::where('id', $id)->update(['state' => 'denied']);
        return Response()->json($user);
    }
}

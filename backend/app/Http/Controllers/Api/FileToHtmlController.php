<?php

namespace App\Http\Controllers\Api;

use CloudConvert\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FileToHtmlController extends Controller
{
    //
    private $cloudApi;
    public function __construct()
    {
        $this->cloudApi = new Api(env('CLOUD_CONVERT_KEY'));
    }
    public function index(Request $request) {
        $file = $request->file('file');
        $sets = $request->has('sets') ? \Helpers\parseStringToArray($request->get('sets')) : [];
        $path = $file->store('temp');
        $path = storage_path('app/public/' . $path);
        $fileInfo = pathinfo($path);
        $name = preg_replace('/[\?\/\#\s\.]/', '-', $file->getClientOriginalName());
        $outputName = uniqid(isset($sets['prefix']) ? $sets['prefix'] : 'cvt') . '_' . $name . '.html';
        $outputPath = storage_path('app/public/document/') . $outputName;
        $outputUrl = url('storage/document/' . $outputName);
        $converterOptions = [];
        switch ($fileInfo['extension']) {
            case 'pdf':
                break;
            case 'doc':
                $converterOptions['simple_html'] = true;
                break;
            default:
                $converterOptions['simple_html'] = true;
                break;
        }
        $this->cloudApi->convert([
            'inputformat' => $fileInfo['extension'],
            'outputformat' => 'html',
            'input' => 'upload',
            'file' => fopen($path, 'r'),
            'converteroptions' => $converterOptions,
        ])->wait()->download($outputPath);
        $xml = new \DOMDocument();
        libxml_use_internal_errors(true);
        $xml->loadHTMLFile($outputUrl);
        $body = $xml->getElementsByTagName('body');
        $bodyHtml = $this->DomInnerHTML($body[0]);
        return Response()->json([
            'status' => true,
            'data' => $bodyHtml,
            'url' => $outputUrl
        ]);
    }
    public function domInnerHTML($element)
    {
        $innerHTML = "";
        $children  = $element->childNodes;

        foreach ($children as $child)
        {
            $innerHTML .= $element->ownerDocument->saveHTML($child);
        }
        return $innerHTML;
    }
}

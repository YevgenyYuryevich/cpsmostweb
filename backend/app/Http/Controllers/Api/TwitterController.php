<?php

namespace App\Http\Controllers\Api;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Models\OptionsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TwitterController extends Controller
{
    //
    public function recentTweets(Request $request) {
        $consumerKey = env('twitter_consumer_key');
        $consumerSecret = env('twitter_consumer_secret');
        $accessToken = env('twitter_access_token');
        $accessTokenSecret = env('twitter_access_token_secret');
        $screenName = $request->get('screen_name');
        $connection = new TwitterOAuth($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
        $tweets = $connection->get('statuses/user_timeline', ['count' => 20, 'exclude_replies' => true, 'screen_name' => $screenName]);
        $recentTweets = [];
        for ($i = 0 ; $i < 4 && $i < count($tweets); $i++) {
            $tweets[$i]->selected = true;
            array_push($recentTweets, $tweets[$i]);
        }
        OptionsModel::updateOrCreate(['name' => 'twitter_tweets'], ['value' => json_encode($recentTweets)]);
        return Response()->json($tweets);
    }
    public function feed() {
        $consumerKey = env('twitter_consumer_key');
        $consumerSecret = env('twitter_consumer_secret');
        $accessToken = env('twitter_access_token');
        $accessTokenSecret = env('twitter_access_token_secret');
        $row = OptionsModel::where('name', 'twitter_screen_name')->first();
        $screenName = $row->value;
        $connection = new TwitterOAuth($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
        $tweets = $connection->get('statuses/user_timeline', ['count' => 20, 'exclude_replies' => true, 'screen_name' => $screenName]);
        return Response()->json($tweets);
    }
    public function searchTweets(Request $request) {
        $consumerKey = $request->get('consumer_key');
        $consumerSecret = $request->get('consumer_secret');
        $accessToken = $request->get('access_token');
        $accessTokenSecret = $request->get('access_token_secret');
        $connection = new TwitterOAuth($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
        $tweets = $connection->get('search/tweets', ['q' => 'post']);
        return Response()->json($tweets);
    }
}

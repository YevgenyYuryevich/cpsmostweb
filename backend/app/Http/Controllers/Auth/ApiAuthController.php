<?php

namespace App\Http\Controllers\Auth;

use App\Models\UsersModel;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class ApiAuthController extends Controller
{
    //
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if ($request->user()->status === 'pending') {
                return Response()->json('pending status');
            }
            $token = Str::random(60);
            $request->user()->forceFill([
                'api_token' => hash('sha256', $token)
            ])->save();
            return Response()->json([
                'api_auth_token' => $token,
                'user' => Auth::user()
            ]);
        } else {
            return Response()->json('invalid credentials');
        }
    }

    public function register(Request $request)
    {
        $data = $request->all();
        if (Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string']
        ])->validate()) {
            $credentials = $request->only('email', 'name', 'password', 'company', 'phone', 'level');
            $credentials['password'] = Hash::make($credentials['password']);
            $row = UsersModel::create($credentials);
            return Response()->json($row);
        }
        return Response()->json(['error' => 'no validation']);
    }

    public function get(Request $request)
    {
        $me = $request->user();
        if ($me) {
            return Response()->json($me);
        } else {
            return Response()->json(['error' => 'invalid credentials']);
        }
    }
}

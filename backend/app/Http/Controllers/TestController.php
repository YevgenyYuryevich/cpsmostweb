<?php

namespace App\Http\Controllers;

use App\Models\OptionsModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class TestController extends Controller
{
    //
    public function index() {
        $str = '9FjM6ICZpJye';
        base64_decode($str);
    }
    public function callback(Request $request) {
        $row = OptionsModel::create(['name' => 'instagram_code', 'value' => $request->get('code')]);
        return Response()->json($row);
    }
}

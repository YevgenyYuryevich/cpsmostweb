<?php

namespace App\Http\Controllers\Cron;

use App\Models\CourseProvidersModel;
use App\Models\CoursesModel;
use App\Models\CourseTypesModel;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPHtmlParser\Dom;

class T3rgController extends Controller
{
    //
    private $formData;
    public function index() {
        $rows = $this->pullByCC(291, 'BRCU', 'NRC');
        $rows = array_merge($rows, $this->pullByCC(40, 'BRC2U', 'SRC-SP'));
        foreach ($rows as $key => &$row) {
            $provider = CourseProvidersModel::where(['origin_id' => $row['provider_company']])->first();
            if ($provider) {
                $row['provider_company'] = $provider->id;
            }
            else {
                unset($row['provider_company']);
            }

            $type = CourseTypesModel::where(['origin_id' => $row['type']])->first();
            if ($type) {
                $row['type'] = $type->id;
            }
            else {
                unset($row['type']);
            }
            $this->insertCourseIfNotExist($row);
        }
        return Response()->json($rows);
    }
    public function parseTr($tr) {
        $tds = $tr->find('td');
        $domSignUp = $tds[0];
        $href = $domSignUp->find('a')->outerHtml;
        preg_match('/link\.value\=\'[^\']+\'/', $href, $matches);


        $domTdClassCode = $tds[1];
        $code = $domTdClassCode->find('span span')[0]->text;
        $name = $domTdClassCode->find('span span')[1]->text;
        $name = str_replace('&nbsp;', '', $name);

        $domTdPrice = $tds[2];
        $price = $domTdPrice->find('span span')->text;
        $price = str_replace('$', '', $price);

        $days = [];

        $domTdDay1 = $tds[3];
        $day1Date = $domTdDay1->find('span span')[0]->text;
        $day1TimeRange = $domTdDay1->find('span span')[1]->text;
        $day1Date = $day1Date != ' ' ? $day1Date : '';
        $day1TimeRange = $day1TimeRange != ' ' ? $day1TimeRange : '';
        if ($day1Date) { array_push($days, ['date' => $day1Date, 'time_range' => $day1TimeRange]); }

        $domTdDay2 = $tds[4];
        $day2Date = $domTdDay2->find('span span')[0]->text;
        $day2TimeRange = $domTdDay2->find('span span')[1]->text;
        $day2Date = $day2Date != ' ' ? $day2Date : '';
        $day2TimeRange = $day2TimeRange != ' ' ? $day2TimeRange : '';
        if ($day2Date) { array_push($days, ['date' => $day2Date, 'time_range' => $day2TimeRange]); }

        $domTdDay3 = $tds[5];
        $day3Date = $domTdDay3->find('span span')[0]->text;
        $day3TimeRange = $domTdDay3->find('span span')[1]->text;
        $day3Date = $day3Date != ' ' ? $day3Date : '';
        $day3TimeRange = $day3TimeRange != ' ' ? $day3TimeRange : '';
        if ($day3Date) { array_push($days, ['date' => $day3Date, 'time_range' => $day3TimeRange]); }

        $domTdDay4 = $tds[6];
        $day4Date = $domTdDay4->find('span span')[0]->text;
        $day4TimeRange = $domTdDay4->find('span span')[1]->text;
        $day4Date = $day4Date != ' ' ? $day4Date : '';
        $day4TimeRange = $day4TimeRange != ' ' ? $day4TimeRange : '';
        if ($day4Date) { array_push($days, ['date' => $day4Date, 'time_range' => $day4TimeRange]); }

        $domTdAvail = $tds[7];
        $avail = $domTdAvail->find('span span')->text;

        $domTdLocation = $tds[8];
        $location = $domTdLocation->find('span span')[0]->text;
        return [
            'code' => $code,
            'name' => $name,
            'price' => $price,
            'days' => $days,
            'avail' => $avail,
            'location' => $location
        ];
    }
    public function pullByCC($c, $CC, $type) {
        $client = new Client();
        $url = 'https://app.msi5.com/scripts/i5.exe?book=t3rgsched&page=CS.CL&c='. $c .'&fr=FrLayout.1&page=CS.CL.Select&CC='. $CC .'&SC=*FA&Mode=PB&WO=0&SF=0&LAP=12';
        $res = $client->get($url, [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'
            ]
        ]);
        $html = $res->getBody()->getContents();
        $dom = new Dom();
        $dom->loadStr($html, []);
        $this->formData = $this->getFormData($dom, $html);
        $table = $dom->find('.container table')[0];
        $trs = $table->find('tr');
        $rows = [];
        foreach ($trs as $key => $tr) {
            if (!$key) { continue; }
            $row = $this->parseTr($tr);
            $location = $this->pullLocationByClass($row['code']);
            $row = array_merge($row, $location);
            $this->formatCourse($row);
            $row = $this->getCourseFromClass($row, $type);
            $formData = $this->formData;
            $formData['link'] = $formData['links'][$key - 1];
            unset($formData['links']);
            $row['register_data'] = json_encode([
                'url' => $url,
                'type' => 'post',
                'form_data' => $formData
            ]);
            array_push($rows, $row);
        }
        return $rows;
    }
    public function pullLocationByClass($classCode) {
        $url = 'http://app.msi5.com/RegMap/RegGMap.aspx?CC='. $classCode .'&CID=7E56';
        $dom = new Dom();
        $dom->loadFromUrl($url);

        $rangeLocation = $dom->getElementById('RangeAdd1')->getAttribute('value');
        $location = $dom->getElementById('ClassAdd1')->getAttribute('value');

        $rangeAddress = $dom->getElementById('RangeAdd2')->getAttribute('value');
        $address = $dom->getElementById('ClassAdd2')->getAttribute('value');

        $rangeCity = $dom->getElementById('RangeCity')->getAttribute('value');
        $city = $dom->getElementById('ClassCity')->getAttribute('value');

        $rangeState = $dom->getElementById('RangeState')->getAttribute('value');
        $state = $dom->getElementById('ClassState')->getAttribute('value');

        $rangeZip = $dom->getElementById('RangeZip')->getAttribute('value');
        $zip = $dom->getElementById('ClassZip')->getAttribute('value');

        $rangeLat = $dom->getElementById('LocationLatitudeRange')->getAttribute('value');
        $rangeLng = $dom->getElementById('LocationLongitudeRange')->getAttribute('value');

        $lat = $dom->getElementById('LocationLatitudeClassRoom')->getAttribute('value');
        $lng = $dom->getElementById('LocationLongitudeClassRoom')->getAttribute('value');

        $rangeInfo = $dom->getElementById('RangeInfo')->getAttribute('value');
        $classRoomInfo = $dom->getElementById('ClassRoomInfo')->getAttribute('value');

        return [
            'range_location' => $rangeLocation,
            'location' => $location,
            'range_address' => $rangeAddress,
            'address' => $address,
            'range_city' => $rangeCity,
            'city' => $city,
            'range_state' => $rangeState,
            'state' => $state,
            'range_zip' => $rangeZip,
            'zip' => $zip,
            'range_lat' => $rangeLat,
            'range_lng' => $rangeLng,
            'geolocation_lat' => $lat,
            'geolocation_lng' => $lng,
            'range_info' => $rangeInfo,
            'location_info' => $classRoomInfo,
        ];
    }
    public function formatCourse(&$course) {
        $firstDay = $course['days'][0];
        $firstDate = $firstDay['date'];
        $firstTime = str_before($firstDay['time_range'], '-');
        $startAtDateTime = \DateTime::createFromFormat('M d D g:iA', $firstDate . ' ' . $firstTime);
        $startAt = $startAtDateTime->format('Y-m-d\TH:i:s.000\Z');
        $course['start_at'] = $startAt;
        $startMon = $startAtDateTime->format('M');
        $startDay = $startAtDateTime->format('d');

        $lastDay = array_last($course['days']);
        $lastDate = $lastDay['date'];
        $lastTime = str_after($lastDay['time_range'], '-');
        $endAtDateTime = \DateTime::createFromFormat('M d D g:iA', $lastDate . ' ' . $lastTime);
        $endAt = $endAtDateTime->format('Y-m-d\TH:i:s.000\Z');
        $course['end_at'] = $endAt;
        $endMon = $endAtDateTime->format('M');
        $endDay = $endAtDateTime->format('d');

        $dateRangeDisplay = ($startMon === $endMon ? $startMon : $startMon . '/' . $endMon) . ' ' . ($startDay === $endDay ? $startDay : $startDay . '-' . $endDay) . ' ' . $startAtDateTime->format('Y');
        $course['dateRangeDisplay'] = $dateRangeDisplay;

        $weekDays = [];
        foreach ($course['days'] as $day) {
            $date = $day['date'];
            $time = str_before($day['time_range'], '-');
            $dateTime = \DateTime::createFromFormat('M d D g:iA', $date . ' ' . $time);
            $day = $dateTime->format('D');
            array_push($weekDays, $day);
        }
        $course['dayRangeDisplay'] = join(', ', $weekDays);
    }
    public function getCourseFromClass($class, $type){
        return [
            'origin_id' => $class['code'],
            'name' => $class['name'],
            'start_at' => $class['start_at'],
            'end_at' => $class['end_at'],
            'address' => $class['address'],
            'type' => $type,
            'city' => $class['city'],
            'state' => $class['state'],
            'cost' => $class['price'],
            'provider_company' => 't3rg',
            'zip' => $class['zip'],
            'geolocation_lat' => $class['geolocation_lat'],
            'geolocation_lng' => $class['geolocation_lng'],
            'open_seats' => $class['avail'] === 'Sold Out' ? 0 : (int) $class['avail'],
            'origin_data' => json_encode($class),
        ];
    }
    public function insertCourseIfNotExist($course) {
        return CoursesModel::updateOrCreate(['origin_id' => $course['origin_id']], $course);
    }
    private function getFormData(Dom $dom, $html){
        $inputs = $dom->find('input');
        $formData = [];
        foreach ($inputs as $input) {
            $name = $input->getAttribute('name');
            $value = $input->getAttribute('value');
            $formData[$name] = $value;
        }
        preg_match('/document\.forms\[0\]\.frsid\.value\=(?P<frsid>\d+)/', $html, $matches);
        if (isset($matches['frsid'])) {
            $formData['frsid'] = $matches['frsid'];
        }
        preg_match_all('/\{document\.CL\.link\.value\=\'(?P<links>[^\']+)\'/', $html, $linkMatches);
        $formData['links'] = $linkMatches['links'];
        return $formData;
    }
}

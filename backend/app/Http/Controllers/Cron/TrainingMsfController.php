<?php

namespace App\Http\Controllers\Cron;

use App\Models\CourseProvidersModel;
use App\Models\CoursesModel;
use App\Models\CourseTypesModel;
use App\Models\OptionsModel;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Arr;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPHtmlParser\Dom;

class TrainingMsfController extends Controller
{
    //
    private $client;
    private $config = [];
    private $baseUrl = 'https://training.msf-usa.org/res/street/enroll/';

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $this->baseUrl,
            'cookies' => true,
        ]);
    }

    public function index()
    {
        $entries1 = $this->pullDataByType('ctgd=KQR6IAigfbpepns%2fZENM7A%3d%3d&zc=Q2YqCvNVHJAavvEVUMEpJw%3d%3d', 'NRC');
        $entries2 = $this->pullDataByType('ctgd=mGtHwWWVpkDbYvTBW7PaYQ%3d%3d&z=80020', 'SRC-SP');
        $entries3 = $this->pullDataByType('ctgd=wY2g42sv6eVmBmYYCET0yw%3d%3d&z=80020', '3WNRC');
        $entries4 = $this->pullDataByType('ctgd=vshw%2b%2b8MZXztV17lNlzhcA%3d%3d&z=80403', 'NRC');
        $classes = array_merge($entries1['classes'], $entries2['classes'], $entries3['classes'], $entries4['classes']);
        $providers = array_merge($entries1['providers'], $entries2['providers'], $entries3['providers'], $entries4['providers']);
        foreach ($providers as $provider) {
            CourseProvidersModel::updateOrCreate(
                ['origin_id' => $provider['origin_id']],
                $provider
            );
        }
        foreach ($classes as $class) {
            $course = $this->getCourseFromClass($class);
            $provider = CourseProvidersModel::where('origin_id', $course['provider_company'])->first();
            $course['provider_company'] = $provider['id'];
            $type = CourseTypesModel::where('origin_id', $course['type'])->first();
            $course['type'] = $type['id'];
            CoursesModel::updateOrCreate(
                ['origin_id' => $course['origin_id']],
                $course
            );
        }
        var_dump($providers);
        die();
    }

    public function test()
    {
        $entries1 = $this->pullDataByType('ctgd=vshw%2b%2b8MZXztV17lNlzhcA%3d%3d&z=80403', 'NRC');
        var_dump($entries1);
        die();
    }

    private function pullDataByType($query, $type)
    {
        $this->getConfig(false, $query);
        $sites = $this->pullSites($query);
        $classes = [];
        $providers = [];
        foreach ($sites as $site) {
            if ($site['type'] === 'class') {
                $siteClasses = $this->pullClasses($site);
                foreach ($siteClasses['classes'] as & $class) {
                    $class['type'] = $type;
                    $class['provider_company'] = $siteClasses['provider']['origin_id'];
                }
                $providers[] = $siteClasses['provider'];
                $classes = array_merge($classes, $siteClasses['classes']);
            } else {
                $siteProvider = $this->pullProvider($site);
                $providers[] = $siteProvider;
            }
//            break;
        }
        return [
            'classes' => $classes,
            'providers' => $providers
        ];
    }

    public function pullSites($query = 'ctgd=KQR6IAigfbpepns%2fZENM7A%3d%3d&zc=Q2YqCvNVHJAavvEVUMEpJw%3d%3d')
    {
        $sites = [];
        $res = $this->client->post('sites.aspx?' . $query, [
            'form_params' => [
                '__EVENTTARGET' => '',
                '__EVENTARGUMENT' => '',
                '__VIEWSTATE' => $this->config['__VIEWSTATE'],
                '__VIEWSTATEGENERATOR' => $this->config['__VIEWSTATEGENERATOR'],
                '__VIEWSTATEENCRYPTED' => '',
                '__EVENTVALIDATION' => $this->config['__EVENTVALIDATION'],
                'ctl00$ctlMainContent$cboMiles' => '500',
                'ctl00$ctlMainContent$btnSearch' => 'Search'
            ],
        ]);
        $rlt = $res->getBody()->getContents();
        $dom = new Dom();
        $dom->loadStr($rlt, []);
        $this->getConfig($dom);
        $pageNumber = 1;
        do {
            $pageSites = $this->parseSitesFromPage($dom, $query);
            $sites = array_merge($sites, $pageSites);
            $pager = $dom->find('.pager');
            $links = count($pager) ? $pager->find('a') : [];
            $pageNumber++;
            $pageExist = false;
            foreach ($links as $link) {
                $href = $link->getAttribute('href');
                if (str_contains($href, 'Page$' . $pageNumber)) {
                    $pageExist = true;
                    break;
                }
            }
            if ($pageExist) {
                $formParams = [
                    '__EVENTTARGET' => 'ctl00$ctlMainContent$grdSites',
                    '__EVENTARGUMENT' => 'Page$' . $pageNumber,
                    '__VIEWSTATE' => $this->config['__VIEWSTATE'],
                    '__VIEWSTATEGENERATOR' => $this->config['__VIEWSTATEGENERATOR'],
                    '__VIEWSTATEENCRYPTED' => '',
                    '__EVENTVALIDATION' => $this->config['__EVENTVALIDATION'],
                    'ctl00$ctlMainContent$cboMiles' => '500',
                ];
                $res = $this->client->post('sites.aspx?' . $query, [
                    'form_params' => $formParams,
                ]);
                $dom->loadStr($res->getBody()->getContents(), []);
                $this->getConfig($dom);
            }
        } while ($pageExist);
        return $sites;
    }

    private function parseSitesFromPage(Dom $dom, $query)
    {
        $pageSites = [];
        $table = $dom->getElementById('ctl00_ctlMainContent_grdSites');
        $trs = $table->find('tr');
        $pager = $dom->find('.pager');
        $len = count($pager) ? count($trs) - 1 : count($trs);
        for ($i = 1; $i < $len; $i++) {
            $tr = $trs[$i];
            $tds = $tr->find('td');
            $city = $tds[7]->text;
            $state = $tds[8]->text;
            if ($state !== 'CO') {
                continue;
            }
            $type = 'class';
            $availability = $tds[5]->find('span')->text;
            if (str_is('Contact*', $availability)) {
                $type = 'provider';
            }
            $site = $tds[0]->find('input')->value;
            $pageSites[] = [
                'site' => $site,
                'config' => $this->config,
                'query' => $query,
                'type' => $type,
                'data' => [
                    'city' => $city,
                    'state' => $state
                ]
            ];
        }
        return $pageSites;
    }

    public function pullClasses($site)
    {
        $redir = '';
        $res = $this->client->post('sites.aspx?' . $site['query'], [
            'form_params' => [
                '__EVENTTARGET' => '',
                '__EVENTARGUMENT' => '',
                '__VIEWSTATE' => $site['config']['__VIEWSTATE'],
                '__VIEWSTATEGENERATOR' => $site['config']['__VIEWSTATEGENERATOR'],
                '__VIEWSTATEENCRYPTED' => '',
                '__EVENTVALIDATION' => $site['config']['__EVENTVALIDATION'],
                'ctl00$ctlMainContent$cboMiles' => '500',
                'grpSites' => $site['site'],
                'ctl00$ctlMainContent$btnNext' => 'Next Step'
            ],
            'on_stats' => function ($stats) use (&$redir) {
                $redir = (string)$stats->getEffectiveUri();
            }
        ]);
        $rlt = $res->getBody()->getContents();
        $dom = new Dom();
        $dom->loadStr($rlt, []);
        $this->getConfig($dom);
        $provider = $this->parseProvider($dom);
        $classes = [];
        $pageNumber = 1;
        do {
            $pageClasses = $this->parseClassesFromPage($dom);
            foreach ($pageClasses as &$class) {
                $class['register_data'] = json_encode([
                    'url' => $this->baseUrl . 'sites.aspx?' . $site['query'],
                    'type' => 'get'
                ]);
            }
            $classes = array_merge($classes, $pageClasses);
            $pager = $dom->find('.pager');
            $links = count($pager) ? $pager->find('a') : [];
            $pageNumber++;
            $pageExist = false;
            foreach ($links as $link) {
                $href = $link->getAttribute('href');
                if (str_contains($href, 'Page$' . $pageNumber)) {
                    $pageExist = true;
                    break;
                }
            }
            if ($pageExist) {
                $formParams = [
                    '__EVENTTARGET' => 'ctl00$ctlMainContent$grdClasses',
                    '__EVENTARGUMENT' => 'Page$' . $pageNumber,
                    '__VIEWSTATE' => $this->config['__VIEWSTATE'],
                    '__VIEWSTATEGENERATOR' => $this->config['__VIEWSTATEGENERATOR'],
                    '__VIEWSTATEENCRYPTED' => '',
                    '__EVENTVALIDATION' => $this->config['__EVENTVALIDATION'],
                ];
                $res = $this->client->post($redir, [
                    'form_params' => $formParams,
                ]);
                $dom->loadStr($res->getBody()->getContents(), []);
                $this->getConfig($dom);
            }
        } while ($pageExist);
        $provider['city'] = $classes[0]['city'];
        $provider['state'] = $classes[0]['state'];
        $provider['zip'] = $classes[0]['zip'];
        $provider['address'] = $classes[0]['address'];
        $provider['geolocation_lat'] = $classes[0]['geolocation_lat'];
        $provider['geolocation_lng'] = $classes[0]['geolocation_lng'];
        return [
            'provider' => $provider,
            'classes' => $classes
        ];
    }

    public function pullProvider($site)
    {
        $redir = '';
        $res = $this->client->post('sites.aspx?' . $site['query'], [
            'form_params' => [
                '__EVENTTARGET' => '',
                '__EVENTARGUMENT' => '',
                '__VIEWSTATE' => $site['config']['__VIEWSTATE'],
                '__VIEWSTATEGENERATOR' => $site['config']['__VIEWSTATEGENERATOR'],
                '__VIEWSTATEENCRYPTED' => '',
                '__EVENTVALIDATION' => $site['config']['__EVENTVALIDATION'],
                'ctl00$ctlMainContent$cboMiles' => '500',
                'grpSites' => $site['site'],
                'ctl00$ctlMainContent$btnNext' => 'Next Step'
            ],
            'on_stats' => function ($stats) use (&$redir) {
                $redir = (string)$stats->getEffectiveUri();
            }
        ]);
        $rlt = $res->getBody()->getContents();
        $dom = new Dom();
        $dom->loadStr($rlt, []);
        $provider = [];
        $provider['name'] = $dom->find('#ctl00_ctlMainContent_lnkCompany')->text;
        $provider['phone'] = $dom->find('#ctl00_ctlMainContent_lblEnrollmentPhone')->text;
        $provider['contact_email'] = $dom->find('#ctl00_ctlMainContent_lblEnrollmentEmail')->text;
        $provider['city'] = $site['data']['city'];
        $provider['state'] = $site['data']['state'];
        $provider['origin_id'] = str_replace(' ', '_', $provider['name']);
        return $provider;
    }

    private function parseClassesFromPage(Dom $dom)
    {
        $table = $dom->find('#ctl00_ctlMainContent_grdClasses');
        if (!count($table)) {
            die();
        }
        $trs = $table->find('tr');
        $classes = [];
        $pager = $dom->find('.pager');
        $len = count($pager) ? count($trs) - 1 : count($trs);
        for ($i = 1; $i < $len; $i++) {
            $tr = $trs[$i];
            $class = $this->parseClassFromTr($tr);
            $class = $this->formatClass($class);
            array_push($classes, $class);
        }
        return $classes;
    }

    private function parseClassFromTr($tr)
    {
        $class = [];
        $tds = $tr->find('td');
        $class['origin_id'] = $tds[0]->find('input')->value;
        $class['open_seats'] = $tds[1]->find('span')->text;
        $class['start_end_date'] = $tds[2]->find('span')->text;
        $class['time'] = $tds[3]->find('span')->text;
        $class['address'] = $tds[4]->find('span')->innerHtml;
        $class['age_free'] = $tds[5]->find('span')->innerHtml;
        return $class;
    }

    private function formatClass(& $class)
    {
        $dateStartEnd = explode(' - ', $class['start_end_date']);
        $startDate = $dateStartEnd[0];
        $endDate = $dateStartEnd[1];
        if (preg_match_all('/\d+:\d+[ap]m/', $class['time'], $matches)) {

        } else if (preg_match_all('/\d+:\d+/', $class['time'], $matches)) {
            foreach ($matches[0] as &$match) {
                $h = (int)str_before($match, ':');
                $m = (int)str_after($match, ':');
                if ($h >= 12) {
                    $match = $h - 12 . ':' . $m . 'pm';
                } else {
                    $match = $match . 'am';
                }
            }
        } else {
            preg_match_all('/\d+[aApP][mM]/', $class['time'], $matches);
            foreach ($matches[0] as &$match) {
                $h = substr($match, 0, strlen($match) - 2);
                $am = substr($match, -2);
                $match = $h . ':00' . strtolower($am);
            }
        }
        $times = $matches[0];
        $startTime = $times[0];
        $endTime = $times[count($times) - 1];
        $startAt = $startDate . ' ' . $startTime;
        $dateTime = \DateTime::createFromFormat('D n/j/Y g:ia', $startAt);
        $class['start_at'] = $dateTime->format('Y-m-d\TH:i:s.000\Z');
        $startMon = $dateTime->format('M');
        $startDay = $dateTime->format('d');
        $startWeekDay = $dateTime->format('D');

        $endAt = $endDate . ' ' . $endTime;
        $dateTime = \DateTime::createFromFormat('D n/j/Y g:ia', $endAt);
        $class['end_at'] = $dateTime->format('Y-m-d\TH:i:s.000\Z');
        $endMon = $dateTime->format('M');
        $endDay = $dateTime->format('d');
        $endWeekDay = $dateTime->format('D');

        $dateRangeDisplay = ($startMon === $endMon ? $startMon : $startMon . '/' . $endMon) . ' ' . ($startDay === $endDay ? $startDay : $startDay . '-' . $endDay) . ' ' . $dateTime->format('Y');
        $class['dateRangeDisplay'] = $dateRangeDisplay;
        $class['dayRangeDisplay'] = $startWeekDay . ' ~ ' . $endWeekDay;

        $addressParse = explode('<br />', $class['address']);
        $class['address'] = $addressParse[0];
        $cityState = $addressParse[count($addressParse) - 1];
        preg_match('@(?P<city>[\w\s]+), (?P<state>\w+) (?P<zip>\d+)@', $cityState, $matches);
        $class['city'] = $matches['city'];
        $class['state'] = $matches['state'];
        $class['zip'] = $matches['zip'];

        preg_match('@\$(?P<cost>[\d\.]+)$@', $class['age_free'], $matches);
        $class['cost'] = (int)$matches['cost'];
        $geo = $this->getGeolocation($class['address']);
        if ($geo) {
            $class['geolocation_lat'] = $geo['lat'];
            $class['geolocation_lng'] = $geo['lng'];
        }
        return $class;
    }

    private function getCourseFromClass($class)
    {
        $course = [
            'name' => '',
            'origin_id' => $class['origin_id'],
            'start_at' => $class['start_at'],
            'end_at' => $class['end_at'],
            'address' => $class['address'],
            'type' => $class['type'],
            'provider_company' => $class['provider_company'],
            'city' => $class['city'],
            'state' => $class['state'],
            'cost' => $class['cost'],
            'zip' => $class['zip'],
            'open_seats' => $class['open_seats'] === 'Full' ? 0 : (int)$class['open_seats'],
            'origin_data' => json_encode($class),
            'register_data' => $class['register_data']
        ];
        if (isset($class['geolocation_lat'])) {
            $course['geolocation_lat'] = $class['geolocation_lat'];
            $course['geolocation_lng'] = $class['geolocation_lng'];
        }
        return $course;
    }

    private function parseProvider(Dom $dom)
    {
        $domProvider = $dom->find('.provider-info-small');
        if (!count($domProvider)) {
            die();
        }
        $provider = [];
        $img = $dom->find('#ctl00_ctlMainContent_imgTrainingProvider');
        if (count($img)) {
            $path = $img->getAttribute('src');
            $absUrl = 'https://training.msf-usa.org/res/street/' . str_after($path, '../');
            $provider['image'] = $absUrl;
        }
        $provider['name'] = $domProvider->find('#ctl00_ctlMainContent_lblCompany')->text;
        $provider['phone'] = $domProvider->find('#ctl00_ctlMainContent_lblEnrollmentPhone')->text;
        $provider['contact_email'] = $domProvider->find('#ctl00_ctlMainContent_lblEnrollmentEmail')->text;
        $provider['origin_id'] = str_replace(' ', '_', $provider['name']);
        return $provider;
    }

    private function getConfig($dom = false, $query = 'ctgd=KQR6IAigfbpepns%2fZENM7A%3d%3d&zc=Q2YqCvNVHJAavvEVUMEpJw%3d%3d')
    {
        if ($dom === false) {
            $res = $this->client->get('sites.aspx?' . $query);
            $dom = new Dom();
            $dom->loadStr($res->getBody()->getContents());
        }
        $inputs = $dom->find('form input');
        foreach ($inputs as $input) {
            $this->config[$input->name] = $input->value;
        }
    }

    private function getGeolocation($address)
    {
        $res = $this->client->get('https://maps.googleapis.com/maps/api/geocode/json', [
            'query' => [
                'address' => $address,
                'key' => env('GOOGLE_MAP_API_KEY')
            ]
        ]);
        $resArr = parseStringToArray($res->getBody()->getContents());
        if (!isset($resArr['results'][0])) {
            return false;
        }
        return $resArr['results'][0]['geometry']['location'];
    }
}

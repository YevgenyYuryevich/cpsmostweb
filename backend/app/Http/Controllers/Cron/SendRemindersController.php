<?php

namespace App\Http\Controllers\Cron;

use App\Models\RemindersModel;
use App\Models\UserLevelsModel;
use App\Models\UsersModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;

class SendRemindersController extends Controller
{
    //
    public function index() {
        $currentReminders = RemindersModel::where(['sent' => 0])->get();
        foreach ($currentReminders as $reminder) {
            $this->send($reminder);
        }
        return Response()->json($currentReminders);
    }
    public function send($reminder) {
        $instructorLevel = UserLevelsModel::where('name', 'instructor')->first();
        $providerLevel = UserLevelsModel::where('name', 'provider')->first();
        switch ($reminder['group']) {
            case 'instructor':
                $users = UsersModel::where('level', $instructorLevel['id'])->get();
                break;
            case 'provider':
                $users = UsersModel::where('level', $providerLevel['id'])->get();
                break;
            default:
                $users = UsersModel::where('level', $providerLevel['id'])->orWhere('level', $instructorLevel['id'])->get();
                break;
        }
        foreach ($users as $user) {
            switch ($reminder['type']) {
                case 'email':
                    $this->sendMail($reminder, $user);
                    break;
                case 'sms':
                    $this->sendSMS($reminder, $user);
                    break;
                default:
                    $this->sendMail($reminder, $user);
                    $this->sendSMS($reminder, $user);
                    break;
            }
        }
        RemindersModel::where('id', $reminder['id'])->update(['sent' => 1]);
    }
    public function sendMail($reminder, $user) {
        if ($user['email']) {
            Mail::raw($reminder['body'], function ($message) use ($user, $reminder) {
                $message->to($user['email']);
                $message->subject($reminder['name']);
            });
            return true;
        } else {
            return false;
        }
    }
    public function sendSMS($reminder, $user) {
        if ($user['phone']) {
            $sid = env('TWILIO_SID');
            $token = env('TWILIO_AUTH_TOKEN');
            $client = new Client($sid, $token);
            $client->messages->create(
                $user['phone'],
                array(
                    'from' => env('TWILIO_PHONE'),
                    'body' => $reminder['body']
                )
            );
        }
    }
}

<?php

namespace App\Http\Controllers\Cron;

use App\Models\CourseProvidersModel;
use App\Models\CoursesModel;
use App\Models\CourseTypesModel;
use GuzzleHttp\Client;
use function Helpers\parseStringToArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PullCoursesController extends Controller
{
    private $client;
//    private $locations = ['80202'];
    private $locations = ['80202', '81625', '81082', '81501', '80751', '81001', '80829'];
    private $config;
    private $configContext;
    public function __construct()
    {
        $this->client = new Client();
        $this->getConfig();
    }
    //
    public function index() {

        $rows = [];
        $types = CourseTypesModel::all();
        foreach ($types as $type) {
            if (!$type['origin_id']) continue;
            foreach ($this->locations as $location) {
                $typeRows = $this->searchClassesByCategoryAndLocation($type['origin_id'], $location);
                $rows = array_merge($rows, $typeRows);
            }
        }
        foreach ($rows as $key => $row) {
            $provider = $this->getProviderFromClass($row);
            $dbProviderId = $this->insertProviderIfNotExist($provider);

            $type = $this->getTypeFromClass($row);
            $dbTypeId = $this->insertTypeIfNotExist($type);

            $course = $this->getCourseFromClass($row);
            $course['provider_company'] = $dbProviderId;
            $course['type'] = $dbTypeId;
            $this->insertCourseIfNotExist($course);
        }
        var_dump($rows);
        die();
    }
    public function searchClassesByCategoryAndLocation($category, $location) {
        $uri = 'https://riders.harley-davidson.com/s/sfsites/aura?r=4&other.ClassLocationsSearch.getSearchResultsByLocation=1';
        $postData = [
            'message' => '{"actions":[{"id":"167;a","descriptor":"apex://ClassLocationsSearch/ACTION$getSearchResultsByLocation","callingDescriptor":"markup://c:findRiderCourseCmp","params":{"location":"%s","courseCode":"%s","distance":"100","includeNearbyStates":"true","distanceUnit":"mi","countryCod":"US","userLang":"EN_US"}}]}',
            'aura.context' => json_encode( $this->configContext ),
            'aura.pageURI' => "/s/?language=en_US",
            'aura.token'=> 'undefined'
        ];
        $postData['message'] = sprintf($postData['message'], $location, $category);
        $res = $this->client->post($uri, [
            'form_params' => $postData,
        ]);
        $resArr = parseStringToArray($res->getBody()->getContents());
        return $resArr['actions'][0]['returnValue'];
    }
    public function insertCourseIfNotExist($course) {
        $this->formatCourse($course);
        return CoursesModel::updateOrCreate(['origin_id' => $course['origin_id']], $course);
    }
    public function insertProviderIfNotExist($provider) {
        $row =  CourseProvidersModel::updateOrCreate(['origin_id' => $provider['origin_id']], $provider);
        return $row->id;
    }
    public function insertTypeIfNotExist($type) {
        $row = CourseTypesModel::where('origin_id', $type['origin_id'])->first();
        if ($row) {
            return $row->id;
        }
        return CourseTypesModel::insert($type);
    }
    private function getCourseFromClass($class) {
        $parseArr = explode(' ', $class['dealerCityStatePostalCode']);
        $zip_code = $parseArr[sizeof($parseArr) - 1];
        $openSeats = (int) $class['classSize'] - (int) $class['enrolledSize'];
        $registerUrl = $this->getRegisterUrlFromClass($class);
        $course = [
            'origin_id' => $class['id'],
            'name' => $class['classRangeLocation'] . ' ' . $class['dealerLocation'],
            'start_at' => $class['classStart'],
            'end_at' => $class['classEnd'],
            'address' => $class['dealerStreetAddress'],
            'type' => $class['courseCode'],
            'city' => $class['rangeCity'],
            'state' => $class['rangeState'],
            'cost' => $class['classFee'],
            'provider_company' => $class['dealerId'],
            'zip' => $zip_code,
            'open_seats' => $openSeats,
            'origin_data' => json_encode($class),
            'register_data' => $registerUrl ? json_encode([
                'url' => $registerUrl,
                'type' => 'get',
            ]) : ''
        ];
        return $course;
    }
    private function getRegisterUrlFromClass($class) {
        switch ($class['dealerName']) {
            case 'Thunder Mountain Harley-Davidson':
                if ($class['courseCode'] === 'NRC') {
                    return 'https://riders.harley-davidson.com/s/?language=en_US#11919&expLvl=new';
                } else if ($class['courseCode'] === 'SRC-SP') {
                    return 'https://riders.harley-davidson.com/s/?language=en_US#11919&expLvl=SRC-SP';
                }
                break;
            case 'Grand Junction Harley-Davidson':
                if ($class['courseCode'] === 'NRC') {
                    return 'https://riders.harley-davidson.com/s/?language=en_US#12035&expLvl=new';
                } else if ($class['courseCode'] === 'SRC-SP') {
                    return false;
                }
                break;
            case 'Rocky Mountain Harley-Davidson':
                if ($class['courseCode'] === 'NRC') {
                    return 'https://riders.harley-davidson.com/s/?language=en_US#12209&expLvl=new';
                } else if ($class['courseCode'] === 'SRC-SP') {
                    return 'https://riders.harley-davidson.com/s/?language=en_US#12209&expLvl=SRC-SP';
                }
                break;
            case 'Greeley Harley-Davidson':
                if ($class['courseCode'] === 'NRC') {
                    return 'https://riders.harley-davidson.com/s/?language=en_US#42398&expLvl=new';
                } else if ($class['courseCode'] === 'SRC-SP') {
                    return 'https://riders.harley-davidson.com/s/?language=en_US#42398&expLvl=SRC-SP';
                }
                break;
            case 'Mile High Harley-Davidson':
                if ($class['courseCode'] === 'NRC') {
                    return 'https://riders.harley-davidson.com/s/?language=en_US#104835&expLvl=new';
                } else if ($class['courseCode'] === 'SRC-SP') {
//                    need to ask to Client whether it's false
                    return false;
                }
                break;
            case 'Avalanche Harley-Davidson':
                return 'https://riders.harley-davidson.com/s/?language=en_US#92452&expLvl=new';
                break;
            default:
                return false;
                break;
        }
    }
    private function getProviderFromClass($class) {
        sscanf($class['dealerCityStatePostalCode'], '%s %s %s', $city, $state, $zip_code);
        $city = str_replace(',', '', $city);
        $provider = [
            'origin_id' => $class['dealerId'],
            'name' => $class['dealerName'],
            'phone' => $class['dealerPhone'],
            'address' => $class['dealerStreetAddress'],
            'contact_email' => $class['dealerEmail'],
            'contact_person' => $class['ramContact'],
            'city' => $city,
            'state' => $state,
            'zip' => $zip_code
        ];
        $geolocation = $this->getGeolocation($provider['address']);
        $provider['geolocation_lat'] = $geolocation['lat'];
        $provider['geolocation_lng'] = $geolocation['lng'];
        return $provider;
    }
    private function getTypeFromClass($class) {
        return [
            'name' => $class['dealerCourseName'],
            'origin_id' => $class['courseCode'],
        ];
    }
    private function formatCourse(& $course) {
        $geolocation = $this->getGeolocation($course['address']);
        $course['geolocation_lat'] = $geolocation['lat'];
        $course['geolocation_lng'] = $geolocation['lng'];
        return $course;
    }
    private function getGeolocation($address) {
        $res = $this->client->get('https://maps.googleapis.com/maps/api/geocode/json', [
            'query' => [
                'address' => $address,
                'key' => env('GOOGLE_MAP_API_KEY')
            ]
        ]);
        $resArr = parseStringToArray($res->getBody()->getContents());
        return $resArr['results'][0]['geometry']['location'];
    }
    public function getConfig() {
        $client = new Client(['cookies' => true]);
        $res = $client->get('https://riders.harley-davidson.com/s/?language=en_US', [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36'
            ]
        ]);
        $html = $res->getBody()->getContents();
        preg_match('/var\s+auraConfig\s*=\s*([^;]+)/', $html, $matches);
        $this->config = parseStringToArray($matches[1]);
        $contextKeys = ['mode', 'fwuid', 'app', 'loaded', 'dn', 'globals', 'uad'];
        $this->configContext = array_filter(
            $this->config['context'],
            function ($key) use ($contextKeys) {
                return in_array($key, $contextKeys);
            },
            ARRAY_FILTER_USE_KEY
        );;
        return $this->config;
    }
    public function test() {
        $rows = $this->searchClassesByCategoryAndLocation('NRC', '80202');
        foreach ($rows as $key => $row) {
            $provider = $this->getProviderFromClass($row);
            echo json_encode($provider);
            $dbProviderId = $this->insertProviderIfNotExist($provider);

            $type = $this->getTypeFromClass($row);
            $dbTypeId = $this->insertTypeIfNotExist($type);

            $course = $this->getCourseFromClass($row);
            $course['provider_company'] = $dbProviderId;
            $course['type'] = $dbTypeId;
            $this->insertCourseIfNotExist($course);
        }
        die();
    }
}

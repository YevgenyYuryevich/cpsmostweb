<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'TestController@index')->name('test');
Route::get('/test/callback', 'TestController@callback')->name('test');

Route::post('/api/auth/register', 'Auth\AuthApi@register')->name('rest_register');
Route::post('/api/auth/login', 'Api\AuthApi@login')->name('rest_login');
Route::post('/api/auth/logout', 'Api\AuthApi@logout')->name('rest_logout');
Route::post('/api/password/email', 'Api\ForgotPasswordController@sendResetLinkEmail')->name('rest_send_reset');
Route::post('/api/password/reset', 'Api\ResetPasswordController@reset')->name('rest_reset_password');
Route::get('/admin/auth/reset-password/{token}', 'Api\ResetPasswordController@showResetForm')->name('password.reset');
//Route::get('/api/auth/get', function () {
//    if (!Auth::check()) {
//            $user = App\User::find(1);
//            Auth::login($user);
//    }
//    $user = Auth::user();
//    return Response()->json(['data' => $user, 'token' => csrf_token()]);
//});

Route::get('/api/app_common', 'Api\AppCommonController@index');

//Route::get('/api/users', 'Api\UsersController@getMany');
Route::get('/api/users/get/{id}', 'Api\UsersController@get');
Route::get('/api/users/delete/{id}', 'Api\UsersController@delete');
Route::post('/api/users/update', 'Api\UsersController@update');
Route::post('/api/users/insert', 'Api\UsersController@insert');

Route::get('/api/user-levels/delete/{id}', 'Api\UserLevelsController@delete');
Route::post('/api/user-levels/update', 'Api\UserLevelsController@update');
Route::post('/api/user-levels/insert', 'Api\UserLevelsController@insert');

Route::get('/cron/pull-courses', 'Cron\PullCoursesController@index');
Route::get('/cron/harely-test', 'Cron\PullCoursesController@test');
Route::get('/cron/get-config', 'Cron\PullCoursesController@getConfig');
Route::get('/cron/pull-t3rg', 'Cron\T3rgController@index');
Route::get('/cron/pull-training', 'Cron\TrainingMsfController@index');
Route::get('/cron/training/get-register-link/{id}', 'Cron\TrainingMsfController@getRegisterLink');
Route::get('/cron/pull-training-test', 'Cron\TrainingMsfController@test');
Route::get('/cron/send-reminders', 'Cron\SendRemindersController@index');
Route::get('/cron/pull-iron-buffalo', 'Cron\IronBuffaloController@index');

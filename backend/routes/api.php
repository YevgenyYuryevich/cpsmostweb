<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('posts/get_many', 'Api\PostsController@getMany');
Route::post('posts/get/{id}', 'Api\PostsController@get');
Route::post('posts/update', 'Api\PostsController@update');
Route::post('posts/update_many', 'Api\PostsController@updateMany');
Route::post('posts/insert', 'Api\PostsController@insert');
Route::post('posts/insert_many', 'Api\PostsController@insertMany');
Route::post('posts/delete/{id}', 'Api\PostsController@delete');
Route::post('posts/permanent-resources', 'Api\PostsController@permanentResources');

Route::post('search', 'Api\SearchController@index');
Route::post('upload', 'Api\UploadsController@index');
Route::post('file_to_html', 'Api\FileToHtmlController@index');
Route::post('course-types/get-many', 'Api\CourseTypesController@getMany');
Route::post('course-types/get', 'Api\CourseTypesController@get');
Route::post('course-providers/get-many', 'Api\CourseProvidersController@getMany');
Route::post('course-providers/get', 'Api\CourseProvidersController@get');
Route::post('course-providers/insert', 'Api\CourseProvidersController@insert');
Route::post('course-providers/update', 'Api\CourseProvidersController@update');
Route::post('course-providers/delete', 'Api\CourseProvidersController@delete');
Route::get('course-providers/fill-geolocation', 'Api\CourseProvidersController@fillGeolocation');
Route::post('course-types/insert', 'Api\CourseTypesController@insert');
Route::post('course-types/update', 'Api\CourseTypesController@update');
Route::post('course-types/delete', 'Api\CourseTypesController@delete');

Route::prefix('courses')->group(function () {
    Route::post('get-many', 'Api\CoursesController@getMany');
    Route::post('insert', 'Api\CoursesController@insert');
    Route::post('update', 'Api\CoursesController@update');
    Route::post('delete', 'Api\CoursesController@delete');
    Route::post('plus-views', 'Api\CoursesController@plusViews');
    Route::post('plus-clicks', 'Api\CoursesController@plusClicks');
    Route::post('get-tracks', 'Api\CoursesController@getTracks');
});

Route::get('options/social', 'Api\OptionsController@getSocial');
Route::post('options/update-or-create', 'Api\OptionsController@updateOrCreate');
Route::post('options/get-options', 'Api\OptionsController@getOptions');

Route::prefix('newsletter')->group(function () {
    Route::post('subscribe', 'Api\NewsletterController@subscribe');
    Route::post('get-many', 'Api\NewsletterController@getMany');
});

Route::post('eventbrite/pull-organizations', 'Api\EventbriteController@pullOrganizations');
Route::post('eventbrite/pull-events', 'Api\EventbriteController@pullEvents');
Route::get('eventbrite/feed', 'Api\EventbriteController@pullEvents');

Route::post('instagram/token/{code}', 'Api\InstagramController@getToken');
Route::get('instagram/feed', 'Api\InstagramController@feed');

Route::post('twitter/pull-recent-tweets', 'Api\TwitterController@recentTweets');
Route::post('twitter/search-tweets', 'Api\TwitterController@searchTweets');
Route::get('twitter/feed', 'Api\TwitterController@feed');

Route::post('facebook/login_url', 'Api\FacebookController@loginUrl');
Route::get('facebook/token', 'Api\FacebookController@getToken');
Route::get('facebook/feed', 'Api\FacebookController@feed');


Route::prefix('reminders')->group(function () {
   Route::post('get', 'Api\RemindersController@get');
   Route::post('get_many', 'Api\RemindersController@getMany');
   Route::post('insert', 'Api\RemindersController@insert');
   Route::post('update', 'Api\RemindersController@update');
   Route::post('delete', 'Api\RemindersController@delete');
});

Route::prefix('faqs')->group(function () {
    Route::post('get', 'Api\FaqsController@get');
    Route::post('get_many', 'Api\FaqsController@getMany');
    Route::post('insert', 'Api\FaqsController@insert');
    Route::post('update', 'Api\FaqsController@update');
    Route::post('update-many', 'Api\FaqsController@updateMany');
    Route::post('delete', 'Api\FaqsController@delete');
});
Route::prefix('media')->group(function () {
    Route::post('upload', 'Api\MediaController@upload');
});
Route::middleware('auth:api')->prefix('incidents')->group(function () {
   Route::post('get', 'Api\IncidentsController@get');
   Route::post('get-many', 'Api\IncidentsController@getMany');
   Route::post('insert', 'Api\IncidentsController@insert');
   Route::post('update', 'Api\IncidentsController@update');
   Route::post('delete', 'Api\IncidentsController@delete');
   Route::post('{id}/sync-media', 'Api\IncidentsController@syncMedia');
});
Route::prefix('users')->group(function () {
    Route::post('get-many', 'Api\UsersController@getMany');
    Route::post('get-instructors', 'Api\UsersController@getInstructors');
    Route::post('get-vendors', 'Api\UsersController@getVendors');
});
Route::middleware('auth:api')->prefix('insurances')->group(function () {
    Route::post('get', 'Api\InsurancesController@get');
    Route::post('get-many', 'Api\InsurancesController@getMany');
    Route::post('insert', 'Api\InsurancesController@insert');
    Route::post('update', 'Api\InsurancesController@update');
    Route::post('delete', 'Api\InsurancesController@delete');
    Route::post('approve', 'Api\InsurancesController@approve');
    Route::post('deny', 'Api\InsurancesController@deny');
});
Route::middleware('auth:api')->prefix('rider-coach-certification')->group(function () {
    Route::post('get', 'Api\RiderCoachCertificationController@get');
    Route::post('get-many', 'Api\RiderCoachCertificationController@getMany');
    Route::post('insert', 'Api\RiderCoachCertificationController@insert');
    Route::post('update', 'Api\RiderCoachCertificationController@update');
    Route::post('delete', 'Api\RiderCoachCertificationController@delete');
    Route::post('approve', 'Api\RiderCoachCertificationController@approve');
    Route::post('deny', 'Api\RiderCoachCertificationController@deny');
});
Route::middleware('auth:api')->prefix('renewal-application')->group(function () {
    Route::post('get', 'Api\RenewalApplicationController@get');
    Route::post('get-many', 'Api\RenewalApplicationController@getMany');
    Route::post('insert', 'Api\RenewalApplicationController@insert');
    Route::post('insert-from-form-library', 'Api\RenewalApplicationController@insertFromFormLibrary');
    Route::post('update', 'Api\RenewalApplicationController@update');
    Route::post('delete', 'Api\RenewalApplicationController@delete');
    Route::post('approve', 'Api\RenewalApplicationController@approve');
    Route::post('deny', 'Api\RenewalApplicationController@deny');
});

Route::middleware('auth:api')->prefix('quality-assurance')->group(function () {
    Route::post('get-many', 'Api\QualityAssuranceController@getMany');
    Route::post('insert', 'Api\QualityAssuranceController@insert');
    Route::post('update', 'Api\QualityAssuranceController@update');
    Route::post('delete', 'Api\QualityAssuranceController@delete');
});

Route::middleware('auth:api')->prefix('form-library')->group(function () {
    Route::post('get-many', 'Api\FormLibraryController@getMany');
    Route::post('insert', 'Api\FormLibraryController@insert');
    Route::post('update', 'Api\FormLibraryController@update');
    Route::post('delete', 'Api\FormLibraryController@delete');
});

Route::prefix('api-auth')->group(function () {
    Route::get('login', 'Auth\ApiAuthController@login');
    Route::post('login', 'Auth\ApiAuthController@login');
    Route::post('register', 'Auth\ApiAuthController@register');
    Route::get('me', 'Auth\ApiAuthController@get')->middleware('auth:api');
});

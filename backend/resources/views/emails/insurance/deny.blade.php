@component('mail::message')
{!! $body !!}

@component('mail::button', ['url' => 'https://cspmostweb.com/admin/manage'])
VISIT SITE
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

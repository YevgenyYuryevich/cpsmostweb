@component('mail::message')
{!! $body !!}

@component('mail::button', ['url' => ''])
Visit Site
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
